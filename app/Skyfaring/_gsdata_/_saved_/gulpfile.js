var gulp        = require('gulp');
var plumber     = require('gulp-plumber');
var util        = require('gulp-util');
var less        = require('gulp-less');
var watch       = require('gulp-watch');

var path        = require('path');
var browserSync = require('browser-sync').create();

browserSync.init({ proxy: { target: "http://sens.oragelac.com" } });

gulp.task('watch', ['assets'], function()
{
    return watch('./Simple/assets/css/*.less', {readDelay: 1000, ignoreInitial: false})
        .pipe(plumber())
        .pipe(less().on('error', util.log))
        .pipe(gulp.dest('./public/css'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('assets', function()
{
    watch(['./**/*.js', './**/*.css', './**/*.html'])
        .pipe(browserSync.reload({stream: true}));
});
