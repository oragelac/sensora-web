<?php

namespace Skyfaring\Simple\HTTP;

use Skyfaring\Simple\Stream as SimpleStream;

/**
 * This class stands as an abstraction layer to retreive and send HTTP messages
 * from and to the client.
 *
 * This object is a factory to delegate HTTP Components creation responsibilities.
 */
class Connection
{
    /**
     * This method translates server-wide values into a Uri object.
     *
     * @param array $server           The server-wide values to operate from
     * @param bool  $useForwardedHost Shall the Uri be seen from the
     *                                forwarded host or not
     *
     * @return Uri The client requested uri
     */
    public static function getUri(array $server = array(), $useForwardedHost = false)
    {
        $server = empty($server) ? $_SERVER : $server;
        $ssl = (isset($server['HTTPS']) && $server['HTTPS'] !== 'off');

        // URI variables definition

        $protocol = 'http'.($ssl ? 's' : '');
        $port = $server['SERVER_PORT'];

        if ((!$ssl && $port == 80) || ($ssl && $port == 443)) {
            $port = '';
        } else {
            $port = ':'.$port;
        }

        if ($useForwardedHost && isset($server['HTTP_X_FORWARDED_HOST'])) {
            $host = $server['HTTP_X_FORWARDED_HOST'];
        } elseif (isset($server['HTTP_HOST'])) {
            $host = $server['HTTP_HOST'].$port;
        } else {
            $host = $server['SERVER_NAME'].$port;
        }

        $path = isset_define($server['REQUEST_URI']);
        $query = isset_define($server['QUERY_STRING']);

        $user = (key_exists('PHP_AUTH_USER', $server) ? $server['PHP_AUTH_USER'] : '');
        $password = (key_exists('PHP_AUTH_PW', $server) ? $server['PHP_AUTH_PW'] : '');

        // URI construction

        $auth = '';
        if ('' !== $password) {
            $auth = $user.('' !== $password ? ':'.$password : '').'@';
        }
        $uri = $protocol.'://'.$auth.$host.$path.$query;

        $components = array('scheme' => 'http', 'host' => 'localhost', 'port' => '80', 'path' => '/');
        $components = array_merge($components, parse_url($uri));

        // Select & Retrieve user input if any
        switch ($server['REQUEST_METHOD']) {
            case 'GET':
                $query = $_GET;
                break;

            case 'POST':
                $query = $_POST;
                break;

            default:
                $query = array();
                break;
        }

        return new Uri($components, $server, $query);
    }

    /**
     * This method translates server-wide values into a Request object.
     *
     * @return Request The client request
     */
    public static function getRequest()
    {
        $uri = self::getUri($_SERVER, false);
        $stream = SimpleStream\StreamFactory::getRequestBody();

        switch ($_SERVER['REQUEST_METHOD']) {
            case 'HEAD':
                $method = Message::HTTP_METHOD_HEAD;
                break;

            case 'GET':
                $method = Message::HTTP_METHOD_GET;
                break;

            case 'POST':
                $method = Message::HTTP_METHOD_POST;
                break;

            case 'PUT':
                $method = Message::HTTP_METHOD_PUT;
                break;
        }
        $headers = getallheaders();

        return new Request($uri, $method, $stream, $headers);
    }
}
