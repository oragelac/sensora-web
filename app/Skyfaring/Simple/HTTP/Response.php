<?php

namespace Skyfaring\Simple\HTTP;

use Skyfaring\Simple\PSR as PSR;

class Response extends Message implements PSR\MessageInterface
{
    /* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var int
     */
    protected $_statusCode = null;

    /**
     * @var string
     */
    protected $_reasonPhrase = null;

    /**
     * @var bool
     */
    protected $_cache = null;

    /**
     * @var int
     */
    protected $_age = null;

    /**
     * Class constructor.
     *
     * Defines main Message and Response components.
     *
     * @param StreamInterface $body         The payload of this response
     * @param array           $headers      The headers this response might send to the client
     * @param string          $reasonPhrase The status code linked reason phrase
     */
    public function __construct(PSR\StreamInterface $body, array $headers = null,
                                $statusCode = '200', $reasonPhrase = null)
    {
        parent::__construct($body, $headers);
        $this->setStatusCode($statusCode);
        $this->setReasonPhrase($reasonPhrase);
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Tiny factory, for chaining.
     */
    public static function create(PSR\StreamInterface $body, array $headers = null,
                                $statusCode = '200', $reasonPhrase = null)
    {
        return new static($body, $headers, $statusCode, $reasonPhrase);
    }

    /**
     * Sets the reason phrase. If the provided reason phrase is empty, the
     * default self::$_statusCodes value is used based on this response
     * $_statusCode.
     *
     * @param string $reasonPhrase The new reason phrase
     *
     * This method implements a fluent interface.
     */
    public function setReasonPhrase($reasonPhrase = '')
    {
        $this->_reasonPhrase = empty($reasonPhrase)
            ? self::$_httpStatus[$this->_statusCode]
            : $reasonPhrase;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getReasonPhrase()
    {
        return $this->_reasonPhrase;
    }

    /**
     * Sets the status code, subject to standard verification.
     *
     * @param int $code The new status code
     *
     * @throws \InvalidArgumentException for unknown status codes
     *
     * This method implements a fluent interface.
     */
    public function setStatusCode($code)
    {
        if (!isset(self::$_httpStatus[$code])) {
            throw new \InvalidArgumentException(
                'Unknown status code '.$code,
                1208
            );
        }

        $this->_statusCode = $code;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getStatusCode()
    {
        return $this->_statusCode;
    }

    /**
     * {@inheritdoc}
     */
    public function withStatus($code, $reasonPhrase = '')
    {
        $response = clone $this;

        return $response
            ->setStatusCode($code)
            ->setReasonPhrase($reasonPhrase);
    }

    /**
     * Sends this response to the client.
     */
    public function send()
    {
        http_response_code($this->_statusCode);
        $this->sendHeaders();
        echo $this->_body->getContents();
    }
}
