<?php

namespace Skyfaring\Simple\HTTP;

use Skyfaring\Simple\PSR as PSR;

/**
 * {@inheritdoc}
 */
class Request extends Message implements PSR\RequestInterface
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var mixed
     */
    protected $_requestTarget = '/';

    /**
     * @var string
     */
    protected $_method = self::HTTP_METHOD_GET;

    /**
     * @var UriInterface
     */
    protected $_uri = null;

    /**
     * Class constructor.
     *
     * @param UriInterface    $uri     This request uri target
     * @param StreamInterface $body    This request body
     * @param string          $method  This request method
     * @param array           $headers This request headers if any
     */
    public function __construct(
        PSR\UriInterface $uri,
        $method = self::HTTP_METHOD_GET,
        PSR\StreamInterface $body = null,
        array $headers = null)
    {
        self::initialize();
        parent::__construct($body, $headers);
        $this->setUri($uri);
        $this->setRequestTarget($uri->getPath());
        $this->setMethod($method);
    }

    /**
     * Standard __clone() redefinition to avoid shallow copy.
     */
    public function __clone()
    {
        parent::__clone();
        $this->_uri = clone $this->_uri;
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Sets this request target.
     *
     * @param mixed $requestTarget The request target
     *
     * @return this For chaining
     */
    public function setRequestTarget($requestTarget)
    {
        $this->_requestTarget = $requestTarget;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestTarget()
    {
        return $this->_requestTarget;
    }

    /**
     * {@inheritdoc}
     */
    public function withRequestTarget($requestTarget)
    {
        $request = clone $this;

        return $request->setRequestTarget($requestTarget);
    }

    /**
     * Sets this request method.
     *
     * @param strin $method The method
     *
     * @return this For chaining
     *
     * @throws \InvalidArgumentException If the method is unknown
     */
    public function setMethod($method)
    {
        if (!in_array($method, self::$_methods)) {
            throw new \InvalidArgumentException(
                'Unknown '.$method.' method.',
                1207
            );
        }

        $this->_method = $method;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return $this->_method;
    }

    /**
     * {@inheritdoc}
     */
    public function withMethod($method)
    {
        $request = clone $this;

        return $request->setMethod($method);
    }

    /**
     * Sets this request URI.
     *
     * @param UriInterface $uri The new URI
     *
     * @return this For chaining
     */
    public function setUri(PSR\UriInterface $uri)
    {
        $this->_uri = $uri;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUri()
    {
        return $this->_uri;
    }

    /**
     * {@inheritdoc}
     */
    public function withUri(PSR\UriInterface $uri, $preserveHost = false)
    {
        $request = clone $this;

        if (!$preserveHost
         || !$request->hasHeader('host')
         || '' == $request->getHeader('host')) {
            $request->setHeader('host', $uri->getHost());
        }

        return $request->setUri($uri);
    }
}
