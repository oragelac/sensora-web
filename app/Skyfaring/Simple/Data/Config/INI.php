<?php

namespace Skyfaring\Simple;

class INI
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

	/**
	 * INI array representation.
	 */

	private $_ini;

	/**
	 * Missing values counter, see verify_ini().
	 */

	private $_missing;

	/**
	 * Memory of the last used / set section.
	 */

	private $_section;

	/**
	 * Constructors & Destructors w/ pseudo overloading (=> nullable args).
	 */

    public function __construct($path = null, $array = null)
    {
        if (isset($path) && isset($array)) {
            throw new \LogicException( "Needs either a path or an array, not both." );
        }

        if (isset($path)) {
            $this->open($path);
        	$this->_section = key($this->_ini);
        } elseif (isset($array)) {
            $this->replace($array);
        	$this->_section = key($_ini);
        }
    }

    public function __destruct()
    {
        unset($_ini);
        unset($_missing);
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Opens the ini file and fill the _ini array with it.
     */

    public function open($path)
    {
        if (file_exists($path)) {
            $this->_ini = parse_ini_file( $path, true );
        } else {
            throw new \DomainException("\nFile not found at $path");
        }
    }

    /**
     * Replace the current in-memory ini array with a given array.
     */

    public function replace($newIni)
    {
        if (array_depth($newIni) > 3) {
            throw new \LogicException("Given array does not fit INI format.");
        } elseif(is_null($newIni)) {
            throw new \LogicException( "Given array needs to exist to replace existing array." );
        } else {
            $this->_ini = $newIni;
        }
    }

    /**
     * Gets a specific section / value from the file.
     *
     * If the section isn't provided, an attempt to find the value from the last
     * section is done.
     *
     * Without parameters, the last used section is returned.
     *
     * If no section was previsouly used, the default FIRST section is used.
     *
     * Complexity = 2³ - 1
     */

    public function get($key = null, $section = null)
    {
    	if (isset($key)) {
    		if (isset($section)) {
    			if (isset($this->_ini[$section][$key])) {
    				return $this->_ini[$section][$key];
    			} else {
    				throw new \OutOfBoundsException("Value [$key] in [$section] does not exist.");
    			}
    		} else {
    			if (isset($this->_ini[$this->_section][$key])) {
    				return $this->_ini[$this->_section][$key];
    			} else {
    				throw new \OutOfBoundsException("Value [$key] in [DEFAULT $this->_section] does not exist.");
    			}
    		}
    	} else {
    		if(isset($section)) {
    			if(isset($this->_ini[$section])) {
    				return $this->_ini[$section];
    			} else {
    				throw new OutOfBoundsException("Section $section does not exist.");
    			}
    		} else {
    			return $this->_ini[$this->_section];
    		}
    	}
    }

    /**
     * Sets a specific section / value within the file.
     */

    public function set($section, $key, $value = null)
    {
        if (isset($value)) {
            $this->_ini[$section][$key] = $value;
        } else {
            $this->_ini[$section][$value];
        }
    }

    /**
     * Saves the configuration in the given file.
     */

    public function save($file)
    {
        $this->verify_ini();

        file_put_contents($file, $this->build_ini());
        if (file_exists($file)) return true;

        throw new \DomainException("\nAn error happened while writing $file.");
    }

    /**
     * Builds a string representation of our ini file.
     */

    private function build_ini()
    {
        $s = '';
        foreach($this->_ini as $key => $value) {
            if(is_array( $key )) {
                $s .= "\n[$key]";
                foreach( $key as $k => $v ) {
                  $s .= "\n$k = $v";
            }
            } else {
                $s .= "\n$key = $value";
            }
        }

        return $s;
    }

    /**
     * Simple k => v check. Logs the result within the server.
     */

    private function verify_ini()
    {
        $this->_missing = 0;
        foreach($this->_ini as $key => $value) {
            if(is_array($key)) {
            foreach($key as $k => $v) {
                if( !isset($v) ) $this->_missing++;
            }
            } else {
                if(!isset($value)) $this->_missing++;
            }
        }

        if($this->_missing > 0) {
            # I think it's okay if some values are empty as a general moderation
            # but I still make a log out of it.
            syslog( LOG_WARNING, "\nVerification of ini file ended with '
                                  .  $this->_missing . ' missing values" );
        }
    }

    /**
     * Stringify ALAS __toString(), but prettier.
     */

    public function stringify()
    {
        print('<pre>' . print_r($this->_ini, true) . '</pre>');
    }
}
