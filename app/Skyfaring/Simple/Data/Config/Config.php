<?php

namespace Skyfaring\Simple\Data\Config;

class Config implements \Serializable, \ArrayAccess, \IteratorAggregate
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var Directive
     */
    protected $_root = null;

    /**
     * Class constructor w\ pseudo-overloading.
     *
     * @param mixed $path The configuration filepath | array | root directive
     */
    public function __construct($value)
    {
        if (is_string($value))
        {
            $parser = new Parser();
            $value = $parser->parse($value);
        }

        if (is_array($value))
        {
            $this->_root = Directive::create('ROOT', null, $value);
            return;
        }

        if ($value instanceof Directive)
        {
            $this->_root = $value;
        }
    }

    /**
     * In case this Configuration is translated to a string,
     * we return its root value.
     *
     * @return string Obviously
     */
    public function __toString()
    {
        return (string) $this->_root;
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Sets this Configuration root directive.
     *
     * @param Directive $root The root directive.
     * @return Directive The previous root if any
     */
    public function setRoot(Directive $root)
    {
        $oldRoot = $this->_root;
        $this->_root = $root;

        return $oldRoot;
    }

    /**
     * Retreives this configuration root directive.
     *
     * @return Directive This configuration root directive
     */
    public function getRoot()
    {
        return $this->_root;
    }

    /**
     * Retreives the given key from this configuration file.
     *
     * @param string $key The searched value name
     * @return mixed The seeked value.
     */
    public function get($value)
    {
        $value = strtolower($value);
        $keyParts = explode('.', $value);

        if( count($keyParts) > 1 )
        {
            $element = $this->_root;
            foreach($keyParts as $key)
            {
                $element = $element->seek($key);
                if (!$element)
                {
                    throw new \RuntimeException(
                        'Unable to find '. $key. ' in '.$this->_root->getName().'.',
                        907
                    );
                }
            }

            $directive = $element;
        }

        else
        {
            $directive = $this->_root->seek($value);
        }

        if (!$directive)
        {
            throw new \RuntimeException(
                'Unable to locate '.$value.' in '.$this->getRoot().'.',
                908
            );
        }

        if ($directive instanceof Directive)
        {
            $config = new Config($directive);
            $config->setRoot($directive);
            return $config;
        }

        return $directive;
    }

    /**
     * Sets a new value to the given seeked variable.
     *
     * @param string $key The seeked variable
     * @param $value $value The new value
     */
    public function set($key, $value)
    {
        $keyParts = explode('.', $key);
        if( count($keyParts) > 1 )
        {
            $element = $this->_root;
            foreach($keyParts as $key)
            {
                $element = $element->seek($key);
            }

            $directive = $element;
        }

        else
        {
            $directive = $this->_root->seek($key);
        }

        if(!$directive)
        {
            throw new \RuntimeException(
                'Requested value '.$key
                .' does not exist in current configuration',
                906
            );
        }

        $directive->setValue($value);
    }

    /**
     * Deletes a directive from this configuration.
     *
     * @param string $key The directive name
     */
    public function delete($key)
    {
        $directive = $this->_root->seek($key);

        if(!$directive)
        {
            return false;
        }

        unset($directive);

        return true;
    }

/* --------------------------------------------------------- Backup handling */

    /**
     * Saves this configuration at the location specified.
     *
     * @param $filepath The file path
     */
    public function save($filepath)
    {
        $file = @fopen($filepath, 'w+');

        if (empty($file))
        {
            // LOG NOTICE SAVE FAILED
            return;
        }

        fwrite($file, $this->serialize());
        fclose($file);
    }

    /**
     * Loads this configuration from a saved file.
     *
     * @param $filepath The file path
     */
    public function load($filepath)
    {
        $file = file_get_contents($filename);
        $this->unserialize($file);
    }

/* -------------------------------------------- \Serializable Implementation */

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize($this->_root);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($data)
    {
        $this->_root = unserialize($data);
    }

/* --------------------------------------------- \ArrayAccess Implementation */

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return !empty($this->get($offset));
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        $this->delete($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

/* --------------------------------------- \IteratorAggregate Implementation */

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->_root->getChildren());
    }

/* ----------------------------------------------------------- Format shifts */

    /**
     * Returns an array representation of this configuration.
     *
     * @return array The array Representation
     */
    public function toArray()
    {
        return $this->_root->toArray();
    }

    public function toString()
    {
        return (string) $this->_root->getValue();
    }

    // TODO - YAML INI JSON (!) WAFFLE (!)
}
