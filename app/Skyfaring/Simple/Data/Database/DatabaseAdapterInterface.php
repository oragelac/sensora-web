<?php

namespace Skyfaring\Simple\Data\Database;

/**
 * General database communication contract.
 *
 * Can be implemented within various behavioural limits.
 * Create / Read / Update / Delete is the objective.
 */
interface DatabaseAdapterInterface
{
    /**
     * Establishes the database communication. The connection MUST be persistent.
     */
    public function connect();

    /**
     * Braks the database connection.
     */
    public function disconnect();

    /**
     * Prepares an sql statement.
     *
     * @param string $sql // TODO
     * @param array $options // TODO
     */
    public function prepare($sql, array $options = array());

    /**
     * Executes the previously prepaired statement.
     *
     * @param array $parameters // TODO
     */
    public function execute(array $parameters = array());

    /**
     * TODO
     */
    public function fetch( $fetchstyle = null, $cursorOrientation = null,
        $cursorOffset = null
    );

    /**
     * TODO
     */
    public function select($table, array $bind, $boolOperator = 'AND');

    /**
     * TODO
     */
    public function insert($table, array $bind);

    /**
     * TODO
     */
    public function update($table, array $bind, $where = '');

    /**
     * TODO
     */
    public function delete($table, $where = '');
}
