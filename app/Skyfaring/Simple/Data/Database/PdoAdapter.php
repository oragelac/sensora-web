<?php

namespace Skyfaring\Simple\Data\Database;

/**
 * Non-canonical PDO Adapter.
 */
class PdoAdapter implements DatabaseAdapterInterface
{
    /**
     * @var array
     */
    protected $_config = array();

    /**
     * TODO.
     */
    protected $_connection = null;

    /**
     * @var string
     */
    protected $_statement = null;

    /**
     * @var int
     */
    protected $_fetchMode = \PDO::FETCH_ASSOC;

    /**
     * Class constructor.
     *
     * @param string $dsn           The database connection string as found in \PDO
     * @param string $username      The database user
     * @param string $password      The database password
     * @param array  $driverOptions
     */
    public function __construct( $dsn, $username = null, $password = null,
        array $driverOptions = array())
    {
        $this->_config = compact('dsn', 'username', 'password', 'driverOptions');
    }


    /**
     * Retrieves the statement.
     *
     * @return \PDOStatement
     */
    public function getStatement()
    {
        if(null === $this->_statement)
        {
            throw new \PDOEException(
                "There is no PDOStatement opbject for use.",
                1400 // TODO Verify this number
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function connect()
    {
        if ($this->_connection)
        {
            return;
        }

        $this->_connection = new \PDO(
            $this->_config['dsn'],
            $this->_config['username'],
            $this->_config['password'],
            $this->_config['driverOptions']
        );

        $this->_connection->setAttribute(
            \PDO::ATTR_ERRMODE,
            \PDO::ERRMODE_EXCEPTION
        );

        $this->_connection->setAttribute(\PDO::ATTR_EMULATES_PREPARES, false);
    }


    /**
     * {@inheritdoc}
     */
    public function disconnect()
    {
        $this->_connection = null;
    }

    /**
     * {@inheritdoc}
     *
     * This method implements a fluent interface.
     */
    public function prepare($sql, array $options = array())
    {
        $this->connect();
        $this->_statement = $this->_connection->prepare($sql, $options);
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * This method implements a fluent interface.
     */
    public function execute(array $parameters = array())
    {
        $this->getStatement()->execute($parameters);
    }

    /**
     * Retrieves the number of affected rows.
     *
     * @return integer The affected rows
     */
    public function affectedRows()
    {
        return $this->getStatement()->rowCount();
    }

    /**
     * Retrieves the last inserted id.
     *
     * @param string $name The table name
     *
     * @return int The last isnerted id
     */
    public function lastInsertId($name = null)
    {
        $this->connect();
        return $this->_connection->lastInsertId($name);
    }

    /**
     * Fetchs a value from the statement.
     *
     * @param integer $fetchstyle The \PDO constant if any
     * @param string $cursorOrientation The \PDO fetching orientation if any
     *
     * @return array The fetched value. // TODO verify
     */
    public function fetch( $fetchstyle = null, $cursorOrientation = null,
        $cursorOffset = null)
    {
        if (null === $fetchstyle)
        {
            $fetchstyle = $this->_fetchMode;
        }

        return $this->getStatement()->fetch(
            $fetchstyle,
            $cursorOrientation,
            $cursorOffset
        );
    }

    /**
     * Fetchs the values from the statement.
     *
     * @param integer $fetchstyle The \PDO constant if any
     * @param string $cursorOrientation The \PDO fetching orientation if any
     *
     * @return array The fetched values. // TODO verify
     */
    public function fetchAll($fetchStyle = null, $column = 0)
    {
        if(null === $fetchStyle)
        {
            $fetchStyle = $this->_fetchMode;
        }

        return $fetchStyle === \PDO::FETH_COLUMN
            ? $this->getStatement()->fetchAll($fetchStyle, $column)
            : $this->getStatement()->fetchAll($fetchStyle)
    }

    /**
     *
     */
    public function select($table, array $bind = array(), $boolOperator = 'AND')
    {
        if ($bind)
        {
            $where = array();

            foreach ($bind as $column => $value)
            {
                unset($bind[$column]);
                $bind[':' . $column] = $value;
                $where[] = $column . ' = :' . $column;
            }
        }

        $sql = 'SELECT * FROM' . $table . ( ($bind)
            ? ' WHERE ' . implode(' ' . $boolOperator . ' ', $where)
            : ' '
        );

        $this->prepare($sql)->execute();

        return $this;
    }
}
