<?php

namespace Skyfaring\Simple\Data;

/**
 * ParameterBag is a key/value store.
 *
 * Inspired by SymfonyComponents ParameterBag class.
 */
class Collection implements \IteratorAggregate, \Countable
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var array
     */
    protected $_parameters          = [];

    /**
     * Class constructor.
     *
     * @param array
     */
    public function __construct(array $parameters = array())
    {
        $this->_parameters = $parameters;
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Returns the parameters.
     *
     * @return array An array of parameters
     */
    public function all()
    {
        return $this->_parameters;
    }

    /**
     * Returns the parameter keys.
     *
     * @return array An array of parameter keys
     */
    public function keys()
    {
        return array_keys($this->_parameters);
    }

    /**
     * Checks wheter or not the parameter key exists.
     *
     * @return boolean Key existence
     */
    public function has($key, $caseSensitive = true)
    {
        if ($caseSensitive) {

            return key_exists($key, $this->_parameters);

        } else {

            $keys = array_keys($this->_parameters);
            $map = array();
            foreach ($keys as $mkey) {
                $map[strtolower($mkey)] = $mkey;
            }

            return key_exists($key, $map);

        }
    }

    /**
     * Replaces the current parameters by a new set.
     *
     * @param array $parameters The new parameter array
     */
    public function replace(array $parameters = array())
    {
        $this->_parameters = $parameters;
    }

    /**
     * Adds parameters.
     * If a key exists in both new and current parameters array, the new value
     * WILL overwrite the current value.
     *
     * @param array $parameters The new parameters array
     */
    public function add(array $parameters = array())
    {
        $this->_parameters = array_merge($this->_parameters, $parameters);
    }

    /**
     * Returns a parameter by name.
     *
     * @param string $key The parameter key
     * @param mixed $default The default value if the parameter key does not exist
     */
    public function get($key, $default = null)
    {
        return array_key_exists($key, $this->_parameters) ? $this->_parameters[$key] : $default;
    }

    /**
     * Sets a parameter by name.
     *
     * @param string $key The parameter key
     * @param mixed $value The parameter value
     */
    public function set($key, $value)
    {
        $this->_parameters[$key] = $value;
    }

    /**
     * Removes a parameter.
     *
     * @param string $key The parameter key
     * @return mixed The removed parameter
     */
    public function remove($key)
    {
        $retval = $this->_parameters[$key];
        unset($this->_parameters[$key]);

        return $retval;
    }

    /**
     * Returns an iterator for parameters.
     *
     * @return \ArrayIterator The parameter array \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->_parameters);
    }

    /**
     * Counts the number of parameters.
     *
     * @return int The number of parameters
     */
    public function count()
    {
        return count($this->_parameters);
    }
}
