<?php

namespace Skyfaring\Simple\Data\Node;

class NodeFactory
{
    /**
     * Returns the selected Network type from the given array.
     *
     * @param string $type The selected type
     * @param array $array The array to build the Node Network from.
     */
    public static function getNetwork($type, array $array)
    {
        $retval = null;

        switch (strtolower($type)) {
            case 'treenetwork':
            case 'treenode':
            case 'tree':
                $retval = TreeNode::create('ROOT', $array, null);
                break;
            
            case 'graphnetwork':
            case 'graphnode':
            case 'graph':
                // $retval = self::getGraph($array); TODO - Implement
                break;

            case 'flowgraphnetwork':
            case 'flowgraphnode':
            case 'flowgraph':
                // $retval = self::getFlowGraph($array); TODO - Implement
                break;

            default:
                throw new \InvalidArgumentException(
                    'Unknown Node network '.$type.' requested.',
                    900
                );
                break;
        }

        return $retval;
    }
}