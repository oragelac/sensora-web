<?php

namespace Skyfaring\Simple\Data\Node;

class GraphNode extends Node implements GraphNodeInterface
{
    const ANNOTATION = '@';

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var array
     */
    protected $_parents = array();

    /**
     * Class constructor.
     *
     * @param $value This node value
     * @param Node[] $parents This node parents if any
     * @param Node[] $children This node children if any
     */
    public function __construct($value, $parents = array(), $children = array())
    {
        parent::__construct($value);
        $this->setParents($parents);
        $this->setChildren($children);
    }

    /**
     * Class destructor.
     *
     * Removes family (parents / children) relations.
     */
    public function __destruct()
    {
        // foreach($this->_parents as $parent)
        // {
        //     $parent->removeChild($this);
        // }

        // foreach($this->_children as $child)
        // {
        //     $child->removeParent($this);
        // }

        // unset($this->_parents);
        // unset($this->_children);
        // unset($this->_value);
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * {@inheritdoc}
     */
    public function orphan()
    {
        return empty($this->_parents);
    }

    /**
     * {@inheritdoc}
     */
    public function addChild(NodeInterface $child)
    {
        if(!in_array($child, $this->_children))
        {
            $this->_children[] = $child;
        }
    }

    /**
     * TODO INTERFACE
     */
    public function hasChild(NodeInterface $child)
    {
        foreach($this->_children as $thisChild)
        {
            if ($child == $thisChild)
            {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function removeChild(NodeInterface $child)
    {
        foreach($this->_children as $key => $thisChild) {
            if ($child == $thisChild) {
                unset($this->_children[$key]);
                break;
            }
        }

        $this->_children = array_values($this->_children);
        $child->removeParent($this);

        return $child;
    }

    /**
     * {@inheritdoc}
     */
    public function removeChildren()
    {
        foreach ($this->_children as $child) {
            $child->removeParent($this);
        }

        $children = $this->_children;
        $this->_children = array();

        return $children;
    }

    /**
     * {@inheritdoc}
     */
    public function setChildren(array $children)
    {
        foreach($this->_children as $child)
        {
            $child->removeParent($this);
        }

        $oldChildren = $this->_children;
        $this->_children = array();        

        foreach($children as $child)
        {
            $this->addChild($child);
        }

        return $oldChildren;
    }

    /**
     * {@inheritdoc}
     */
    public function getChildren()
    {
        return $this->_children;
    }

    /**
     * {@inheritdoc}
     */
    public function getNeighbors()
    {
        $neighbors = array();
        foreach ($this->_parents as $parent) {
            $neighbors = array_merge($neighbors, $parent->getChildren());
        }

        return $neighbors;
    }

    /**
     * {@inheritdoc}
     */
    public function getNeighborsAndSelf()
    {
        $neighborsAndSelf = $this->getNeighbors(); 
        array_unshift($neighborsAndSelf, $this);
        return $neighborsAndSelf;
    }

    /**
     * {@inheritdoc}
     */
    public function addParent(NodeInterface $parent)
    {
        if(!in_array($parent, $this->_parents))
        {
            $this->_parents[] = $parent;
        }
    }

    /**
     * TODO INTERFACE
     */
    public function hasParent(NodeInterface $parent)
    {
        foreach($this->_parents as $thisParent)
        {
            if ($parent == $thisParent)
            {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function removeParent(NodeInterface $parent)
    {
        foreach ($this->_parents as $key => $thisParent) 
        {
            if ($parent == $thisParent)
            {
                unset($this->_parents[$key]);
            }
        }

        return $parent;
    }

    /**
     * {@inheritdoc}
     */
    public function removeParents()
    {
        $parents = $this->_parents;
        $this->_parents = array();
        return $parents;
    }

    /**
     * {@inheritdoc}
     */
    public function setParents(array $parents)
    {
        foreach($this->_parents as $parent)
        {
            $parent->removeChild($this);
        }

        $oldParents = $this->_parents;
        $this->_parents = array();

        foreach($parents as $parent)
        {
            $this->addParent($parent);
        }        

        return $oldParents;
    }

    /**
     * {@inheritdoc}
     */
    public function getParents()
    {
        return $this->_parents;
    }

    /**
     * {@inheritdoc}
     */
    public function isRoot()
    {
        return empty($this->_parents);
    }
}