<?php

namespace Skyfaring\Simple\Data\Node;

interface TreeNodeInterface extends NodeInterface
{
    /**
     * Removes this node current parent.
     *
     * @return NodeInterface The removed parent
     */
    public function removeParent();

    /**
     * Replaces this node current parent.
     *
     * @param NodeInterface The new parent
     */
    public function setParent(NodeInterface $parent);

    /**
     * Retreives this node parent.
     *
     * @return NodeInterface The parent
     */
    public function getParent();

    /**
     * Returns true if root node, false otherwise.
     *
     * @return bool Wheter this node is a root node.
     */
    public function isRoot();
}