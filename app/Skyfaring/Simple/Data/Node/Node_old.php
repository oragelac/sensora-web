<?php

namespace Skyfaring\Simple\YAML;

class Node implements RecursiveIterator
{
    const ANY = 'ANY';
    const ANNOTATION = 'ANNOTATION';

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var string
     */
    protected $_type = null;

    /**
     * @var string
     */
    protected $_name = null;

    /**
     * @var string
     */
    protected $_immutable = null;

    /**
     * @var array
     */
    protected $_origins = array();

    /**
     * @var array
     */
    protected $_seeds = array();

    /**
     * Class constructor.
     */
    public function __construct($type, $immutable = true, $name = null)
    {
        if ($immutable && !empty($name))
        {
            throw new \LogicException(
                'Node set to immutable yet a name was given.',
                905
            );
        }

        $this->setType($type);
        $this->_immutable = $immutable;
        $this->_name = $name;
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Name getter.
     *
     * @return string The name OR false for immutable Nodes
     */
    public function getName()
    {
        if ($this->_immutable) {
            return false;
        }

        return $this->_name;
    }

    /**
     * Name setter.
     *
     * @param string $name The name
     *
     * @throws \LogicException for method call with Node immutability
     */
    public function setName()
    {
        if($this->_immutable) {
            throw new \LogicException(
                'Node set to immutable.',
                905
            );
        }

        $this->_name = $name;
    }

    /**
     * Value getter.
     *
     * @return mixed The value
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * Value setter.
     *
     * @param mixed $value The value
     */
    public function setValue($value)
    {
        $this->_value = $value;
    }

    /**
     * Type getter.
     *
     * @return string The type
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * Type setter. Updates parents and children.
     *
     * @param string $type The type
     */
    public function setType($type)
    {
        foreach ($this->_origins as $origin) {
            $origin->detachSeed($this);
        }
        foreach ($this->_seeds as $seed) {
            $seed->detachOrigin($this);
        }

        $this->_type = $type;

        foreach ($this->_origins as $origin) {
            $origin->addSeed($this);
        }
        foreach ($this->_seeds as $seed) {
            $seed->addOrigin($this);
        }
    }

    /**
     * Seeds getter.
     */
    public function getSeeds()
    {
        return $this->_seeds;
    }

    /**
     * Origins getter.
     */
    public function getOrigins()
    {
        return $this->_origins;
    }

    /**
     * Checks wheter this node is an acceptable parent for the given node.
     *
     * @param Node $node The node
     *
     * @return bool
     */
    public function ascends(Node $node)
    {
        foreach ($node->getOrigins() as $nodeOrigin) {
            if ($this->_type == $nodeOrigin->getType()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks wheter this node is an acceptable origin for the given node.
     *
     * @param Node $node The node
     *
     * @return bool
     */
    public function descends(Node $node)
    {
        foreach ($node->getSeeds() as $nodeSeed) {
            if ($this->_type == $nodeSeed->getType()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Adds a node to this node seeds.
     *
     * @param Node $node The node
     *
     * This method implements a fluent interface
     */
    public function addSeed(Node $node)
    {
        $node->addOrigin($this);
        $this->_seeds[$node->getType()] = $node;

        return $this;
    }

    /**
     * Detaches a seed from this Node seeds.
     *
     * @param Node $node The seed
     */
    public function detachSeed(Node $node)
    {
        $node->detachOrigin($this);
        unset($this->_seeds[$node->getType()]);
    }

    /**
     * Adds a node to this node origins.
     *
     * @param Node $node The node
     *
     * This method implements a fluent interface
     */
    public function addOrigin(Node $node)
    {
        $node->addSeed($this);
        $this->_origins[$node->getType()] = $node;

        return $this;
    }

    /**
     * Detaches an origin from this node origins.
     *
     * @param Node $node the origin
     */
    public function detachOrigin(Node $node)
    {
        $node->detachSeed($this);
        unset($this->_origins[$node->getType()]);
    }

    /**
     * Says wheter this variable is immutable or not.
     *
     * @return bool
     */
    public function immutable()
    {
        return $this->_immutable;
    }

/* ========================================================================== */
/* === INTERFACE IMPLEMENTATIONS ============================================ */
/* ========================================================================== */

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        reset($this->_seeds);
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return current($this->_seeds);
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return key($this->_seeds);
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return null !== key($this->_seeds);
    }

    /**
     * {@inheritdoc}
     */
    public function getChildren()
    {
        return $this->current();
    }

    /**
     * {@inheritdoc}
     */
    public function hasChildren()
    {
        return $this->current() instanceof Node;
    }
}
