<?php

namespace Skyfaring\Simple\Data\Node;

interface GraphNodeInterface extends NodeInterface
{
    /**
     * Adds a parent to this node parents.
     *
     * @param NodeInterface $parent The new parent
     */
    public function addParent(NodeInterface $parent);

    /**
     * Removes a parent from this node parents.
     *
     * @param NodeInterface $parent The parent to remove
     * @return NodeInterface The removed parent
     */
    public function removeParent(NodeInterface $parent);

    /**
     * Removes this node parents.
     *
     * @return array The removed parents
     */
    public function removeParents();

    /**
     * Sets this node direct parent.
     *
     * @param array $parents The new parent
     * @return Node[] The old parents
     */
    public function setParents(array $parents);

    /**
     * Retreives this node parent(s).
     *
     * @return array The parents
     */
    public function getParents();

    /**
     * Returns true if root node, false otherwise.
     *
     * @return bool Wheter this node is a root node.
     */
    public function isRoot();
}