<?php

namespace Skyfaring\Simple\Data\Node;

/**
 * This class represents the Tree Data Structure.
 *
 * Main difference with the Graph structure being that only one parent per node
 * is allowed.
 */
class TreeNode extends Node implements TreeNodeInterface
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var NodeInterface
     */
    protected $_parent = null;

    /**
     * Class constructor.
     */
    public function __construct($value, $parent = null, $children = array())
    {
        parent::__construct($value);
        $this->setParent($parent);
        $this->setChildren($children);
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Tiny factory to ease up chain Node creation.
     *
     * @param mixed $value The node value
     * @param mixed $array The - potential - array
     * @param TreeNodeInterface $parent The $parent
     */
    public static function create($name, $evaluated, TreeNodeInterface $parent)
    {
        $node = new TreeNode(null, $parent);

        if (is_array($evaluated)) {
            $node->setValue($name);
            foreach($evaluated as $key => $child) {
                $node->addChild(self::create($key, $child, $node));
            }
        } else {
            $node->setValue($evaluated);
        }

        return $node;
    }

    /**
     * {@inheritdoc}
     */
    public function removeParent()
    {
        $this->_parent->removeChild($this);
        $oldParent = $this->_parent;
        
        $this->_parent = null;

        return $oldParent;
    }

    /**
     * {@inheritdoc}
     */
    public function setParent(NodeInterface $parent)
    {
        $this->_parent->removeChild($this);
        $oldParent = $this->_parent;
        
        $this->_parent = $parent;
        $this->_parent->addChild($this);
    
        return $oldParent;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return $this->_parent;
    }

    /**
     * {@inheritdoc}
     */
    public function isRoot()
    {
        return empty($this->_parent);
    }

    /**
     * {@inheritdoc}
     */
    public function addChild(NodeInterface $child)
    {
        if (!in_array($child, $this->_children)) {
            $this->_children[] = $child;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeChild(NodeInterface $child)
    {
        foreach ($this->_children as $key => $thisChild) {
            if ($child == $thisChild) {
                $this->_children[$key]->removeParent();
                unset($this->_children[$key]);
                break;
            }
        }

        return $child;
    }

    /**
     * {@inheritdoc}
     */
    public function removeChildren()
    {
        foreach ($this->_children as $child) {
            $child->removeParent();
        }

        $children = $this->_children;
        $this->_children = array();
        
        return $children;
    }

    /**
     * {@inheritdoc}
     */
    public function setChildren(array $children)
    {
        foreach ($children as $child) {
            $child->setParent($this);
        }

        $oldChildren = $this->_children;
        $this->_children = $children;

        return $oldChildren;
    }

    /**
     * {@inheritdoc}
     */
    public function getChildren()
    {
        return $this->_children;
    }

    /**
     * {@inheritdoc}
     */
    public function getNeighbors()
    {
        if ($this->isRoot()) {
            return null;
        }

        $neighbors = getNeighborsAndSelf();
        foreach ($neighbors as $key => $neighbor) {
            if ($this == $neighbor) {
                unset($neighbors[$key]);
                break;
            }
        }

        return $neighbors;
    }

    /**
     * {@inheritdoc}
     */
    public function getNeighborsAndSelf()
    {
        if ($this->isRoot()) {
            return $this;
        }

        return $this->_parent->getChildren();
    }

    public function seek($value, $direction = true)
    {
        $found = false;

        if ($direction) {
            $array = $this->getChildren();
        } else {
            $array = $this->getAncestors();
        }

        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveArrayIterator($array),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach($iterator as $node)
        {
            if ($node->getValue() == $value) {
                $found = $node;
                break;
            }
        }

        return $found;
    }
}