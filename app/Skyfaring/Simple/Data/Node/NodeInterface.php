<?php

namespace Skyfaring\Simple\Data\Node;

/**
 * Interface for general purpose nodes.
 *
 * The very fact that parenting and neighbors may vary from a node type to 
 * another contribute to render any ancestor-related method irrelevant.
 *
 * That is to say : This interface is worthless by itself.
 */
interface NodeInterface extends \Countable, \IteratorAggregate
{
    /**
     * Sets the node value.
     *
     * @param mixed $value The value
     */
    public function setValue($value);

    /**
     * Retreives the node value.
     *
     * @return mixed The value
     */
    public function getValue();

    /**
     * Adds a child to this node children.
     *
     * @param NodeInterface $child The child
     */
    public function addChild(NodeInterface $child);

    /**
     * Removes a child from this node children.
     *
     * @param NodeInterface $child The child
     * @return NodeInterface The removed child 
     */
    public function removeChild(NodeInterface $child);

    /**
     * Removess every children from node.
     *
     * @return array The children
     */
    public function removeChildren();

    /**
     * Sets this node children.
     *
     * @param Node[] The new children
     * @return Node[] The previous children
     */
    public function setChildren(array $children);

    /**
     * Retreives this node children.
     *
     * @return Node[] The children
     */
    public function getChildren();

    /**
     * Retreives all the neighbors.
     *
     * @return Node[]
     */
    public function getNeighbors();

    /**
     * Retreives all the neighbors and self.
     *
     * @return Node[]
     */
    public function getNeighborsAndSelf();

    /**
     * Returns true if leaf node, false otherwise.
     *
     * @return bool Wheter this node is a leaf node.
     */
    public function isLeaf();
}