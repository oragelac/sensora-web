<?php

namespace Skyfaring\Simple\Data;

class Blueprint implements \Serializable
{
    protected static $_const = array('ANNOTATION', 'ANY');

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var string
     */
    protected $_file = null;

    /**
     * @var Node
     */
    protected $_root;

    /**
     * Class constructor.
     *
     * Takes a file path as parameter, opens it, parses it and creates a node
     * array with it.
     *
     * @param string $filepath The file path
     */
    public function __construct($filepath)
    {
        $check = @explode('.', $filepath);
        if (!is_string($filepath) || count($check) > 3
            || $check[1] != 'blueprint' || $check[2] != 'yml') {
            throw new \RuntimeException(
                'Incorrect blueprint file ['.$filepath.'] fed to Blueprint',
                904
            );
        }

        $this->_file = $filepath;
        $this->_root = new Node('ROOT');

        $this->parse(yaml_parse_file($filepath));
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Parses the fed yaml into a Node network.
     *
     * @param array $nodes The yaml array
     */
    protected function parse(array $nodes)
    {
        $iterator = new \RecursiveIteratorIterator(
            $this->_root,
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($nodes as $nodeType => $nodeContent)
        {
            $iterator->rewind();
            foreach ($iterator as $registeredNode)
            {
                if ($registeredNode->getType() == $nodeContent['context'])
                {
                    $registeredNode->addSeed(new Node(
                        $noteType,
                        $immutable = $nodeContent['immutable'] == 'true'
                    ));
                }
            }
        }

    }

    public function check(array $array)
    {
        $check = new \RecursiveIteratorIterator(
            new \RecursiveArrayIterator($array),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        $rule = new \RecursiveIteratorIterator(
            $this->_root,
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach($check as $directive => $content)
        {
            # Context (aka parent key) retreival
            if(1 == $check->getDepth())
            {
                $context = 'ROOT';
            }

            else
            {
                
            }

            $rule->rewind();
            foreach ($rule as $node)
            {
                if($node->getType() == $context)
                {
                    $seeds = $node->getSeeds();
                    
                    # If we don't find the directive in this context possibilites,
                    # we check for possible mutable directive.
                    if( !in_array($directive, array_keys($seeds)) )
                    {
                        $mutableSeed = false;
                        foreach($seeds as $seed)
                        {
                            if (!$seed->$immutable())
                            {
                                $mutableSeed = true;
                                continue 1;
                            }
                        }

                        # If there is a mutable seed, we consider the directive
                        # to be in the correct context.
                        if ($mutableSeed)
                        {
                            continue 1;
                        }

                        throw new \RuntimeException(
                            'Directive '.$directive.' is out of context.'.PHP_EOL
                            .'Possible directives for '.$context.' : '
                            .'<pre>'.print_r($array_keys($seeds), true).'</pre>'.PHP_EOL,
                            906
                        );
                    }

                    # Else the given directive is in the correct context.
                    else
                    {
                        continue 1;
                    }
                }
            }
            
            $context = $directive;
        }
    }

}
