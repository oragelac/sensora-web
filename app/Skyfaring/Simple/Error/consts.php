<?php

namespace Skyfaring\Simple\Error;

# ============================================================================ #
# SERVER TO HTTP ERROR TRANSLATION
# ============================================================================ #

define('Skyfaring\Simple\Error\ERROR_MAP', serialize(array(
    '1001'  => '500', // Unavailable Route file - runtime error
    '1002'  => '404', // Undefined Route - client error
    '1003'  => '400', // Unrecognized HTTP method - client error
    '1004'  => '500', // Undefined Controller - conf error
    '1005'  => '500', // Undefined Action for Controller - dev error
    '1006'  => '405', // Unapropirate request to route
    '1101'  => '500', // Unable to load views.yml - runtime error
    '1102'  => '500', // Invalid kvstore given to Document - dev error
    '1201'  => '505', // Unsupported HTTP scheme - client/dev error
    '1202'  => '500', // Invalid hostname // TODO CHECK
    '1203'  => '500', // Invalid port // TODO CHECK
    '1204'  => '500', // Invalid path // TODO CHECK
    '1205'  => '500', // Invalid query string // TODO CHECK
    '1206'  => '500', // Invalid header - dev error (should) // TODO CHECK
    '1207'  => '500', // Invalid request method - dev error // TODO CHECK
    '1208'  => '500', // Invalid HTTP Status code - dev error // TODO CHECK
    '1301'  => '500', // Unsupported Stream mode - dev error
    '1302'  => '500', // File opening error - runtime error
    '1303'  => '500', // Seek command on closed file - dev error
    '1304'  => '500', // Seek command failed on file - runtime error
    '1305'  => '500', // Rewind command failed on file - runtime error
    '1306'  => '500', // Unable to write to file - runtime error
    '1307'  => '500', // File is not writable - dev/runtime error
    '1308'  => '500', // Unable to truncate file - runtime error
    '1309'  => '500', // Unable to read file - runtime error
    '1401'  => '500', // Invalid template variable - dev error
    '1402'  => '500', // Attempted swap on undeclared content - dev error
    '1403'  => '500' // Invalid css - dev error
)));

# ============================================================================ #
# CLIENT ERROR TEXT
# ============================================================================ #

define('Skyfaring\Simple\Error\ERROR_MSG', serialize(array(
    '500'   => '<h1>Hmmmm...</h1><p>Yeah. It sems like something broke.</p>',
    '400'   => '<h1>Hum.</h1> You tried something unauthorized here.',
    '404'   => '<h1>Oops!</h1><p>We can\'t seem to find the page you\'re looking for.</p>',
)));
