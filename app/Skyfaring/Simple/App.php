<?php

namespace Skyfaring\Simple;

use Skyfaring\Simple as S;
use Skyfaring\Simple\Routing as SRouting;
use Skyfaring\Simple\HTTP as SHTTP;
use Skyfaring\Simple\MVC\Controller as SController;
use Skyfaring\Simple\Error as SError;
use Skyfaring\Simple\Parser as SParser;

class App
{
    protected static $CONF_FILE = null;
    protected static $CONF_SAVE = null;

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var array
     */
    protected static $_required = array(
        'helpers.php',
        'config/consts.php',
        'config/config.php',
        'Autoloader.php'
    );

    /**
     * @var Config
     */
    protected static $_configuration = null;

    /**
     * @var Router
     */
    public static $_router = null;

    /**
     * @var Request
     */
    public static $_request = null;

    /**
     * @var string
     */
    protected static $_env = null;

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Initializes the application.
     */
    public static function create()
    {
        self::bootstrap();
        session_start();

        self::$_router = new SRouting\Router();
        self::$_request = SHTTP\Connection::getRequest();
    }

    /**
     * Initializes system components such as configuration files, classes,
     * Autoloader, so on so forth.
     */
    protected static function bootstrap()
    {
        foreach (self::$_required as $file)
        {
            require_once $file;
        }

        // Vendor libs inclusion
        if(file_exists(VENDOR_ROOT))
        {
            require_once VENDOR_ROOT.'/autoload.php';
        }

        Autoloader::register();

        self::$CONF_FILE = CONFIG_ROOT.'/config.yml';
        self::$CONF_SAVE = SYSTEM_ROOT.'/conf.save';

        if(!file_exists(self::$CONF_SAVE)
        || (int)@filemtime(self::$CONF_FILE) > (int)@filemtime(self::$CONF_SAVE))
        {
            self::$_configuration = new S\Data\Config\Config(self::$CONF_FILE);
            self::$_configuration->save(self::$CONF_SAVE);
        }

        else
        {
            self::$_configuration = new S\Data\Config\Config(S\Data\Config\Directive::create(''));
            self::$_configuration->unserialize(self::$CONF_SAVE);
        }

        self::$_env = self::$_configuration->get('environment');

        require_once 'Error/ErrorController.php';
        SError\ErrorController::register();
    }

    /**
     * Starts the application.
     */
    public static function start()
    {
        self::$_router->route();
    }

    /**
     * Returns the running router.
     *
     * @return Router
     */
    public static function getRouter()
    {
        return self::$_router;
    }

    /**
     * Returns the active request.
     *
     * @return Request
     */
    public static function getRequest()
    {
        return self::$_request;
    }

    /**
     * Returns the active configuration.
     */
    public static function getConfiguration()
    {
        return self::$_configuration;
    }
}
