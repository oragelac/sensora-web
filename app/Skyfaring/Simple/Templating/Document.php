<?php

namespace Skyfaring\Simple\Templating;

use Skyfaring\Simple as S;

class Document
{
    /**
     * @var boolean
     */
    static protected $INITIALIZED = false;

    /**
     * @var Twig_Loader_Filesystem
     */
    static protected $TWIG_LOADER = null;

    /**
     * @var Twig_Environment
     */
    static public $TWIG_ENVIRONMENT = null;

    /**
     * @var array
     */
    protected static $_traits = array(
        'charset', 'title', 'description',
    );

    /**
     * @var array
     */
    protected static $_habits = array(
        'css', 'js'
    );

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var Directive
     */
    protected $_specification = null;

    /**
     * @var Twig_Environment
     */
    protected $_render = null;

    /**
     * @var array
     */
    protected $_context = array();

    /**
     * @var Component[]
     */
    protected $_design = null;

    /**
     * @var string
     */
    protected $_active = null;

    /**
     * @var boolean
     */
    protected $_toSanitize = true;

    /**
     * Class constructor.
     *
     * Takes a Configuration position ~ Directive reference ~ as parameter.
     *
     * @param Directive The directive reference
     */
    public function __construct(S\Data\Config\Directive $directive)
    {
        self::initialize();

        $this->_specification = new S\Data\Config\Config($directive);

        $this->_render = self::$TWIG_ENVIRONMENT->loadTemplate('html.twig');

        foreach(self::$_traits as $trait)
        {
            $holder = $this->_specification->get($trait);
            if(!is_string($holder)) $holder = $holder->toString();

            $this->_context[strtolower($trait)] = $holder;
        }

        foreach(self::$_habits as $habit)
        {
            $this->_context[$habit] = @$this->_specification->get($habit)->toArray()[$habit];
        }

        foreach($this->_specification->get('file') as $design)
        {
            $this->_design[$design->getName()] = $design;
        }

        reset($this->_design);
        $this->_active = key($this->_design);
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->_render->render();
    }

    /**
     * Returns a stream representation of this document render.
     *
     * @return PSR\StreamInterface
     */
    public function toStream()
    {
        $stream = S\Stream\StreamFactory::getMemoryStream();
        $stream->write($this->_render->render($this->_context));

        return $stream;
    }

    /**
     * Loads the static $_squeletton Template if not loaded.
     */
    protected static function initialize()
    {
        if(!self::$INITIALIZED)
        {
            self::$TWIG_LOADER = new \Twig_Loader_Filesystem(
                unserialize(\TEMPLATE_ROOT)
            );

            self::$TWIG_ENVIRONMENT = new \Twig_Environment(self::$TWIG_LOADER, array(
                'cache' => \CACHE_PATH
            ));

            self::$INITIALIZED = true;
        }
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Sets this document specification.
     *
     * @param Directive The specification
     */
    public function setSpecificarion(S\Data\Config\Directive $specification)
    {
        $this->_specification = $specification;
    }

    /**
     * Retreives this document specification.
     *
     * @return Directive The specification
     */
    public function getSpecificarion()
    {
        return $this->_specification;
    }

    /**
     * Adds a design to this document possible designs.
     *
     * @param string $name The requested design to add
     */
    public function addDesign($name, \Twig_TemplateInterface $design)
    {
        $this->_design[$name] = $design;
    }

    /**
     * Adds a component to this webpage components.
     */
    public function addContext($name, $value)
    {
        $this->_context[$name] = $value;
    }

    /**
     * Adds many different values to this document context.
     */
    public function addContexts(array $array)
    {
        $this->_context = array_merge($this->_context, $array);
    }

    /**
     * Retreives the contexts.
     *
     * @return array
     */
    public function getContexts()
    {
        return $this->_context;
    }

    /**
     * Removes a design from this document possible designs.
     *
     * @param string $name The requested design to remove
     */
    public function removeDesign($name)
    {
        if (in_array($name, $this->_design))
        {
            unset($this->_design[$name]);
        }

        else
        {
            throw \LogicException(
                'Cannot remove design '.$name.' as it does not exist in document.',
                1402
            );
        }
    }

    /**
     * Retreives a design.
     *
     * @param string $name The requested design name if any
     */
    public function getDesign($name = null)
    {
        if (empty($name)) $name = $this->_active;
        return @$this->_design[$name];
    }

    /**
     * Returns the design array.
     *
     * @return array The designs
     */
    public function getDesigns()
    {
        return $this->_design;
    }

    /**
     * Sets the current active content.
     *
     * @param string $name The requested active design
     */
    public function draft($name = null)
    {
        if(empty($name))
        {
            reset($this->_design);
            $this->_active = key($this->_design);
            return;
        }

        foreach ($this->_design as $designName => $design)
        {
            if ($name == $designName)
            {
                $this->_active = $name;
                $this->_context['page_content'] = $this->_design[$this->_active];
                return;
            }
        }

        throw new \LogicException(
            'Cannot activate design '.$name.' as it does not exist in document.',
            1403
        );
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        return $this->_render->render($this->_context);
    }
}
