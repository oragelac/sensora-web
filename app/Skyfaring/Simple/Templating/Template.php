<?php

namespace Skyfaring\Simple\Templating;

/**
 * Describes a Template behaviour.
 */
interface Template
{
    /**
     * This method SHOULD outputs the CURRENT state of the template as a string.
     *
     * This method MUST NOT raise an exception in order to conform with PHP's
     * string casting operations.
     *
     * @see http://php.net/manual/en/language.oop5.magic.php#object.tostring
     *
     * @return string
     */
    public function __tostring();

    /**
     * Resets template state to its initialized state.
     *
     * @return void|object For chaining|false On failure
     */
    public function reset();

    /**
     * Operates any necessary action to prepair the template for rendering.
     *
     * @return Stream The rendered content
     */
    public function compose();

    /**
     * Analog function to compose but returns instead a string.
     *
     * @return string The rendered content
     */
    public function render();

    /**
     * Defines wheter or not the template should remove its sanitizing.
     *
     * @param boolean
     */
    public function toSanitize($var);
}
