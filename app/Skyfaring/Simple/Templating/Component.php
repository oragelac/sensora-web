<?php

namespace Skyfaring\Simple\Templating;

use Skyfaring\Simple\Stream as SimpleStream;

/**
 * Describe an html component.
 *
 * Typically, this class opens a Skyfaring\Simple\Stream\Stream file, places it
 * in memory an replace {VARIABLE} parameters by their runtime values.
 *
 * @see Skyfaring\Simple\View\View
 * @see Skyfaring\Simple\Stream\Stream
 */
class Component implements Template
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * Template file stream.
     *
     * @var Skyfaring\Simple\Stream\Stream
     */
    protected $_template = null;

    /**
     * In memory (php://memory) template copy.
     *
     * @var Skyfaring\Simple\Stream\Stream
     */
    protected $_render = null;

    /**
     * Key-value store.
     *
     * @var array
     */
    protected $_variables = [];

    /**
     * @var boolean
     */
    protected $_toSanitize = true;

    /**
     * Class constructor.
     *
     * Creates a stream from file $file at location $path.
     * Copy array $args to internal key-value store $_variables.
     *
     * @param string Path of the file
     */
    public function __construct($file, $args = false, $path = null)
    {
        if (null === $path)
        {
            $paths = unserialize(TEMPLATE_ROOT);
        }

        $filepath = null;
        foreach ($paths as $path)
        {
            if(file_exists("$path/$file"))
            {
                $filepath = $path;
                break;
            }
        }

        $this->_template = SimpleStream\StreamFactory::getFileStream("$filepath/$file");
        $this->_render = SimpleStream\StreamFactory::getMemoryStream();

        if (false !== $args && array_depth($args) < 3) {
            $this->_variables = $args;
        }
    }

    /**
     * Class destructor.
     *
     * Closes filesystem and memory file streams.
     */
    public function __destruct()
    {
        $this->_template->close();
        $this->_render->close();
    }

    /**
     * {@inheritdoc}
     * Returns current _render state as a string.
     *
     * @return string
     */
    public function __tostring()
    {
        return $this->_render->getContents();
    }

    /**
     * Allocates a new php://memory stream for the cloned object.
     * The file stream remains the same (reference).
     * The new object $_variables is emptied.
     */
    public function __clone()
    {
        $this->_render = SimpleStream\StreamFactory::getMemoryStream();
        $this->_variables = array();
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    public function toSanitize($var = null)
    {
        if(is_null($var)) $var = !$this->_toSanitize;
        $this->_toSanitize = $var;
    }

    /**
     * {@inheritdoc}
     *
     * Overwrite _render content with _template content as if nothing happenned.
     *
     * @return $this For chaining
     */
    public function reset()
    {
        $this->_render->overwrite($this->_template->getContents());

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * Replaces template {VARIABLES} by their $_variables counterpart.
     *
     * @return Stream The resulting stream
     */
    public function compose()
    {
        $this->reset();

        foreach ($this->_variables as $k => $v) {
            $this->replace($k, $v);
        }

        if($this->_toSanitize)
        {
            $this->sanitize();
        }

        return $this->_render;
    }

    /**
     * {@inheritdoc}
     *
     * Analog method to compose() but returns a string.
     *
     * @return string The rendered string
     */
    public function render()
    {
        return $this->compose()->getContents();
    }

    /**
     * Replace a {VARIABLE} by its value in _render.
     *
     * @param string          $key   The {VARIABLE} that has to be replaced
     * @param string|Template $value The value that will replace
     */
    protected function replace($k, $v)
    {
        $string = $this->_render->getContents();
        $replacement = ($v instanceof Template ? $v->render() : (string) $v);

        $string = str_replace('#'.strtoupper($k).'#', $replacement, $string);

        $this->_render->overwrite($string);
    }

    /**
     * Rips off any remaining {VARIABLE} from _render as a preparation
     * for rendering.
     *
     * @return this For chaining
     */
    protected function sanitize()
    {
        $this->_render->overwrite(preg_replace(
            '@#[a-zA-Z0-9-_]*#@',
            '',
            $this->_render->getContents())
        );

        return $this;
    }

    /**
     * Variable setter.
     *
     * @param string           $key   The key
     * @param string|Component $value The value
     *
     * @return this For chaining
     */
    public function set($key, $value)
    {
        if(!(is_string($value) || $value instanceof Template))
        {
            throw new \InvalidArgumentException(
                'Invalid value type ['.gettype($value).'] for Component.',
                1401
            );
        }

        $this->_variables[$key] = $value;
        return $this;
    }

    /**
     * Variable getter.
     *
     * @param string $key The key
     *
     * @return string $variable The seeked variable OR false on failure
     */
    public function get($key)
    {
        if (isset($this->_variables[$key])) {
            return $this->_variables[$key];
        }

        return false;
    }

    /**
     * Template getter.
     *
     * @return Component $template The template
     */
    public function getTemplate()
    {
        return $this->_template;
    }
}
