<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;

class SensoraHome extends SensoraController
{
    public function __construct()
    {
        parent::__construct(null, new SView\View('default'));
    }

    public function index()
    {
        $this->setGlobalContext();

        $config = S\App::getConfiguration();
        $document = $this->_view->getDocument();

        $document->draft('home');
        $this->respond();
    }

    public function run(array $params = array())
    {
        $this->index();
    }
}
