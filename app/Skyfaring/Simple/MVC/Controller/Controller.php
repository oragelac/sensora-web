<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\PSR as PSR;
use Skyfaring\Simple\MVC\Model as SModel;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\HTTP as SHTTP;

abstract class Controller
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var Model
     */
    protected $_model = null;

    /**
     * @var View
     */
    protected $_view = null;

    /**
     * @var array
     */
    protected $_dynamics = array();

    /**
     * Class constructor.
     */
    public function __construct(SModel\Model $model = null, SView\View $view = null,
        $dynamic_vars = array())
    {
        $this->_model = isset($model) ? $model : new SModel\Model();
        $this->_view = isset($view) ? $view : new SView\View();

        $this->_dynamics = $dynamic_vars;
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Tiny factory. For fluent behaviour.
     */
    static public function create(SModel\Model $model = null, SView\View $view = null)
    {
        return new static($model, $view);
    }

    /**
     * Sends a response back to the client.
     */
    public function respond(PSR\StreamInterface $stream = null, array $headers = array(), $statusCode = '200')
    {
        if (is_null($stream))
        {
            $stream = $this->_view->getDocument()->toStream();
        }

        SHTTP\Response::create($stream, $headers, $statusCode)->send();
    }

/* ========================================================================== */
/* === WIP ================================================================== */
/* ========================================================================== */

    /**
     * WIP
     *
     * ASYNCHRONOUS ANSWER
     */
    public function react($string, array $headers = array(), $statusCode = '200')
    {
        $stream = self::memory();
        $json = json_encode($string);
        $stream->overwrite($json);

        SHTTP\Response::create($stream, $headers, $statusCode)->send();
    }

    /**
     * Returns a memory stream.
     *
     * @return Stream THe memory stream.
     */
    protected static function memory()
    {
        return S\Stream\StreamFactory::getMemoryStream();
    }

    /**
     * Refreshes the page.
     */
    protected function refresh($destination = null)
    {
        header('Refresh:0; url=/');
        // $header = 'Refresh:0' . (empty($destination) ? '' : 'url=' . $destination);
        // $this->respond(self::memory(), array($header), '200');
    }

    /**
     * This method is the method called if none is provided within the route
     * definition and must therefore be implemented in child controller classes.
     *
     * @param array $params The parameters to work with.
     */
    abstract public function run(array $params = array());

}
