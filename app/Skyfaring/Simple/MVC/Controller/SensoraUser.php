<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\MVC\Model as SModel;

class SensoraUser extends SensoraController
{
    public function __construct()
    {
        parent::__construct(null, new SView\View('default'));
    }

    public function getUser(){
        $Reponse = new Reponse();
        $db =array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );
        $model = new SModel\Model();
        $model->initialize($db);

        //Get data from POST
        $input = \file_get_contents('php://input');
        $data = json_decode($input);
        $idUser = $data->IdUser;

        $User = new SModel\User(null,null,null,null,null,null,null,null,null);
        //$User->setId($idUser);
        $Reponse->resultat = $User->read($idUser);

        if (!empty($Reponse->resultat)) {
            $Reponse->statut = true;
        }

        //$Reponse->resultat = $this;

        echo json_encode($Reponse);
    }

    public function getAllUser(){
        $db = array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );

        $model = new SModel\Model();
        $model->initialize($db);

        $data = json_decode(file_get_contents('php://input'));
        $User = new SModel\User(null,null,null, null, null, null, null,null,null);

        $readAll = $User->readAll();

        if($readAll){
            $this->react($readAll);
            return ;
        }

        $this->react(false, array(), '401');

    }

    public function saveUser(){

        $Reponse = new Reponse();
        $db =array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );
        $model = new SModel\Model();
        $model->initialize($db);

        //Get data from POST
        $input = \file_get_contents('php://input');
        $data = json_decode($input);
        $user = $data->User;

        $User = new SModel\User($user->_pseudonym,$user->_password,$user->_lastname,$user->_firstname,$user->_email,null,null,null,null);
        $User->setId($user->_id);
        $Reponse->resultat=$User->update();



        if (!empty($Reponse->resultat)) {
            $Reponse->statut = true;
        }

        echo json_encode($User);
    }

        public function run(array $params = array())
        {
            $this->index();
        }
}
