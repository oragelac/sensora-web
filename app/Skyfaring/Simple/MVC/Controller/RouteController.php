<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\Templating as STemplating;

class RouteController extends Controller
{
    public function __construct()
    {
        parent::__construct(null, new SView\View('skf-route'));

        $this->_view->getDocument()->addContexts( array(
            'path' => '/config/route' // TODO - Dynamise
        ));
    }

    public function run(array $params = array())
    {
        $this->listRoutes();
    }

    public function listRoutes()
    {
        $document = $this->_view->getDocument();
        $routes = S\App::getRouter()->getRoutes();

        $links = array(
            array('location' => $routes->get('skf-config')->getPath(), 'name' => 'config'),
            array('location' => $routes->get('skf-route')->getPath(), 'name' => 'routes')
        );

        $document->addContexts( array(
            'page_header' => 'skf/skf-header.twig',
            'content' => 'skf/skf-routes.twig',
            'links' => $links,
            'routes' => $routes
        ));

        $document->draft('content');
        $this->respond();
    }
}
