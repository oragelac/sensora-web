<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\MVC\Model as SModel;

class SensoraContact extends SensoraController
{
    public function __construct()
    {
        parent::__construct(null, new SView\View('default'));
    }

    public function index()
    {
        $this->setGlobalContext();
        $this->_view->getDocument()->draft('contact');
        $this->respond();
    }

    public function sendMessage()
    {
        $db =array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );
        $model = new SModel\Model();
        $model->initialize($db);

        $isValueGood = true;
        $Reponse = new Reponse();

        //Get data from POST
        $input = \file_get_contents('php://input');
        $data = json_decode($input);
        $contact = $data->Contact;

        //TODO check le contenu de chaque variable, bloquer les injection (how ?)

        $Contact = new SModel\Messagerie(htmlentities($contact->sujet),htmlentities($contact->type),htmlentities($contact->contenu),htmlentities($contact->emailSource),null,null,false);
        $Contact->create();

        $Reponse->statut =true;
        $Reponse->resultat = $data;

        echo json_encode($Reponse);
    }

    public function sendEmail()
    {
        $db =array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );
        $model = new SModel\Model();
        $model->initialize($db);

        $isValueGood = true;
        $Reponse = new Reponse();

        //Get data from POST
        $input = \file_get_contents('php://input');
        $data = json_decode($input);
        $action = $data->Action;
        $email = $data->Email;

        //TODO check le contenu de chaque variable, bloquer les injection (how ?)

        //TODO send email
        $Messagerie = new SModel\Messagerie(htmlentities($action),null,htmlentities($email->contenu),null,htmlentities($email->destinataire),null,false);
        $Reponse->resultat = $Messagerie->sendEmail();
        $Reponse->statut=true;
        echo json_encode($Reponse);
    }

    public function getAll()
    {
        $db =array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );
        $model = new SModel\Model();
        $model->initialize($db);

        $Reponse = new Reponse();

        $Contact = new SModel\Messagerie(null,null,null,null,null,null,null);
        $Reponse->resultat = $Contact->readAll();

        if (!empty($Reponse->resultat)) {
            $Reponse->statut = true;
        }else{
            $Reponse->msgErreur[] = "Aucun message n'a été trouvé.";
        }

        echo json_encode($Reponse);
    }

    public function run(array $params = array())
    {
        $this->index();
    }
}
