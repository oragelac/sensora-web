<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\MVC\Model as SModel;

class SensoraInscription extends SensoraController
{
    public function __construct()
    {
        parent::__construct(null, new SView\View('default'));
    }

    public function inscription()
    {
        $this->setGlobalContext();
        $document = $this->_view->getDocument()->draft('inscription');
        $this->respond();
    }

    public function addUser(){
        $db =array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );
        $model = new SModel\Model();
        $model->initialize($db);

        $isValueGood = true;
        $Reponse = new Reponse();

        //Get data from POST
        $input = \file_get_contents('php://input');
        $data = json_decode($input);
        $newUser = $data->NewUser;

        //Manipulation des data
        if ((strlen($newUser->mdp)<8)||(strlen($newUser->mdp)>30)){
            $Reponse->msgErreur[]="La longueur du mot de passe doit être entre 8 et 30 caractère.";
            $isValueGood = false;
        }

        $User = new SModel\User(htmlentities($newUser->pseudo),htmlentities($newUser->mdp),htmlentities($newUser->nom),htmlentities($newUser->prenom),htmlentities($newUser->email),null,null,null,null);

        if (!$User->checkDispo("pseudo")){
            $Reponse->msgErreur[]="Le pseudo existe déjà.";
            $isValueGood = false;
        }

        if (!$User->checkDispo("email")){
            $Reponse->msgErreur[]="Le email existe déjà.";
            $isValueGood = false;
        }

        if($isValueGood){
            $Reponse->resultat = $User->create();
            $Reponse->statut = true;
        }

        echo json_encode($Reponse);
    }


    public function run(array $params = array())
    {
        $this->index();
    }
}
