<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;
use Fetch\Server;
use Fetch\Message;

class Mail
{
    public function __construct( \Fetch\Message $msg )
    {
        $this->_author      = $msg->getAddresses('sender');
        $this->_subject     = $msg->getSubject();
        $this->_date        = $msg->getDate();
        $this->_msg         = $msg->getPlainTextBody();
        $this->_htmlMsg     = $msg->getHtmlBody();
    }

    public $_author     = null;
    public $_subject    = null;
    public $_date       = null;
    public $_msg        = null;
    public $_htmlMsg    = null;
}

class SensoraAdministration extends SensoraController
{
    /**
     * @var string
     */
    protected $_mailHost = '23.20.2.150';

    /**
     * @var string
     */
    protected $_mailPort = '143'; // Mail certificates are self-signed for now.

    /**
     * @var \Fetch\Server
     */
    protected $_mailServer = null;

    /**
     * {@inheritdoc}
     */
    public function __construct($var, $var2, $dynamics)
    {
        parent::__construct(null, new SView\View('default'), $dynamics);

        $this->_mailServer = new Server(
            $this->_mailHost,
            $this->_mailPort
        );
    }

    /**
     *
     */
    public function index()
    {
        $this->setGlobalContext();
        $document = $this->_view->getDocument();

        $document->draft('administration');
        $this->respond();
    }

    /**
     * Authenticate against the mail server.
     *
     * @param string $username The mail username
     * @param string $password The mail password
     *
     * @return boolean Wheter the connection was succesfull
     */
    public function authenticate()
    {
        if (empty($_POST))
        {
            $this->react(false, array(), '401');
        }

        $username = $_POST['username'];
        $password = $_POST['password'];

        try
        {
            $this->_mailServer->setAuthentication( $username, $password );

            $rawMails = $this->_mailServer->getMessages();
            $mails = array();
            foreach ($rawMails as $data)
            {
                $mails[] = new Mail($data);
            }

            $this->react($mails, array(), '200');
        }

        catch (\Exception $e)
        {
            $this->react($e->getMessage(), array(), '401');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run(array $params = array())
    {
        $this->index();
    }
}
