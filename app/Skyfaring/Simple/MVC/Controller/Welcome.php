<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;

class Welcome extends Controller
{
    public function __construct()
    {
        parent::__construct(null, new SView\View('default'));
    }

    public function index()
    {
        $config = S\App::getConfiguration();

        $this->_view->add(array(
            'YOU' => $_SERVER['REMOTE_ADDR'],
            'CONFIG' => $config->get('routes.skf-config.path')->toString()
        ), 'welcome');

        $this->respond();
    }

    public function run(array $params = array())
    {
        $this->itWorks();
    }
}
