<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Fetch\Server;
use Fetch\Message;

class Playground extends Controller
{
    public function hello()
    {
        $server = new Server('23.20.2.150', 143);
        $server->setAuthentication('admin@sens.oragelac.com', 'sjS3bejvep$');

        $messages = $server->getMessages();
        foreach ($messages as $message)
        {
            echo "Subject: {$message->getSubject()}", PHP_EOL;
            echo "Body: {$message->getMessageBody()}", PHP_EOL;
        }
    }

    public function run(array $params = array()) {}
}
