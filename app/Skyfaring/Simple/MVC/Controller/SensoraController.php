<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;

abstract class SensoraController extends Controller
{
    protected function setGlobalContext()
    {
        $router = S\App::getRouter();
        $routes = $router->getRoutes();
        $document = $this->_view->getDocument();

        // Header
        $link_profil = '/profil';
        $link_logo = $routes->get('sensora-connection')->getPath();
        if(isset($_SESSION['user']))
        {
            $link_profil .= '/'.$_SESSION['user']->getId();

            if($_SESSION['user']->getRank() == 'administrateur')
            {
                $link_logo = $routes->get('sensora-administration')->getPath();
            }

            else
            {
                $link_logo = $routes->get('sensora-raspberry')->getPath();
            }
        }


        $document->addContexts( array(
            'page_header'           => 'header.twig',
            'connected'             => isset($_SESSION['user']),
            'link_home'             => $routes->get('sensora-accueil')->getPath(),
            'link_measure'          => $routes->get('sensora-raspberry')->getPath(),
            'link_shop'             => $routes->get('sensora-shop')->getPath(),
            'link_logo'             => $link_logo,
            'link_contact'          => $routes->get('sensora-contact')->getPath(),
            'link_profil'           => $link_profil,
            'link_administration'   => $routes->get('sensora-administration')->getPath(),
            'link_inscription'      => $routes->get('sensora-inscription')->getPath(),
            'link_connection'       => $routes->get('sensora-connection')->getPath(),
            'link_disconnection'    => $routes->get('sensora-disconnection')->getPath()
        ));
    }
}
