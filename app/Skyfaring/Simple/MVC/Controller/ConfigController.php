<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\Templating as STemplating;

class ConfigController extends Controller
{
    public function __construct()
    {
        parent::__construct(null, new SView\View('skf-config'));

        $this->_view->getDocument()->addContexts( array(
            'path' => '/config' // TODO - Dynamise
        ));
    }

    public function index()
    {
        $appConfig = S\App::getConfiguration();
        $routes = S\App::getRouter()->getRoutes();
        $document = $this->_view->getDocument();

        $links = array(
            array('location' => $routes->get('skf-config')->getPath(), 'name' => 'config'),
            array('location' => $routes->get('skf-route')->getPath(), 'name' => 'routes')
        );

        $document->addContexts( array(
            'page_header' => 'skf/skf-header.twig',
            'links' => $links,
            'version' => $appConfig->get('version')->toString(),
            'environment' => $appConfig->get('environment')->toString(),
            'content' => 'skf/skf-general.twig'
        ));

        $document->draft('content');
        $this->respond();
    }

    public function run(array $params = array())
    {
        $this->index();
    }
}
