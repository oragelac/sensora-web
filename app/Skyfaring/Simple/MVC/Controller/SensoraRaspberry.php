<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\MVC\Model as SModel;

class SensoraRaspberry extends SensoraController
{
    public function __construct()
    {
        parent::__construct(null, new SView\View('default'));
    }

    public function index()
    {
        $this->setGlobalContext();

        $config = S\App::getConfiguration();
        $document = $this->_view->getDocument();

        $document->draft('myRaspberry');
        $this->respond();
    }

    public function getAllFrom()//TODO a supprimé si on peut mettre param dans url
    {
        $Reponse = new Reponse();
        $db =array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );
        $model = new SModel\Model();
        $model->initialize($db);

        $user = new SModel\User(null,null,null,null,null,null,null,null,null);
        $user->setId($_SESSION['user']->_id);//id du compte macchite

        $Raspberry = new SModel\Raspberry(null,null,null,null,null, null,null,$user);
        //$Reponse->resultat = $Raspberry->read("6");//fonctionne mais que pour un rasp précis=)

        $Reponse->resultat = $Raspberry->readAllFrom($_SESSION['user']->_id);

        if (!empty($Reponse->resultat)) {
            $Reponse->statut = true;
        }

        echo json_encode($Reponse);
    }

    public function getAll()
    {
        $Reponse = new Reponse();
        $db =array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );
        $model = new SModel\Model();
        $model->initialize($db);

        $Raspberry = new SModel\Raspberry(null,null,null,null,null, null,null,null);

        $Reponse->resultat = $Raspberry->readAll();

        if (!empty($Reponse->resultat)) {
            $Reponse->statut = true;
        }

        echo json_encode($Reponse);
    }

    public function insertData()
    {
        $Reponse = new Reponse();
        $db =array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );
        $model = new SModel\Model();
        $model->initialize($db);

        //Get data from POST
        $input = \file_get_contents('php://input');
        $data = json_decode($input);

        switch ($data->type){
            case "son":
                for($i=0;$i<sizeof($data->data);$i++){
                    $DataSound = new SModel\DataSound($data->data[$i]->db,$data->data[$i]->time,$data->id);
                    $DataSound->create();
                }
                break;
            case "radar":
                for($i=0;$i<sizeof($data->data);$i++){
                    $dataRadar = new SModel\DataCarSpeed($data->data[$i]->km,$data->data[$i]->time,$data->id);
                    $dataRadar->create();
                }
                break;

        }


        echo json_encode($input);
    }

    public function deleteData(){
        $Reponse = new Reponse();
        $db =array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );
        $model = new SModel\Model();
        $model->initialize($db);

        //Get data from POST
        $input = \file_get_contents('php://input');
        $data = json_decode($input);

        switch ($data->Type){
            case "sound" :
                $DataSound = new SModel\DataSound(null,null,$data->IdRaspberry);
                $DataSound->deleteAllFrom();
                break;
        }

        $Reponse->statut =true;
        echo json_encode($Reponse);

    }


    public function run(array $params = array())
    {
        $this->index();
    }
}
