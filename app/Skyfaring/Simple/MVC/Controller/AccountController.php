<?php

namespace Skyfaring\Simple\MVC\Controller;

class AccountController extends Controller
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    private static $_loginForm      = ['username', 'pass'];
    private static $_subscribeForm  = ['username', 'pass', 'email'];

    public function __construct()
    {
        parent::__construct();
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    public function login($fill = null)
    {
        $error  = '';
        $result = false;

        if ($fill != null) {
            try {
                $logged = $this->_model->login($_POST);
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }

            if ($logged) {
                header('Location: /home', true, 303);
            } else {
                $this->_view->login_fail($error);
            }
        }

        $this->_view->set_content('login');
        echo $this->_view->render();
    }

    public function subscribe(array $fill = null)
    {
        $error  = '';
        $subcribed = false;

        if ($fill != null) {
            try {
                $subscribed = $this->_model->subscribe($_POST);
            } catch (\PDOException $e) {
                $error = "Database error.";
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }

            if ($subscribed) {
                if(MAIL_ACTIVATION) {
                    header("Location: /account/activate/$result", true, 303);
                } else {
                    header("Location: /account/login", true, 303);
                }
            } else {
                $this->_view->subscribe_fail($error);
            }
        }

        $this->_view->set_content('subscribe');
        echo $this->_view->render();
    }

    public function activate(array $args = null)
    {
        $error = '';
        $result = false;

        if ($args[2] != null) {
            try {
                $result = $this->_model->activate($args);
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }

            if ($result) {
                header('Location: /account/login');
            } else {
                $this->_view->activate_error($error);
            }
        }

        $this->_view->set_content('activate');
        echo $this->_view->render();
    }

    public function run(array $params = array())
    {

    }
}
