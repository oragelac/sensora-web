<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;

class Section
{
    public function __construct($name, $src, $active = false, $inactive = false)
    {
        $this->_name = $name;
        $this->_src = $src;
        $this->_active = $active;
        $this->_inactive = $inactive;
    }

    public $_active;
    public $_inactive;
    public $_name;
    public $_src;
}

class SensoraAnnex extends SensoraController
{
    public function __construct($var, $var2, $dynamics)
    {
        parent::__construct(null, new SView\View('sensora-annex'), $dynamics);
    }

    public function index()
    {
        $document = $this->_view->getDocument();
        $section = $this->_dynamics['section'];

        // Menu creation
        $document->addContext('sections', array(
            new Section('A propos', '/annex/about', $section == 'about'),
            new Section('Foire Aux Questions', '/annex/faq', $section == 'faq', true),
            new Section('Documentation', '/annex/doc', $section == 'doc', true),
            new Section('Qui sommes-nous ?', '/annex/who', $section == 'who', true)
        ));

        // Content dealing
        $content_root = $_SERVER['DOCUMENT_ROOT']. '/public/content/';
        switch ($section)
        {
            case 'about':
                $file = file_get_contents($content_root . 'about.json');
                break;

            case 'faq':
                $file = file_get_contents($content_root . 'faq.json');
                break;

            case 'doc':
                $file = file_get_contents($content_root . 'doc.json');
                break;

            case 'who':
                $file = file_get_contents($content_root . 'who.json');
                break;

            default:
                $file = '{ "title": "Unknown", "content": [ { "text":"Unknown section requested."}] }';
                break;
        }

        $content = json_decode($file, true);

        // AJAX calls
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
        && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            $answer = array(
                'title' => ucfirst($section),
                'content' => $content
            );

            $this->react(json_decode($file, true));
        }

        else
        {
            $document->addContexts( array(
                'title' => $content['title'],
                'content' => $content['content']
            ));

            $document->draft('annex');
            $this->respond();
        }

    }

    /**
     * {@inheritdoc}
     */
    public function run(array $params = array())
    {
        $this->about();
    }
}
