<?php
namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\MVC\Model as SModel;

class SensoraConnection extends SensoraController
{
    /**
     * Typical Controller constructor, nothing to see.
     */
    public function __construct()
    {
        parent::__construct(null, new SView\View('default'));
    }

    /**
     * Main class method.
     *
     * Outputs login page.
     */
    public function connect()
    {
        $routes = S\App::getRouter()->getRoutes();
        $document = $this->_view->getDocument();

        $document->addContext('subscription', $routes->get('sensora-inscription')->getPath());
        $document->draft('connection');

        $this->respond();
    }

    /**
     * Authentication method.
     *
     * Verifies user credentials, contact the database and returns the result
     * asynchronously.
     */
    public function authenticate()
    {
        $db = array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );

        $model = new SModel\Model();
        $model->initialize($db);

        $data = json_decode(file_get_contents('php://input'));
        $user = new SModel\User(
            htmlentities($data->user->pseudo),
            htmlentities($data->user->mdp),
            null, null, null, null, null,null,null
        );

        if ($result = $user->connect())
        {
            //TEST
            //$Adresse = new SModel\Adresse(null,null,null,null,null);
            //$read = $Adresse->read(1);
            $read  = $user->read($result);
            $_SESSION['user'] = $read;
            $this->react($read);

            return;
        }

        $this->react(false, array(), '401');
    }

    /**
     * Disconnects a user.
     */
    public function disconnect()
    {
        session_unset();
        session_destroy();
        $this->refresh('/');
    }

    /**
     * {@inheritdoc}
     */
    public function run(array $params = array())
    {
        $this->index();
    }
}
