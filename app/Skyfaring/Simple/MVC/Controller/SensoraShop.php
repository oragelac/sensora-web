<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\MVC\Model as SModel;

class SensoraShop extends SensoraController
{
    public function __construct()
    {
        parent::__construct(null, new SView\View('default'));
    }

    public function index()
    {
        $this->setGlobalContext();
        $this->_view->getDocument()->draft('shop');
        $this->respond();
    }

    public function buy()
    {
        $db =array(
            "name"  => "sensora",
            "host" => "oragelac.com",
            "user" => "root",
            "password" => "waffles4love"
        );
        $model = new SModel\Model();
        $model->initialize($db);

        //Get data from POST
        $input = \file_get_contents('php://input');
        $data = json_decode($input);
        $idUser = $data->IdUser;
        $panier = $data->Panier;

        $User = new SModel\User(null,null,null,null,null,null,null,null,null);
        $User->setId($idUser);

        for ($i=0; $i < sizeof($panier); $i++)
        {
            $isSon = 0;
            $isCarSpeed = 0;
            $isAirQuality = 0;
            for ($j=0; $j < sizeof($panier[$i]->modules); $j++)
            {
                switch ($panier[$i]->modules[$j]->type)
                {
                    case 'module-son' :
                        $isSon = 1;
                        break;
                    case 'module-densite' :
                        $isDensite = 1;
                        break;
                    case 'module-radar' :
                        $isCarSpeed = 1;
                        break;
                    case 'module-airQuality' :
                        $isAirQuality = 1;
                        break;
                    case 'module-alimentation_solaire' :
                        $isAlimSolaire = 1;
                        break;
                }
            }
            $Raspberry = new SModel\Raspberry($panier[$i]->nom,0,$isSon,$isCarSpeed,$isAirQuality, null,$User, null);
            $resultat[]=$Raspberry->create();
        }

        //$resultat = $panier;

        if($resultat){
            $this->react($resultat);

            return;
        }
        $this->react(false, array(), '401');
    }

    public function run(array $params = array())
    {
        $this->index();
    }
}
