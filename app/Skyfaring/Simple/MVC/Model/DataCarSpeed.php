<?php

namespace Skyfaring\Simple\MVC\Model;

class DataCarSpeed extends Model implements CRUD
{
    /**
     * @var string
     */
    protected $_speed = null;

    /**
     * @var string
     */
    protected $_time = null;

    /**
     * @var string
     */
    protected $_idRaspberry = null;

    /**
     * Class constructor.
     *
     * @param string $lastname The user lastname
     * @param string $firstname The user firstname
     * @param string $email The dev email
     * @param string $bank The user bank
     * @param string $payment The user payment
     */
    public function __construct($speed,$time,$idRaspberry)
    {
        $this->_speed = $speed;
        $this->_time = $time;
        $this->_idRaspberry = $idRaspberry;
    }

    /**
     * speed getter.
     *
     * @return string
     */
    public function getSpeed()
    {
        return $this->_speed;
    }

    /**
     * time getter.
     *
     * @return string
     */
    public function getTime()
    {
        return $this->_time;
    }

    /**
     * idRaspberry getter.
     *
     * @return string
     */
    public function getIdRaspberry()
    {
        return $this->_idRaspberry;
    }

    /**
     * speed setter.
     *
     * @param string $speed.
     */
    public function setSpeed($speed)
    {
        $this->_speed = $speed;
    }

    /**
     * time setter.
     *
     * @param string $time
     */
    public function setTime($time)
    {
        $this->_time = $time;
    }

    /**
     * idRaspberry setter.
     *
     * @param string $idRaspberry
     */
    public function setIdRaspberry($idRaspberry)
    {
        $this->_idRaspberry = $idRaspberry;
    }

    /**
     * Simple toString.
     *
     * @return string
     */
    public function __toString()
    {
        return pretty_dump($this);
    }

/* ------------------------------------------------------ CRUD Implementation */

    /**
    * {@inheritdoc}
    */
    public function create()
    {
        $request = 'INSERT INTO `CarSpeed` (`speed`,`time`,`idRaspberry`)'
            .'VALUES ("'
            .$this->_speed.'", "'
            .$this->_time.'", "'
            .$this->_idRaspberry.'");';

        self::$_PDO->query($request);
        $this->_id = self::$_PDO->lastInsertId();
    }

    /**
     * {@inheritdoc}
     */
    public static function read($id)
    {
        $request = 'SELECT `id`, `speed`, `time`'
                   .'FROM `CarSpeed` WHERE `id`='.$id.';';

        $row        = self::$_PDO->query($request);
        $imported   = $row->fetch();

        $dataSpeed = new static(
            $imported['speed'],
            $imported['time'],
            null //idRaspberry
        );

        $dataSpeed->setId($imported['id']);
        return $dataSpeed;
    }

    /**
     * {@inheritdoc}
     */
    public function update()
    {
        $request = 'UPDATE `CarSpeed` '
                   .'SET decibel=`'.$this->_speed.'`, '
                   .'SET time=`'.$this->_time.'`, '
                   .'WHERE id='.$this->_id.';';

        self::$_PDO->query($request);
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        $request = 'DELETE FROM `CarSpeed` WHERE id='.$this->_id.';';

        self::$_PDO->query($request);

        //TODO delete from inter_data
    }

    public static function readAllFrom($idRaspberry)
    {
        $request = 'SELECT `id`, `speed`, `time`'
                   .'FROM `CarSpeed` WHERE `idRaspberry`='.$idRaspberry.';';

        $row        = self::$_PDO->query($request);
        $dataCarSpeed=null;
        while ($imported   = $row->fetch())
        {
            $speed["_id"] = $imported["id"];
            $speed["_speed"] = $imported["speed"];
            $speed["_time"] = $imported["time"];
            $dataCarSpeed[] = $speed;
        }
        return $dataCarSpeed;
    }

    public function deleteAllFrom(){
        $request = 'DELETE FROM `CarSpeed` WHERE idRaspberry='.$this->_idRaspberry.';';

        self::$_PDO->query($request);
    }

    /**
     * {@inheritdoc}
     */
    public static function delegate(array $args)
    {
        return new static(
            $args['speed'],
            $args['time']
        );
    }

    /**
     * Returns an array representation of this object.
     *
     * @return array
     */
    public function toArray()
    {
        $retval = array(
            'speed'     => $this->_speed,
            'time'      => $this->_time
        );

        return $retval;
    }

}
