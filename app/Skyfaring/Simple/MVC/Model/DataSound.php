<?php

namespace Skyfaring\Simple\MVC\Model;

class DataSound extends Model implements CRUD
{
    /**
     * @var string
     */
    protected $_decibel = null;

    /**
     * @var string
     */
    protected $_time = null;

    /**
     * @var string
     */
    protected $_idRaspberry = null;

    /**
     * Class constructor.
     *
     * @param string $lastname The user lastname
     * @param string $firstname The user firstname
     * @param string $email The dev email
     * @param string $bank The user bank
     * @param string $payment The user payment
     */
    public function __construct($decibel,$time,$idRaspberry)
    {
        $this->_decibel = $decibel;
        $this->_time = $time;
        $this->_idRaspberry = $idRaspberry;
    }

    /**
     * decibel getter.
     *
     * @return string
     */
    public function getDecibel()
    {
        return $this->_decibel;
    }

    /**
     * time getter.
     *
     * @return string
     */
    public function getTime()
    {
        return $this->_time;
    }

    /**
     * decibel setter.
     *
     * @param string $decibel.
     */
    public function setDecibel($decibel)
    {
        $this->_decibel = $decibel;
    }

    /**
     * time setter.
     *
     * @param string $time
     */
    public function setTime($time)
    {
        $this->_time = $time;
    }

    /**
     * Simple toString.
     *
     * @return string
     */
    public function __toString()
    {
        return pretty_dump($this);
    }

/* ------------------------------------------------------ CRUD Implementation */

    /**
    * {@inheritdoc}
    */
    public function create()
    {
        $request = 'INSERT INTO `Son` (`decibel`,`time`,`idRaspberry`)'
            .'VALUES ("'
            .$this->_decibel.'", "'
            .$this->_time.'", "'
            .$this->_idRaspberry.'");';

        self::$_PDO->query($request);
        $this->_id = self::$_PDO->lastInsertId();


    }

    /**
     * {@inheritdoc}
     */
    public static function read($id)
    {
        $request = 'SELECT `id`, `decibel`, `time`'
                   .'FROM `Son` WHERE `id`='.$id.';';

        $row        = self::$_PDO->query($request);
        $imported   = $row->fetch();

        $dataSound = new static(
            $imported['decibel'],
            $imported['time']
        );

        $dataSound->setId($imported['id']);
        return $dataSound;
    }

    /**
     * {@inheritdoc}
     */
    public function update()
    {
        $request = 'UPDATE `Son` '
                   .'SET decibel=`'.$this->_decibel.'`, '
                   .'SET time=`'.$this->_time.'`, '
                   .'WHERE id='.$this->_id.';';

        self::$_PDO->query($request);
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        $request = 'DELETE FROM `Son` WHERE id='.$this->_id.';';

        self::$_PDO->query($request);
    }

    public static function readAllFrom($idRaspberry)
    {
        $request = 'SELECT `id`, `decibel`, `time`'
                   .'FROM `Son` WHERE `idRaspberry`='.$idRaspberry.';';

        $row        = self::$_PDO->query($request);
        $dataSound=null;
        while ($imported   = $row->fetch())
        {
            $sound["_id"] = $imported["id"];
            $sound["_decibel"] = $imported["decibel"];
            $sound["_time"] = $imported["time"];
            $dataSound[] = $sound;
        }
        return $dataSound;
    }

    public function deleteAllFrom(){
        $request = 'DELETE FROM `Son` WHERE idRaspberry='.$this->_idRaspberry.';';

        self::$_PDO->query($request);
    }

    /**
     * {@inheritdoc}
     */
    public static function delegate(array $args)
    {
        return new static(
            $args['decibel'],
            $args['time'],
            $args['idRaspberry']
        );
    }

    /**
     * Returns an array representation of this object.
     *
     * @return array
     */
    public function toArray()
    {
        $retval = array(
            'decibel'     => $this->_decibel,
            'time'      => $this->_time,
            'idRaspberry'      => $this->_idRaspberry
        );

        return $retval;
    }

    /**
     * Returns a json representation of this object.
     *
     * @return json The json
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }
}
