<?php
namespace Skyfaring\Simple\MVC\Model;

class Raspberry extends Model implements CRUD
{
    /**
     * @var string
     */
    public $_matricule = null;

    /**
     * @var string
     */
    public $_isOn = null;

    /**
     * @var string
     */
    public $_isSon = null;

    /**
     * @var string
     */
    public $_isCarSpeed = null;

    /**
     * @var string
     */
    public $_isAirQuality = null;

    /**
     * @var Date
     */
    public $_startLocationTime = null;


    /**
     * @var User
     */
    public $_user = null;

    /**
     * @var array
     */
    public $_dataSound = null;

    /**
     * @var array
     */
    public $_dataCarSpeed = null;

    /**
     * Class constructor.
     */
    public function __construct($matricule,$isOn,$isSon,$isCarSpeed,$isAirQuality, $startLocationTime,User $user =null, $son)
    {
        $this->_matricule = $matricule;
        $this->_isOn = $isOn;
        $this->_isSon = $isSon;
        $this->_isCarSpeed = $isCarSpeed;
        $this->_isAirQuality = $isAirQuality;
        $this->_startLocationTime = $startLocationTime;
        $this->_user = $user;
        $this->_dataSound =$son;
    }

    /**
     * matricule getter
     *
     * @return string matricule
     */
    public function getMatricule()
    {
        return $this->_matricule;
    }
    /**
     * isOn getter
     *
     * @return string isOn
     */
    public function getIsOn()
    {
        return $this->_isOn;
    }
    /**
     * isSon getter
     *
     * @return string isSon
     */
    public function getIsSon()
    {
        return $this->_isSon;
    }
    /**
     * isCarSpeed getter
     *
     * @return string isCarSpeed
     */
    public function getIsCarSpeed()
    {
        return $this->_isCarSpeed;
    }
    /**
     * isAirQuality getter
     *
     * @return string isAirQuality
     */
    public function getIsAirQuality()
    {
        return $this->_isAirQuality;
    }

    /**
     * startLocationTime getter
     *
     * @return string startLocationTime
     */
    public function getStartLocationTime()
    {
        return $this->_startLocationTime;
    }

    /**
     * user getter
     *
     * @return string user
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * dataSound getter
     *
     * @return string dataSound
     */
    public function getDataSound()
    {
        return $this->_dataSound;
    }

    /**
     * _dataCarSpeed getter
     *
     * @return string _dataCarSpeed
     */
    public function getDataCarSpeed()
    {
        return $this->_dataCarSpeed;
    }

    /**
     * matricule setter.
     *
     * @param string matricule.
     */
    public function setMatricule($matricule)
    {
        $this->_matricule = $matricule;
    }

    /**
     * startLocationTime setter.
     *
     * @param string startLocationTime.
     */
    public function setStartLocationTime($startLocationTime)
    {
        $this->_startLocationTime = $startLocationTime;
    }

    /**
     * user setter.
     *
     * @param string user.
     */
    public function setUser($user)
    {
        $this->_user = $user;
    }

    /**
     * dataSound setter.
     *
     * @param string dataSound.
     */
    public function setDataSound($dataSound)
    {
        $this->_dataSound = $dataSound;
    }

    /**
     * _dataCarSpeed setter.
     *
     * @param string _dataCarSpeed.
     */
    public function setCarSpeed($dataCarSpeed)
    {
        $this->_dataCarSpeed = $dataCarSpeed;
    }

/* ------------------------------------------------------ CRUD Implementation */

    /**
     * {@inheritdoc}
     */
    public function create()
    {
$idUser = $this->_user->getId();
        $request = 'INSERT INTO`Raspberry` (`matricule`,`isOn`,`isSon`,`isCarSpeed`,`isAirQuality`, `startLocationTime`, `idUser`)'
                        .'VALUES ("'
                        .$this->_matricule.'", '
                        .$this->_isOn.', '
                        .$this->_isSon.', '
                        .$this->_isCarSpeed.', '
                        .$this->_isAirQuality.', '
                        .'NOW(), '
                        .$idUser.');';


        self::$_PDO->query($request);
        $this->_id = self::$_PDO->lastInsertId();
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function read($id)
    {
        $request = 'SELECT `id`, `matricule`,`isOn`,`isSon`,`isCarSpeed`,`isAirQuality`, `startLocationTime`, `idUser`'
                   .'FROM `Raspberry` WHERE `id`='.$id.';';

        $row = self::$_PDO->query($request);
        $imported = $row->fetch();
        $user = User::read($imported['idUser']);

        if($imported['isSon'])//Récupère les data de son lié à ce raspberry
        {
            $sons = DataSound::readAllFrom($imported['id']);
        }

        if($imported['isCarSpeed'])//Récupère les data de son lié à ce raspberry
        {
            $carSpeed = DataCarSpeed::readAllFrom($imported['id']);
        }

        $developper = new static(
            $imported['matricule'],
            $imported['isOn'],
            $imported['isSon'],
            $imported['isCarSpeed'],
            $imported['isAirQuality'],
            $imported['startLocationTime'],
            $user,
            $sons
        );

        $developper->setId($imported['id']);
        $developper->setCarSpeed($carSpeed);

        return $developper;
    }

    /**
     * {@inheritdoc}
     */
    public function update()
    {
        $request = 'UPDATE `Raspberry` '
                   .'SET matricule=`'.$this->_matricule.'`, '
                   .'SET isOn=`'.$this->_isOn.'`, '
                   .'SET isSon=`'.$this->_isSon.'`, '
                   .'SET isCarSpeed=`'.$this->_isCarSpeed.'`, '
                   .'SET isAirQuality=`'.$this->_isAirQuality.'`, '
                   .'SET startLocationTime=`'.TIME().'`, '
                   .'SET idUser=`'.$this->_user->getId().'`, '
                   .'WHERE id='.$this->_id.';';

        self::$_PDO->query($request);
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        $request = 'DELETE FROM `Raspberry` WHERE id='.$this->_id.';';

        self::$_PDO->query($request);
    }

    public function readAllFrom($idUser)
    {
        $request = 'SELECT `id`, `matricule`,`isOn`,`isSon`,`isCarSpeed`,`isAirQuality`, `startLocationTime`, `idUser`'
                   .'FROM `Raspberry` WHERE `idUser`='.$idUser.';';

        $row = self::$_PDO->query($request);
        while ($imported = $row->fetch())
        {
            $sons=null;
            $carSpeed=null;

            if($imported['isSon'])//Récupère les data de son lié à ce raspberry
            {
                $sons = DataSound::readAllFrom($imported['id']);
            }

            if($imported['isCarSpeed'])//Récupère les data de son lié à ce raspberry
            {
                $carSpeed = DataCarSpeed::readAllFrom($imported['id']);
            }

            $user = User::read($idUser);

            $developper = new static(
                $imported['matricule'],
                $imported['isOn'],
                $imported['isSon'],
                $imported['isCarSpeed'],
                $imported['isAirQuality'],
                $imported['startLocationTime'],
                $user,
                $sons
            );
            $developper->setId($imported['id']);
            $developper->setCarSpeed($carSpeed);

            $AllRaspberryFrom[] = $developper;
        }
        return $AllRaspberryFrom;
    }

    public function readAll()
    {
        $request = "SELECT id, matricule,isOn,isSon,isCarSpeed,isAirQuality, startLocationTime, idUser
                   FROM Raspberry";

        $row = self::$_PDO->query($request);
        while ($imported = $row->fetch())
        {
            $sons=null;
            $carSpeed=null;

            if($imported['isSon'])//Récupère les data de son lié à ce raspberry
            {
                $sons = DataSound::readAllFrom($imported['id']);
            }

            if($imported['isCarSpeed'])//Récupère les data de son lié à ce raspberry
            {
                $carSpeed = DataCarSpeed::readAllFrom($imported['id']);
            }

            $user = User::read($imported['idUser']);

            $developper = new static(
                $imported['matricule'],
                $imported['isOn'],
                $imported['isSon'],
                $imported['isCarSpeed'],
                $imported['isAirQuality'],
                $imported['startLocationTime'],
                $user,
                $sons
            );
            $developper->setId($imported['id']);
            $developper->setCarSpeed($carSpeed);

            $AllRaspberry[] = $developper;
        }
        return $AllRaspberry;
    }

    /**
     * {@inheritdoc}
     */
    public static function delegate(array $args)
    {
        return new static(
            $args['matricule'],
            $args['isOn'],
            $args['isSon'],
            $args['isCarSpeed'],
            $args['isAirQuality'],
            $args['startLocationTime'],
            $args['user'],
            $args['state']
        );
    }

    public function toArray()
    {

        $retval =array(
            'matricule' => $this->_matricule,
            'isOn' => $this->_isOn,
            'isSon' => $this->_isSon,
            'isCarSpeed' => $this->_isCarSpeed,
            'isAirQuality' => $this->_isAirQuality,
            'startLocationTime' => $this->_startLocationTime,
            'user' => $this->_user->toArray(),
            'state' => $this->_state->toArray(),
        );

        return $retArray;
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }
}

 ?>
