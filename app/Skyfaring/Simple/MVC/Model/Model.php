<?php

namespace Skyfaring\Simple\MVC\Model;

class Model
{
    static protected $dbTech = 'mysql';

    /**
     * @var PDO
     */
    protected static $_PDO = null;

    /**
     * @var integer
     */
    public $_id = null;

    /**
     * Class initializer (for PDO sharing purpose).
     *
     * Takes an associative array which holds database arguments as parameter
     * and potentially the type of database we connect to.
     *
     * @var array $db The database arguments
     * @var string $dbTech The database technology (mysql / postgres / ...)
     * if any. Mysql is selected if no technology is provided.
     */
    public static function initialize(array $db, $dbTech = null)
    {
        $dbTech = isset($dbTech) ? $dbTech : self::$dbTech;

        self::$_PDO = new \PDO(
            $dbTech.':dbname='.$db['name'].';host='.$db['host'],
            $db['user'],
            $db['password']
        );

        self::$_PDO->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Sets the id.
     *
     * @param integer $id The new id
     */
    public final function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * Retrieves this object id.
     *
     * @return integer This object id
     */
    public final function getId()
    {
        return $this->_id;
    }

/* ---------------------------------------------------- Automatic db fetching */

    /**
     * Retrieives every given instance of the children -domain- calling class
     * from the database.
     *
     * @param Model $child The child class name
     * @return Model[] The array of desired objects
     */
    public static function all($child)
    {
        $results = self::$_PDO->query('SELECT * FROM '.strtolower($child).';');

        $retval = array();
        foreach ($results->fetchAll(\PDO::FETCH_ASSOC) as $result)
        {
            $retval[] = call_user_func(ucfirst($child).'::delegate', $result);
        }

        return $retval;
    }

    /*
     * Returns database rows as a matrice.
     *
     * @param string $table The table name
     */
    public static function allArray($table)
    {
        $retArray = array();
        $allDevelopers = self::all($table);
        foreach($allDevelopers as $developer)
        {
            $retArray[] = $developer->toArray();
        }

        return $retArray;
    }
}
