<?php

namespace Skyfaring\Simple\MVC\Model;

class User extends Model implements CRUD
{
    /**
     * @var string
     */
    public $_pseudonym = null;

    /**
     * @var string
     */
    protected $_password = null;

    /**
     * @var string
     */
    public $_lastname = null;

    /**
     * @var string
     */
    public $_firstname = null;

    /**
     * @var string
     */
    public $_email = null;

    /**
     * @var Bank
     */
    protected $_bank = null;

    /**
     * @var Payment
     */
    protected $_payment = null;

    /**
     * @var Raspberries
     */
    protected $_raspberries = array();

    /**
     * @var Rank
     */
    public $_rank = array();

    /**
     * @var Adresse
     */
    public $_adresse = null;

    /**
     * @var createTime
     */
    public $_createTime = null;

    /**
     * Class constructor.
     *
     * @param string $lastname The user lastname
     * @param string $firstname The user firstname
     * @param string $email The dev email
     * @param string $bank The user bank
     * @param string $payment The user payment
     */
    public function __construct($pseudonym,$password,$lastname,$firstname,$email,$bank,$payment,$rank, $adresse = null)
    {
        $this->_pseudonym = $pseudonym;
        $this->_password = $password;

        $this->_lastname = $lastname;
        $this->_firstname = $firstname;
        $this->_email = $email;

        $this->_bank = $bank;
        $this->_payment = $payment;

        $this->_rank =$rank;
        $this->_adresse=$adresse;
    }

    /**
     * lastname getter.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->_lastname;
    }

    /**
     * firstname getter.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->_firstname;
    }

    /**
     * email getter
     *
     * @return string email
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * rank getter
     *
     * @return string rank
     */
    public function getRank()
    {
        return $this->_rank;
    }

    /**
     * adresse getter
     *
     * @return string adresse
     */
    public function getAdresse()
    {
        return $this->_adresse;
    }

    /**
     * lastname setter.
     *
     * @param string lastname.
     */
    public function setLastname($lastname)
    {
        $this->_lastname = $lastname;
    }

    /**
     * firstname setter.
     *
     * @param string $firstname.
     */
    public function setFirstname($firstname)
    {
        $this->_firstname = $firstname;
    }

    /**
     * email setter.
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->_email = $email;
    }

    /**
     * rank setter.
     *
     * @param string $rank
     */
    public function setRank($rank)
    {
        $this->_rank = $rank;
    }

    /**
     * adresse setter.
     *
     * @param string $adresse
     */
    public function setAdresse($adresse)
    {
        $this->_adresse = $adresse;
    }

    /**
     * createTime setter.
     *
     * @param string $createTime
     */
    public function setCreateTime($createTime)
    {
        $this->_createTime = $createTime;
    }

    /**
     * Simple toString.
     *
     * @return string
     */
    public function __toString()
    {
        return pretty_dump($this);
    }

/* ------------------------------------------------------ CRUD Implementation */

    /**
    * {@inheritdoc}
    */
    public function create()
    {
        /*$this->_bank->create();
        $this->_payment->create();*/

        $request = 'INSERT INTO `User` (`pseudo`,`name`, `firstname`, `email`, `password`, `createTime`,`idBankInformation`, `idPayment`)'
            .'VALUES ("'
            .$this->_pseudonym.'", "'
            .$this->_lastname.'", "'
            .$this->_firstname.'", "'
            .$this->_email.'", "'
            .password_hash($this->_password,PASSWORD_BCRYPT).'", ' //PASSWORD_BCRYPT : utilisée pour créer une nouvelle table de hachage de mot de passe
            .'NOW(),null, null);';

            //.$this->_bank->getId().'`, `'
            //.$this->_payment->getId().'`);';

        self::$_PDO->query($request);
        $this->_id = self::$_PDO->lastInsertId();
    }

    /**
     * {@inheritdoc}
     */
    public static function read($id)
    {
        $adress =

        $request = "SELECT U.id as idUser, `pseudo`, U.name, `firstname`, `password`, `email`, `idBankInformation`, `idPayment`, `idAdresse`,R.name as rank
                    FROM User U
                    JOIN UserRank UR ON U.id = UR.idUser
                    JOIN Rank R ON R.id = UR.idRank
                    WHERE U.id = $id";

        /* $request = 'SELECT `id`, `pseudo`, `name`, `firstname`, `password`, `email`, `idBankInformation`, `idPayment`'
                  .'FROM `User`'
                  .'WHERE `id`='.$id.';';*/

        $row                    = self::$_PDO->query($request);
        $imported               = $row->fetch();
        $imported['bank']       = null; //Bank::read($imported['idBankInformation']);
        $imported['payment']    = null; //Payment::read($imported['idPayment']);

        // Temporary
        $imported['address'] = empty($imported['idAdresse'])
            ? null
            : $imported['idAdresse'];

        $user = self::delegate($imported);
        $user->setId($imported['idUser']);

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function update()
    {
        $request = 'UPDATE `User` '
                   .'SET `pseudo`="'.$this->_pseudonym.'", ';
        if (!is_null($this->_password))
        {
      $request .=  '`password`="'.password_hash($this->_password,PASSWORD_BCRYPT).'", ';
        }
        $request  .='`name`="'.$this->_lastname.'", '
                   .'`firstname`="'.$this->_firstname.'", '
                   .'`email`="'.$this->_email.'" '
                   //.'SET idBankInformation=`'.$this->_bank->getId().'`, '
                   //.'SET idPayment=`'.$this->_payment->getId().'` '
                   .'WHERE id="'.$this->_id.'";';

        self::$_PDO->query($request);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        $request = 'DELETE FROM `User` WHERE id='.$this->_id.';';

        self::$_PDO->query($request);

        $this->_bank->delete();
        $this->_payment->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function checkDispo($rowSearched)
    {
        $request=null;
        switch ($rowSearched){
            case "pseudo":
                $request = 'SELECT COUNT(*) FROM User WHERE pseudo="'.$this->_pseudonym.'";';
                break;
            case "email":
                $request = 'SELECT COUNT(*) FROM User WHERE email="'.$this->_email.'";';
                break;
        }

        self::$_PDO->query($request);

        $row = self::$_PDO->query($request);
        $nbRow  = $row->fetchColumn();

        $result = ($nbRow == 0)?true:false;
        return $result;
    }

    public function connect()
    {
        $request    = 'SELECT id,pseudo, password FROM User WHERE pseudo="'.$this->_pseudonym.'";';
        $row        = self::$_PDO->query($request);
        $imported   = $row->fetch();

        if (password_verify ( $this->_password , $imported["password"] ))
        {
            return $imported["id"];
        }

        return null;
    }

    public function readAll()
    {
        $request = "SELECT U.id as idUser, `pseudo`,R.name as rank,`createTime`
                    FROM User U
                    JOIN UserRank UR ON U.id = UR.idUser
                    JOIN Rank R ON R.id = UR.idRank";

        $row = self::$_PDO->query($request);
        while ($imported = $row->fetch())
        {

            $user = new static(
                $imported['pseudo'],
                null,
                null,
                null,
                null,
                null,
                null,
                $imported['rank'],
                null
            );
            $user->setCreateTime($imported['createTime']);
            $user->setId($imported['idUser']);

            $AllUsers[] = $user;
        }
        return $AllUsers;
    }

    /**
     * {@inheritdoc}
     */
    public static function delegate(array $args)
    {
        return new static(
            $args['pseudo'],
            $args['password'],
            $args['name'],
            $args['firstname'],
            $args['email'],
            $args['bank'],
            $args['payment'],
            $args['rank'],
            $args['address']
        );
    }

    /**
     * Returns an array representation of this object.
     *
     * @return array
     */
    public function toArray()
    {
        $retval = array(
            'pseudonym'     => $this->_pseudonym,
            'password'      => $this->_password,
            'lastame'       => $this->_lastname,
            'firstname'     => $this->_firstname,
            'email'         => $this->_email,
            'bank'          => $this->_bank->toArray(),
            'payment'       => $this->_payment->toArray()
        );

        return $retval;
    }

    /**
     * Returns a json representation of this object.
     *
     * @return json The json
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }
}
