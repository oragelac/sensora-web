<?php

namespace Skyfaring\Simple\MVC\Model;

interface CRUD
{
    /**
     * Imports a new instance from the database.
     *
     * @return Model The new instance as taken from the database.
     */
    public static function read($id);

    /**
     * Export this instance to the database.
     *
     * @return integer The object database id
     */
    public function create();

    /**
     * Updates the database instance of this object.
     */
    public function update();

    /**
     * Deletes this object from the database.
     */
    public function delete();

    /**
     * ~factory
     *
     * Delegates object creation for the all() method of the Model clas.
     *
     * @param array $args The necessary arguments to create an
     * instance of this object
     */
    public static function delegate(array $args);
}
