<?php

namespace Skyfaring\Simple\MVC\Model;

class Messagerie extends Model implements CRUD
{
    /**
     * @var string
     */
    public $_sujet = null;

    /**
     * @var string
     */
    public $_type = null;

    /**
     * @var string
     */
    public $_contenu = null;

    /**
     * @var string
     */
    public $_email_source = null;

    /**
     * @var string
     */
    public $_email_destinataire = null;

    /**
     * @var dateTime
     */
    public $_sendTime = null;

    /**
     * @var boolean
     */
    public $_isAnswer = null;

    /**
     * Class constructor.
     *
     * @param string $lastname The user lastname
     * @param string $firstname The user firstname
     * @param string $email The dev email
     * @param string $bank The user bank
     * @param string $payment The user payment
     */
    public function __construct($sujet,$type,$contenu,$email_source,$email_destinataire,$sendTime,$isAnswer)
    {
        $this->_sujet = $sujet;
        $this->_type = $type;
        $this->_contenu = $contenu;
        $this->_email_source = $email_source;
        $this->_email_destinataire = $email_destinataire;
        $this->_sendTime = $sendTime;
        $this->_isAnswer = $isAnswer;
    }

    /**
     * sujet getter.
     *
     * @return string
     */
    public function getSujet()
    {
        return $this->_sujet;
    }

    /**
     * sujet setter.
     *
     * @param string $sujet.
     */
    public function setSujet($sujet)
    {
        $this->_sujet = $sujet;
    }

    /**
     * type getter.
     *
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * type setter.
     *
     * @param string $type.
     */
    public function setType($type)
    {
        $this->_type = $type;
    }

    /**
     * contenu getter.
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->_contenu;
    }

    /**
     * contenu setter.
     *
     * @param string $contenu.
     */
    public function setContenu($contenu)
    {
        $this->_contenu = $contenu;
    }

    /**
     * email_source getter.
     *
     * @return string
     */
    public function getEmailSource()
    {
        return $this->_email_source;
    }

    /**
     * email_source setter.
     *
     * @param string $_email_source.
     */
    public function setEmailSource($_email_source)
    {
        $this->_email_source = $_email_source;
    }

    /**
     * _email_destinataire getter.
     *
     * @return string
     */
    public function getEmailDestinataire()
    {
        return $this->_email_destinataire;
    }

    /**
     * email_destinataire setter.
     *
     * @param string $_email_destinataire.
     */
    public function setEmailDestinataire($_email_destinataire)
    {
        $this->_email_destinataire = $_email_destinataire;
    }

    /**
     * sendTime getter.
     *
     * @return string
     */
    public function getSendTime()
    {
        return $this->_sendTime;
    }

    /**
     * sendTime setter.
     *
     * @param string $sendTime.
     */
    public function setSendTime($sendTime)
    {
        $this->_sendTime = $sendTime;
    }

    /**
     * isAnswer getter.
     *
     * @return string
     */
    public function getIsAnswer()
    {
        return $this->_isAnswer;
    }

    /**
     * isAnswer setter.
     *
     * @param string $isAnswer.
     */
    public function setIsAnswer($isAnswer)
    {
        $this->_isAnswer = $isAnswer;
    }


    /**
     * Simple toString.
     *
     * @return string
     */
    public function __toString()
    {
        return pretty_dump($this);
    }

/* ------------------------------------------------------ CRUD Implementation */

    /**
    * {@inheritdoc}
    */
    public function create()//utile quand les uti nous envoie des msg via la page contact
    {
        $request = 'INSERT INTO `Messagerie` (`sujet`,`type`,`contenu`,`email_source`,`email_destinataire`,`sendTime`,`isAnswer`)'
            .'VALUES ("'
            .$this->_sujet.'", "'
            .$this->_type.'", "'
            .$this->_contenu.'", "'
            .$this->_email_source.'", "'
            .$this->_email_destinataire.'",'
            .'NOW(),'
            .'false);';

        self::$_PDO->query($request);
        $this->_id = self::$_PDO->lastInsertId();
    }

    /**
     * {@inheritdoc}
     */
    public static function read($id)
    {
        $request = 'SELECT `id`, `sujet`,`type`,`contenu`,`email_source`,`email_destinataire`,`sendTime`,`isAnswer`'
                   .'FROM `Messagerie` WHERE `id`='.$id.';';

        $row        = self::$_PDO->query($request);
        $imported   = $row->fetch();

        $message = new static(
            $imported['sujet'],
            $imported['type'],
            $imported['contenu'],
            $imported['email_source'],
            $imported['email_destinataire'],
            $imported['sendTime'],
            $imported['isAnswer']
        );

        $message->setId($imported['id']);
        return $message;
    }

    /**
     * {@inheritdoc}
     */
    public function update()
    {
        $request = 'UPDATE `Messagerie` '
                   .'SET sujet=`'.$this->_sujet.'`, '
                   .'SET type=`'.$this->_type.'`, '
                   .'SET contenu=`'.$this->_contenu.'`, '
                   .'SET email_source=`'.$this->_email_source.'`, '
                   .'SET email_destinataire=`'.$this->_email_destinataire.'`, '
                   .'WHERE id='.$this->_id.';';

        self::$_PDO->query($request);
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        $request = 'DELETE FROM `Messagerie` WHERE id='.$this->_id.';';

        self::$_PDO->query($request);
    }

    /**
     * Cette function permet de récupérer toutes les row de la table Messagerie
     */
    public function readAll()
    {
        $request = 'SELECT `id`, `sujet`,`type`,`contenu`,`email_source`,`email_destinataire`,`sendTime`,`isAnswer`'
                   .'FROM `Messagerie`;';

        $row = self::$_PDO->query($request);
        while ($imported = $row->fetch())
        {

            $message = new static(
                $imported['sujet'],
                $imported['type'],
                $imported['contenu'],
                $imported['email_source'],
                $imported['email_destinataire'],
                $imported['sendTime'],
                $imported['isAnswer']
            );
            $message->setId($imported['id']);

            $allMessage[] = $message;
        }
        return $allMessage;
    }

    function sendEmail()
    {
        error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', 1);
echo 'I am : ' . `whoami`;
$result = mail('timartin@live.be','Testing 1 2 3','This is a test.');
echo '<hr>Result was: ' . ( $result === FALSE ? 'FALSE' : 'TRUE') . $result;
echo '<hr>';
echo phpinfo();
    }

    /**
     * {@inheritdoc}
     */
    public static function delegate(array $args)
    {
        return new static(
            $args['sujet'],
            $args['type'],
            $args['contenu'],
            $args['email_source'],
            $args['email_destinataire'],
            $args['sendTime'],
            $args['isAnswer']
        );
    }

    /**
     * Returns an array representation of this object.
     *
     * @return array
     */
    public function toArray()
    {
        $retval = array(
            'sujet'     => $this->_sujet,
            'type'      => $this->_type,
            'contenu'      => $this->_contenu,
            'email_source'      => $this->_email_source,
            'email_destinataire'      => $this->_email_destinataire,
            'sendTime'      => $this->_sendTime,
            'isAnswer'      => $this->_isAnswer
        );

        return $retval;
    }

    /**
     * Returns a json representation of this object.
     *
     * @return json The json
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }
}
