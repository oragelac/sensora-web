<?php

namespace Skyfaring\Simple\MVC\Model;

class Adresse extends Model implements CRUD
{
    /**
     * @var string
     */
    public $_street = null;

    /**
     * @var string
     */
    public $_number = null;

    /**
     * @var string
     */
    public $_zipCode = null;

    /**
     * @var string
     */
    public $_locality = null;

    /**
     * @var string
     */
    public $_country = null;

    /**
     * Class constructor.
     *
     * @param string $lastname The user lastname
     * @param string $firstname The user firstname
     * @param string $email The dev email
     * @param string $bank The user bank
     * @param string $payment The user payment
     */
    public function __construct($street, $number, $zipCode, $locality, $country)
    {
        $this->_street = $street;
        $this->_number = $number;
        $this->_zipCode = $zipCode;
        $this->_locality = $locality;
        $this->_country = $country;
    }

    /**
     * street getter.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->_street;
    }

    /**
     * number getter.
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->_number;
    }

    /**
     * zipCode getter.
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->_zipCode;
    }

    /**
     * locality getter.
     *
     * @return string
     */
    public function getLocality()
    {
        return $this->_locality;
    }

    /**
     * country getter.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->_country;
    }



    /**
     * street setter.
     *
     * @param string $street.
     */
    public function setStreet($street)
    {
        $this->_street = $street;
    }

    /**
     * number setter.
     *
     * @param string $number.
     */
    public function setNumber($number)
    {
        $this->_number = $number;
    }

    /**
     * zipCode setter.
     *
     * @param string $zipCode.
     */
    public function setZipCode($zipCode)
    {
        $this->_zipCode = $zipCode;
    }

    /**
     * locality setter.
     *
     * @param string $locality.
     */
    public function setLocality($locality)
    {
        $this->_locality = $locality;
    }

    /**
     * country setter.
     *
     * @param string $country.
     */
    public function setCountry($country)
    {
        $this->_country = $country;
    }



    /**
     * Simple toString.
     *
     * @return string
     */
    public function __toString()
    {
        return pretty_dump($this);
    }

/* ------------------------------------------------------ CRUD Implementation */

    /**
    * {@inheritdoc}
    */
    public function create()
    {
        $request = 'INSERT INTO `Adresse` (`street`,`number`,`zipCode`,`locality`,`country`)'
            .'VALUES ("'
            .$this->_street.'", "'
            .$this->_number.'", "'
            .$this->_zipCode.'", "'
            .$this->_locality.'", "'
            .$this->_country.'");';

        self::$_PDO->query($request);
        $this->_id = self::$_PDO->lastInsertId();
    }

    /**
     * {@inheritdoc}
     */
    public static function read($id)
    {
        try
        {
            $request    = "SELECT `id`, `street`, `number_street`, `zipCode`, `locality`,`country` FROM Adresse WHERE id='$id';";
            $row        = self::$_PDO->query($request);
            $imported   = $row->fetch();
        }

        catch (Exception $e)
        {
            echo 'Error: '.$e->getMessage();
        }

        $address = self::delegate( array(
            'street'    => $imported['street'],
            'number'    => $imported['number_street'],
            'zipCode'   => $imported['zipCode'],
            'locality'  => $imported['locality'],
            'country'   => $imported['country']
        ));
        $address->setId($imported['id']);

        pretty_dump($imported);
        return $address;
    }

    /**
     * {@inheritdoc}
     */
    public function update()
    {
        $request = 'UPDATE `Adresse` '
                   .'SET `street`="'.$this->_street.'", '
                        .'`number`="'.$this->_number.'", '
                        .'`zipCode`="'.$this->_zipCode.'", '
                        .'`locality`="'.$this->_locality.'", '
                        .'`country`="'.$this->_country.'", '
                   .'WHERE id="'.$this->_id.'";';

        self::$_PDO->query($request);
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        $request = 'DELETE FROM `Adresse` WHERE id='.$this->_id.';';

        self::$_PDO->query($request);
    }

    /**
     * {@inheritdoc}
     */
    public static function delegate(array $args)
    {
        return new static(
            $args['street'],
            $args['number'],
            $args['zipCode'],
            $args['locality'],
            $args['country']
        );
    }

    /**
     * Returns an array representation of this object.
     *
     * @return array
     */
    public function toArray()
    {
        $retval = array(
            'street'     => $this->_street,
            'number'      => $this->_number,
            'zipCode'      => $this->_zipCode,
            'locality'      => $this->_locality,
            'country'      => $this->_country
        );

        return $retval;
    }

    /**
     * Returns a json representation of this object.
     *
     * @return json The json
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }
}
