<?php

/* myRaspberry.twig */
class __TwigTemplate_e72b556f975a02f23de2c6948dc4151647e91e17319c982b01bc163eb6550a1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "#HEADER#
<script src=\"/public/js/Chart.js\"></script>
<div ng-app=\"sensora\">

    <section id=\"myRaspberry\" ng-controller=\"myRaspberryCtrl\">

        <ul class=\"left-menu measures-nav\">
            <li ng-repeat=\"raspberry in raspberries\" ng-click=\"setCurrentRaspberry(\$index);\">{[{raspberry._matricule}]}</li>
        </ul>

        <div class=\"measures\">
            <div class=\"content\">
                <div class=\"sensora-box\">

                    <ul class=\"contentMenu\" ng-show=\"currentRaspberry != null\">
                        <li ng-class=\"{true:'active', false:''}[content.type == 'son']\" ng-click=\"content.type = 'son'\" ng-show=\"currentRaspberry._dataSound != null\">
                            <a>Son</a>
                        </li>
                        <li ng-class=\"{true:'active', false:''}[content.type == 'air']\" ng-click=\"content.type = 'air'\" ng-show=\"currentRaspberry._dataAirQuality != null\">
                            <a>Air</a>
                        </li>
                        <li ng-class=\"{true:'active', false:''}[content.type == 'radar']\" ng-click=\"content.type = 'radar'\" ng-show=\"currentRaspberry._dataCarSpeed != null\">
                            <a>Radar</a>
                        </li>
                        <li ng-class=\"{true:'active', false:''}[content.type == 'info']\" ng-click=\"content.type = 'info'\">
                            <a>Info</a>
                        </li>
                    </ul>

                    <div id=\"air\" ng-show=\"content.type =='air'\"> Contenu du air</div>
                    <div id=\"radar\" ng-show=\"content.type =='radar'\"> Contenu du radar</div>
                    <div id=\"info\" ng-show=\"content.type =='info'\">
                        Id : {[{currentRaspberry._id}]}<br>
                        Matricule : {[{currentRaspberry._matricule}]}<br>
                        Is on : {[{currentRaspberry._isOn}]}<br>
                        Date location : {[{currentRaspberry._startLocationTime}]}<br>
                    </div>
                    <div id=\"son\" ng-show=\"content.type =='son'\">
                        <canvas id=\"myChart\"></canvas>
                        <div class=\"tmp\">
                            <button ng-click=\"testInsertSound()\">Insérer de fausses valeur dans le Raspberry test</button>
                            <button ng-click=\"cleanDataSound()\">Supprimer les données</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
";
    }

    public function getTemplateName()
    {
        return "myRaspberry.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "myRaspberry.twig", "/app/Skyfaring/public/html/myRaspberry.twig");
    }
}
