<?php

/* errors/dev.twig */
class __TwigTemplate_cddd102e74219df69a3b6698372f09cf2a3df3148e41c3f3bbd8784a0b107310 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"error-page\"><div class=\"wrapper\">
    <h1>Error ";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
        echo "</h1>

    <p>";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
        echo "</p>

    <div class=\"code-wrapper\">
        <code>";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["stacktrace"]) ? $context["stacktrace"] : null), "html", null, true);
        echo "</code>
    </div>

</div></div>
";
    }

    public function getTemplateName()
    {
        return "errors/dev.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 7,  27 => 4,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "errors/dev.twig", "/app/Skyfaring/Simple/MVC/View/assets/errors/dev.twig");
    }
}
