<?php

/* inscription.twig */
class __TwigTemplate_f83efb06f954dff4575c8d3dea12fffd505b7e327d98a5836ac1c9b59c133385 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "#HEADER#
<div ng-app=\"sensora\">

    <section id=\"inscription\" ng-controller=\"inscriptionCtrl\">
        <div class=\"inscription-container\">
            <div class=\"title\">
                <h1>Inscription</h1>
            </div>

            <div class=\"row\">

                <form name=\"formInscription\" ng-submit=\"addUser()\">

                    <div class=\"element\">
                        <label for=\"nom\">Nom</label>

                        <input type=\"text\" name=\"nom\" ng-model=\"newUser.nom\" input-Directive required>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.nom.\$error.isEmpty\">Le nom est obligatoire</span>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.nom.\$error.haveSpecialChar\">Les caractères spéciaux sont interdits : <>\\&|#(§^{}()\"',</span>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.nom.\$error.haveSpecialWord\">Certains mots sont interdits : select, update, insert, delete, table, drop</span>
                    </div>

                    <div class=\"element\">
                        <label for=\"nom\">Prenom</label>

                        <input type=\"text\" name=\"prenom\" ng-model=\"newUser.prenom\" input-Directive required>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.prenom.\$error.isEmpty\">Le prénom est obligatoire</span>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.prenom.\$error.haveSpecialChar\">Les caractères spéciaux sont interdits : <>\\&|#(§^{}()\"',</span>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.prenom.\$error.haveSpecialWord\">Certains mots sont interdits : select, update, insert, delete, table, drop</span>
                    </div>

                    <div class=\"element\">
                        <label for=\"nom\">Pseudo</label>

                        <input type=\"text\" name=\"pseudo\" ng-model=\"newUser.pseudo\" input-Directive required>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.pseudo.\$error.isEmpty\">Le pseudo est obligatoire</span>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.pseudo.\$error.haveSpecialChar\">Les caractères spéciaux sont interdits : <>\\&|#(§^{}()\"',</span>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.pseudo.\$error.haveSpecialWord\">Certains mots sont interdits : select, update, insert, delete, table, drop</span>
                    </div>

                    <div class=\"element\">
                        <label for=\"nom\">Email</label>

                        <input type=\"email\" name=\"email\" ng-model=\"newUser.email\" input-Directive required>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.email.\$error.isEmpty\">L'email est obligatoire</span>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.email.\$error.haveSpecialChar\">Les caractères spéciaux sont interdits : <>\\&|#(§^{}()\"',</span>
                        <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.email.\$error.haveSpecialWord\">Certains mots sont interdits : select, update, insert, delete, table, drop</span>
                        <!--<div class=\"msgErreurForm\" ng-show=\"formInscription.email.\$dirty && formInscription.email.\$error.email\">L'email n'est pas valide.</div>-->
                    </div>


                    <div class=\"toConfirm\">
                        <div class=\"element\">
                            <label for=\"nom\">Mot de passe</label>

                            <input type=\"password\" name=\"mdp\" ng-model=\"newUser.mdp\" input-Directive required valideMdp-directive longMdpMax=\"longMdpMin\" longMdpMin=\"longMdpMax\">
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.mdp.\$error.isEmpty\">Le sujet est obligatoire</span>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.mdp.\$error.haveSpecialChar\">Les caractères spéciaux sont interdits : <>\\&|#(§^{}()\"',</span>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.mdp.\$error.haveSpecialWord\">Certains mots sont interdits : select, update, insert, delete, table, drop</span>
                        </div>

                        <div class=\"element\">
                            <label for=\"nom\">Confirmation mot de passe</label>

                            <input type=\"password\" name=\"confmdp\" ng-model=\"newUser.confmdp\" ng-pattern=\"newUser.mdp\" input-Directive required valideMdp-directive>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.confmdp.\$error.isEmpty\">Le sujet est obligatoire</span>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.confmdp.\$error.haveSpecialChar\">Les caractères spéciaux sont interdits : <>\\&|#(§^{}()\"',</span>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formInscription.confmdp.\$error.haveSpecialWord\">Certains mots sont interdits : select, update, insert, delete, table, drop</span>
                            <br><div class=\"msgErreurForm needed-warning\" data-ng-show=\"formInscription.confmdp.\$error.pattern\">
                                La confirmation doit être identique au \"Mot de passe\".
                            </div>
                        </div>
                    </div>

                    <button id=\"btn-inscription\" type=\"submit\" class=\"btn btn-perso col-xs-12\" ng-disabled=\"formInscription.\$invalid\">
                        Inscription
                    </button>
                </form>

            </div>
        </div>
    </section>

</div>
";
    }

    public function getTemplateName()
    {
        return "inscription.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "inscription.twig", "/app/Skyfaring/public/html/inscription.twig");
    }
}
