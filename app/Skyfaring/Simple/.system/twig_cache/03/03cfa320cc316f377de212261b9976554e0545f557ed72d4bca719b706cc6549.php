<?php

/* skf/skf-general.twig */
class __TwigTemplate_ab8eb2ac9cdd698426548672f06090b9776016d41c11ba5f35e0c9ccb1e0d923 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"skf-info\">
    <div class=\"skf-version\"><b>Framework:</b> v";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["version"]) ? $context["version"] : null), "html", null, true);
        echo "</div>
    <div class=\"skf-environment\"><b>Environment:</b> ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["environment"]) ? $context["environment"] : null), "html", null, true);
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "skf/skf-general.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "skf/skf-general.twig", "/app/Skyfaring/Simple/MVC/View/assets/skf/skf-general.twig");
    }
}
