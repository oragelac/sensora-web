<?php

/* skf/skf-header.twig */
class __TwigTemplate_76e125498e8f4bae7971e17e1fb403eb2cd8f67af14219469154075d8ef2caeb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class='skf-header'>
    <div class=\"breadcrumb\"><h1><a href='/'>Skyfaring</a></h1></div>
    <div class=\"path\">#PATH#</div>
</header>
";
    }

    public function getTemplateName()
    {
        return "skf/skf-header.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "skf/skf-header.twig", "/app/Skyfaring/Simple/MVC/View/assets/skf/skf-header.twig");
    }
}
