<?php

/* html.twig */
class __TwigTemplate_9f0f986d0c0e7b0cd2afc030852d6ad69ba64b3c9dfdb0c0199dacc80a04542e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["lang"]) ? $context["lang"] : null), "html", null, true);
        echo "\">

    <head>
        <meta charset=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["charset"]) ? $context["charset"] : null), "html", null, true);
        echo "\">

        <title>";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>
        <meta name=\"description\" content=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["description"]) ? $context["description"] : null), "html", null, true);
        echo "\">

        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["icons"]) ? $context["icons"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["icon"]) {
            // line 11
            echo "            <link rel=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["icon"], "name", array()), "html", null, true);
            echo "\" sizes=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["icon"], "size", array()), "html", null, true);
            echo "x";
            echo twig_escape_filter($this->env, $this->getAttribute($context["icon"], "size", array()), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["icon"], "file", array()), "html", null, true);
            echo "\" />
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['icon'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "        <!-- <link rel=\"apple-touch-icon-precomposed\" sizes=\"57x57\" href=\"http://sens.oragelac.com/apple-touch-icon-57x57.png\" />
        <link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"http://sens.oragelac.com/apple-touch-icon-114x114.png\" />
        <link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"http://sens.oragelac.com/apple-touch-icon-72x72.png\" />
        <link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"http://sens.oragelac.com/apple-touch-icon-144x144.png\" />
        <link rel=\"apple-touch-icon-precomposed\" sizes=\"60x60\" href=\"http://sens.oragelac.com/apple-touch-icon-60x60.png\" />
        <link rel=\"apple-touch-icon-precomposed\" sizes=\"120x120\" href=\"http://sens.oragelac.com/apple-touch-icon-120x120.png\" />
        <link rel=\"apple-touch-icon-precomposed\" sizes=\"76x76\" href=\"http://sens.oragelac.com/apple-touch-icon-76x76.png\" />
        <link rel=\"apple-touch-icon-precomposed\" sizes=\"152x152\" href=\"http://sens.oragelac.com/apple-touch-icon-152x152.png\" />
        <link rel=\"icon\" type=\"image/png\" href=\"http://sens.oragelac.com/favicon-196x196.png\" sizes=\"196x196\" />
        <link rel=\"icon\" type=\"image/png\" href=\"http://sens.oragelac.com/favicon-96x96.png\" sizes=\"96x96\" />
        <link rel=\"icon\" type=\"image/png\" href=\"http://sens.oragelac.com/favicon-32x32.png\" sizes=\"32x32\" />
        <link rel=\"icon\" type=\"image/png\" href=\"http://sens.oragelac.com/favicon-16x16.png\" sizes=\"16x16\" />
        <link rel=\"icon\" type=\"image/png\" href=\"http://sens.oragelac.com/favicon-128.png\" sizes=\"128x128\" />
        <meta name=\"application-name\" content=\"Sensora\"/>
        <meta name=\"msapplication-TileColor\" content=\"#FFFFFF\" />
        <meta name=\"msapplication-TileImage\" content=\"http://sens.oragelac.com/mstile-144x144.png\" />
        <meta name=\"msapplication-square70x70logo\" content=\"http://sens.oragelac.com/mstile-70x70.png\" />
        <meta name=\"msapplication-square150x150logo\" content=\"http://sens.oragelac.com/mstile-150x150.png\" />
        <meta name=\"msapplication-wide310x150logo\" content=\"http://sens.oragelac.com/mstile-310x150.png\" />
        <meta name=\"msapplication-square310x310logo\" content=\"http://sens.oragelac.com/mstile-310x310.png\" /> -->

        ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["css"]) ? $context["css"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
            // line 35
            echo "            <link href=\"";
            echo twig_escape_filter($this->env, $context["file"], "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\">
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "    </head>

    <body>
        <div class=\"se-pre-con\"></div>

        ";
        // line 42
        try {
            $this->loadTemplate((isset($context["page_header"]) ? $context["page_header"] : null), "html.twig", 42)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 43
        echo "        ";
        $this->loadTemplate((isset($context["page_content"]) ? $context["page_content"] : null), "html.twig", 43)->display($context);
        // line 44
        echo "        ";
        try {
            $this->loadTemplate((isset($context["page_footer"]) ? $context["page_footer"] : null), "html.twig", 44)->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 45
        echo "
        ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["js"]) ? $context["js"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
            // line 47
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $context["file"], "html", null, true);
            echo "\"></script>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 49,  129 => 47,  125 => 46,  122 => 45,  114 => 44,  111 => 43,  104 => 42,  97 => 37,  88 => 35,  84 => 34,  61 => 13,  46 => 11,  42 => 10,  37 => 8,  33 => 7,  28 => 5,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "html.twig", "/app/Skyfaring/Simple/MVC/View/assets/html.twig");
    }
}
