<?php

/* skf/skf-content.twig */
class __TwigTemplate_8d1b3cd0c76434981aa0522e2ba6c7dafea0a153edc9b2d92025e73cd96ed93e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"skf-nav\">
    <ul>
        ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 4
            echo "            <li><a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["link"], "location", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["link"], "name", array()), "html", null, true);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "        <!-- <li><a href=\"/config\">Index</a></li>
        <li><a href=\"/config/routes\">Routes</a></li> -->
    </ul>
</div>

<div class=\"skf-content\">
    ";
        // line 12
        echo twig_include($this->env, $context, (isset($context["content"]) ? $context["content"] : null));
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "skf/skf-content.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 12,  38 => 6,  27 => 4,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "skf/skf-content.twig", "/app/Skyfaring/Simple/MVC/View/assets/skf/skf-content.twig");
    }
}
