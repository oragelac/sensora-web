<?php

/* contact.twig */
class __TwigTemplate_61c26d02cdbb60e97d966bc91883f14207412b1240508da3341b70c9aab9fc59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "#HEADER#
<div ng-app=\"sensora\">
    <section id=\"contact\" ng-controller=\"contactCtrl\">

        <ul class=\"left-menu\">
            <li ng-class=\"{true:'active', false:''}[contact.type == 'compte']\" ng-click=\"contact.type = 'compte'\">Problème de compte</li>
            <li ng-class=\"{true:'active', false:''}[contact.type == 'administratif']\" ng-click=\"contact.type = 'administratif'\">Administratif</li>
            <li ng-class=\"{true:'active', false:''}[contact.type == 'produit']\" ng-click=\"contact.type = 'produit'\">Produit</li>
            <li ng-class=\"{true:'active', false:''}[contact.type == 'autre']\" ng-click=\"contact.type = 'autre'\">Autre</li>
        </ul>

        <div class=\"measures\">
            <div class=\"content\">
                <div class=\"sensora-box\">
                    <form name=\"formContact\" ng-submit=\"sendMessage()\">

                        <div class=\"element\">
                            <input type=\"text\" name=\"sujet\" ng-model=\"contact.sujet\" class=\"sensora-input\" placeholder=\"Sujet\" input-Directive required>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formContact.sujet.\$error.isEmpty\">Le sujet est obligatoire</span>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formContact.sujet.\$error.haveSpecialChar\">Les caractères spéciaux sont interdits : <>\\&|#(§^{}()\"',</span>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formContact.sujet.\$error.haveSpecialWord\">Certains mots sont interdits : select, update, insert, delete, table, drop</span>

                            <input type=\"email\" name=\"emailSource\" ng-model=\"contact.emailSource\" class=\"sensora-input\" placeholder=\"Email\" input-Directive required>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formContact.emailSource.\$error.isEmpty\">L'email est obligatoire</span>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formContact.emailSource.\$error.haveSpecialChar\">Les caractères spéciaux sont interdits : <>\\&|#(§^{}()\"',</span>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formContact.emailSource.\$error.haveSpecialWord\">Certains mots sont interdits : select, update, insert, delete, table, drop</span>
                        </div>


                        <div class=\"element\">
                            <textarea type=\"text\" name=\"contenu\" ng-model=\"contact.contenu\" class=\"sensora-input\" placeholder=\"Contenu du message\" input-Directive required></textarea>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formContact.contenu.\$error.isEmpty\">Le contenu est obligatoire</span>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formContact.contenu.\$error.haveSpecialChar\">Les caractères spéciaux sont interdits : <>\\&|#(§^{}()\"',</span>
                            <span class=\"msgErreurForm needed-warning\" ng-show=\"formContact.contenu.\$error.haveSpecialWord\">Certains mots sont interdits : select, update, insert, delete, table, drop</span>
                        </div>

                        <div class=\"element\">
                            <button class=\"sensora-button\" type=\"submit\" ng-disabled=\"formContact.\$invalid\">Envoyer</button>
                        </div>

                        ";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["errorSendEmail"]) ? $context["errorSendEmail"] : null), "html", null, true);
        echo "

                    </form>

                </div>
            </div>
        </div>

    </section>
</div>
";
    }

    public function getTemplateName()
    {
        return "contact.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 41,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "contact.twig", "/app/Skyfaring/public/html/contact.twig");
    }
}
