<?php

/* home.twig */
class __TwigTemplate_699c36828671f6cca6a91f002f42b3e7f8f132fc655cc80d3b0b1fa812760f31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div ng-app=\"sensora\">
    <div class=\"sensora-home\" id=\"fullpage\">

<!------------------------------------------------------------- INTRODUCTION -->

<section class=\"section\" id=\"section-intro\">
    <div class=\"logo-container\">
        <div class=\"logo-typo\"></div>
    </div>

    <h2>Rencontrez Sensora, la balise du foyer</h2>
    <div class=\"arrow\">
        <i class=\"material-icons\">keyboard_arrow_down</i>
    </div>
</section>

<!---------------------------------------------------------------- AGITATION -->

<section class=\"section\" id=\"section-problem\">
    <div class=\"illustration-neocity\"></div>
    <article class=\"sensora-article\"><div class=\"relative-center\">

            <div>
                <h2>Vie moderne, siège d'agitation.</h2>

                <p>
                    La vie est remplie de tracas, de délais à respecter,
                    d'exigences à satisfaire et de frustrations à affronter.
                    Dans notre vie de tous les jours, nous sommes accoutumés
                    au stress qui résulte de nos activités et l'affrontons
                    volontier pour atteindre nos objectifs.
                    Cependant le 21ième siècle et toutes les nouvelles
                    technologies qu'il apporte amènent une dimension
                    nouvelle à cette agitation.
                </p>

                <p><strong>
                    Chez Sensora, nous pensons que cette effervescence
                    devrait trouver frontière aux portes de votre foyer.
                </strong></p>

                <p>
                    La balise <strong>Sensoward</strong> se propose comme
                    première étape dans cette dynamique de protection et
                    vous propose d'observer de manière objective votre
                    lieu de vie.
                </p>
            </div>

    </div></article>
    <div class=\"arrow\"><i class=\"material-icons\">keyboard_arrow_down</i></div>
</section>

<!-------------------------------------------------------------- EXPLANATION -->

<section class=\"section\" id=\"section-explanation\">
    <div class=\"illustration-neighborhood\"></div>
    <article class=\"sensora-article\">

        <div class=\"relative-center\">

            <h2>Comment ça marche ?</h2>
            <div class=\"caption\">Etape par etape</div>

            <div class=\"slider\">
                <div class=\"slide\" data-anchor=\"explanation-s1\"> Slide 1 </div>
                <div class=\"slide\" data-anchor=\"explanation-s2\"> Slide 2 </div>
                <div class=\"slide\" data-anchor=\"explanation-s3\"> Slide 3 </div>
            </div>

        </div>

        <p>
            N'hésitez pas à visiter la <a href=\"#faq\">foire aux questions</a><br>
            où à nous <a href=\"#contact\">contacter</a> pour tous problèmes !
        </p>

    </article>
    <div class=\"arrow\"><i class=\"material-icons\">keyboard_arrow_down</i></div>
</section>

<!-------------------------------------------------------------------- DUNNO -->

<section class=\"section\">
    <div class=\"illustration-neighborhood\"></div>
    <article class=\"sensora-article\"><div class=\"relative-center\">

        <div class=\"text\">
            <h2>TODO</h2>
        </div>

        <div class=\"student-logo text-illustration\"></div>
    </div></article>
</section>

<!------------------------------------------------------------------ FOOTER -->

<section class=\"footer section fp-auto-height\">
    <div class=\"wrapper\">

        <div class=\"lists\">
            <ul class=\"info\">
                <li><a href=\"#\">A propos</a></li>
                <li><a href=\"#\">FAQ</a></li>
                <li><a href=\"#\">Documentation</a></li>
                <li><a href=\"#\">Qui sommes-nous ?</a></li>
                <li><a href=\"#\">Contact</a></li>
            </ul>

            <ul class=\"social\">
                <li><a href=\"#\"><div class=\"twitter\"></div></a></li>
                <li><a href=\"#\"><div class=\"facebook\"></div></a></li>
            </ul>
        </div>

        <a href=\"http://www.ephec.be\" target=\"_blank\" class=\"ephec\"></a>
        <div class=\"design\">Design by Martin Vogeleer & Florian Indot</div>
        <div class=\"copyright\">Sensora ©2016. All rights reserved.</div>

    </div>
</section>

<!------------------------------------------------------------------ THE END -->

    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "home.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "home.twig", "/app/Skyfaring/public/html/home.twig");
    }
}
