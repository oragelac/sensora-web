<?php

/* skf/skf-routes.twig */
class __TwigTemplate_368b81e450b76e96f1cafd666be3a213df63c8e373aad63aaa6a36f7281b1d04 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"route-page\">
    <div class=\"nb-route\">Defined routes: ";
        // line 2
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["routes"]) ? $context["routes"] : null)), "html", null, true);
        echo "</div>
    <table><tbody>
        <tr class=\"border-bottom\">
            <th>Name</th>
            <th>Path</th>
            <th>Controller</th>
            <th>Action</th>
        </tr>
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["routes"]) ? $context["routes"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["route"]) {
            // line 11
            echo "            <tr>
                <td>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["route"], "getName", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["route"], "getPath", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 14
            echo twig_escape_filter($this->env, twig_last($this->env, twig_split_filter($this->env, $this->getAttribute($context["route"], "getController", array()), "\\")), "html", null, true);
            echo "</td>
                <td>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["route"], "getAction", array()), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['route'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "        <tr><td colspan=\"4\"><button>Add route</button></td></tr>
    </tbody></table>
</div>
";
    }

    public function getTemplateName()
    {
        return "skf/skf-routes.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 18,  52 => 15,  48 => 14,  44 => 13,  40 => 12,  37 => 11,  33 => 10,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "skf/skf-routes.twig", "/app/Skyfaring/Simple/MVC/View/assets/skf/skf-routes.twig");
    }
}
