<?php

namespace Skyfaring\Simple;

/**
 * Simple Recursive / Namespace Aware Autoloader.
 *
 * A simple autoloader that looks and loads classes recursively starting at the
 * project root directory OR at a namespace correlating directory structure if
 * a namespace is specified for the given class.
 *
 * Special namespace / path correlations can be added for file lookup via
 * the addPrefix() method.
 *
 * @author eka
 */
class Autoloader
{
    /**
     * @var string
     */
    protected static $DOCUMENT_ROOT = null;

    /**
     * @var string
     */
    protected static $EXTENSION = null;

    /**
     * @var array
     */
    protected static $PREFIXES = array();

    /**
     * Class-wide initializer.
     *
     * Defines the constants this class shall use.
     *
     * @param string $DOCUMENT_ROOT This server document root (without Skyfaring)
     * @param string $EXTENSION     The extension of class files (.php / .class.php / ...)
     */
    public static function initialize($DOCUMENT_ROOT = '/app', $EXTENSION = '.php')
    {
        self::$DOCUMENT_ROOT = $DOCUMENT_ROOT;
        self::$EXTENSION = $EXTENSION;
    }

    /**
     * Registers this class as an spl autoloader.
     */
    public static function register()
    {
        spl_autoload_register(array(__CLASS__, 'load'));
    }

    /**
     * Adds a path for the given namespace to this autoloader.
     *
     * @param string $namespace The correlating namespace
     * @param string $path      The path to look in for this namespace
     *
     * @throws \InvalidArgumentException for $namespace or $path that wouldnt be
     *                                   of strings
     */
    public static function addPrefix($namespace, $path)
    {
        if (!is_string($namespace) || !is_string($path)) {
            throw new \InvalidArgumentException(
                'Prefix namespaces and paths must be of type string',
                42 // TODO - Implements error codes
            );
        }

        self::$PREFIXES[$namespace] = $path;
    }

    /**
     * Loads the class file for a given class name.
     *
     * @param string $class The class name
     */
    public static function load($class)
    {
        if (!isset(self::$DOCUMENT_ROOT) || !isset(self::$EXTENSION)) {
            self::initialize();
        }

        list($here, $path, $filename) = self::translate($class);

        // Skyfaring\Simple tree search
        $directory = new \RecursiveDirectoryIterator(
            self::$DOCUMENT_ROOT.'/'.$here,
            \RecursiveDirectoryIterator::SKIP_DOTS
        );

        // If the file does not exist within Skyfaring/Simple,
        // we check registered directories.
        if (!self::seekIn($directory, $filename)) {
            foreach (self::$PREFIXES as $namespace => $directoryString) {
                $classNamespace = str_replace('/', '\\', '/'.$here.$path);
                $directory = new \RecursiveDirectoryIterator(self::$DOCUMENT_ROOT.$path);
                if ($classNamespace == $namespace && self::seekIn($directory, $filename)) {
                    break;
                }
            }
        }
    }

    /**
     * Traverses a given directory for a file.
     * Includes it if found, returns false otherwise.
     *
     * @param \RecursiveDirectoryIterator $directory The directory to traverse
     * @param string                      $filename  The filename
     *
     * @return bool Wheter the file has been found
     */
    public static function seekIn(\RecursiveDirectoryIterator $directory, $filename)
    {
        $files = new \RecursiveIteratorIterator(
            $directory,
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $file) {
            if (($file->getFileName() === $filename) && $file->isReadable()) {
                include_once $file->getPathName();

                return true;
            }
        }

        return false;
    }

    /**
     * Translates a namespace into a directory path.
     * This path is relative to the current directory aka ../Skyfaring/Simple.
     *
     * @param string $namespace The namespace
     *
     * @return array(0 => currentDir, 1 => pathRelativetoCurrentDir, 2 => filename)
     */
    public static function translate($namespace)
    {
        $workingDirectory = str_replace('\\', '/', __NAMESPACE__);
        $totalClassPath = str_replace('\\', '/', $namespace);
        $relativeClassPath = str_replace($workingDirectory, '', $totalClassPath);
        $relativeClassPath = trim($relativeClassPath);

        $pathArray = explode('/', $relativeClassPath);

        $filename = $pathArray[count($pathArray) - 1];
        $filename .= self::$EXTENSION;

        $path = '';
        for ($i = 0; $i < count($pathArray) - 1; ++$i) {
            $path .= '/'.$pathArray[$i];
        }

        return array($workingDirectory, $path, $filename);
    }
}
