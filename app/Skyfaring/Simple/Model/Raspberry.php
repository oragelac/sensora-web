<?php
namespace Skyfaring\Simple\MVC\Controller;

//use Skyfaring\Simple\Model;

class Raspberry {

    public $id;
    public $matricule;
    public $startLocationTime;
    public $idState;

    public function __construct($id, $matricule) {
        $this->id = $id;
        $this->matricule = $matricule;
        $this->dateToday = date("Y-m-d H:i:s");
    }

    public function getAll() {
        try
        {
            $bdd = new \PDO("mysql:host=oragelac.com;
            dbname=sensora", "root" , "waffles4love");
        }
        catch (Exception $e)
        {
            echo "<script>alert('Erreur de connexion')</script>";
            die('Erreur : '. $e->getMessage());
        }
        /*$res = $bdd->query("SELECT R.id,R.matricule, R.idState, R.startLocationTime,  S.decibel, S.time
                                    FROM sensora.Raspberry R
                                    LEFT JOIN sensora.Inter_data I ON I.idRaspberry=R.id
                                    LEFT JOIN sensora.Son S ON I.idSon=S.id
                                    WHERE R.idUser='16'");//TODO get real id user

        while ($donnee = $res->fetch(\PDO::FETCH_ASSOC)) {
            $raspberry["id"] = $donnee["id"];
            $raspberry["matricule"] = html_entity_decode($donnee["matricule"]);
            $raspberry["idState"] = html_entity_decode($donnee["idState"]);
            $raspberry["startLocationTime"] = html_entity_decode($donnee["startLocationTime"]);
            $raspberry["decibel"] = html_entity_decode($donnee["decibel"]);
            $raspberry["time"] = html_entity_decode($donnee["time"]);

            $resultat[] = $raspberry;
        }*/
        //GET EACH RASPBERRY //TODO trouver meilleur requète SQL
        $res = $bdd->query("SELECT R.id,R.matricule, R.idState, R.startLocationTime
                                    FROM sensora.Raspberry R
                                    WHERE R.idUser='16'");//TODO get real id user
        while ($donnee = $res->fetch(\PDO::FETCH_ASSOC)) {
            $raspberry["id"] = $donnee["id"];
            $idRaspberry = $raspberry["id"] ;
            $raspberry["matricule"] = html_entity_decode($donnee["matricule"]);
            $raspberry["idState"] = html_entity_decode($donnee["idState"]);
            $raspberry["startLocationTime"] = html_entity_decode($donnee["startLocationTime"]);
            //GET ALL DATA FROM EACH RASPBERRY
            $req = $bdd->query("SELECT S.decibel, S.time
                                        FROM sensora.Inter_data I
                                        JOIN sensora.Son S ON I.idSon=S.id
                                        WHERE I.idRaspberry='$idRaspberry'");
            $son=null;
            while ($data = $req->fetch(\PDO::FETCH_ASSOC)) {
                $son["decibel"][] = html_entity_decode($data["decibel"]);
                $son["time"][] = html_entity_decode($data["time"]);

            }
            $raspberry["son"] = $son;
            $resultat[] = $raspberry;
        }
        return $resultat;
    }

}

 ?>
