<?php

namespace Skyfaring\Simple\Routing;

class Crossroads implements \IteratorAggregate, \Countable
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var array
     */
    protected $_routes = array();

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Retrieves this object routes.
     *
     * @return array The routes
     */
    public function all()
    {
        return $this->_routes;
    }

    /**
     * Adds a route to the collection.
     *
     * @param string $name  The route name
     * @param Route  $route The route
     *
     * This method implements a fluent interface
     */
    public function add($name, Route $route)
    {
        $this->_routes[$name] = $route;

        return $this;
    }

    /**
     * Retrieves a route.
     *
     * @param string $name The route name
     */
    public function get($name)
    {
        return $this->_routes[$name];
    }

    /**
     * Merge a Crossroads into this Crossroads.
     *
     * @param RouteCollection $collection The Crossroads that will be mergeds
     * @param bool            $erase      Wheter this object routes should be
     *                                    erased by the new routes
     *
     * This method implements a fluent interface
     */
    public function merge(Crossroads $collection, $erase = true)
    {
        foreach ($collection->all() as $name => $route) {
            if ($erase) {
                $this->_routes[$name] = $route;
            } else {
                $this->_routes[$name] = (isset($this->_routes[$name])
                    ? $this->_routes[$name]
                    : $route
                );
            }
        }

        return $this;
    }

    /**
     * Removes a route from the collection.
     *
     * @param string $name The route name
     *
     * @return Route The removed route
     */
    public function remove($name)
    {
        $route = $this->_routes[$name];
        unset($this->_routes[$name]);

        return $route;
    }

    /**
     * Seek the given path in this route collection routes.
     *
     * @param string $path The seeked path
     *
     * @return mixed The found route name OR false on failure
     */
    public function seek($path)
    {
        foreach ($this->_routes as $route) {
            if ($route->is($path)) {
                return $route->getName();
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return count($this->_routes);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->_routes);
    }
}
