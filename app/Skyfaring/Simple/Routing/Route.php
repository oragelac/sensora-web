<?php

namespace Skyfaring\Simple\Routing;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\Model as SModel;

/**
 * Représentation d'un chemin pris par l'utilisateur.
 *
 * L'objectif étant de sortir du schéma '/chemin/vers/le/fichier.html' et de
 * se dirigier vers des URL réécrites auxquelles correspondent un controlleur
 * et une méthode donnée dans le respect du principe MVC.
 */
class Route
{
    public static $HTTP_METHODS = array('GET', 'PUT', 'POST', 'DELETE');
    protected static $CONTROLLER_NAMESPACE = 'Skyfaring\Simple\MVC\Controller';

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var string
     */
    protected $_name = null;

    /**
     * @var string
     */
    protected $_path = null;

    /**
     * @var $regex
     */
    protected $_pattern = null;

    /**
     * @var boolean
     */
    protected $_dynamic = false;

    /**
     * @var array
     */
    protected $_availableMethods = array();

    /**
     * @var string
     */
    protected $_controller = null;

    /**
     * @var callable
     */
    protected $_action = null;

    /**
     * Class constructor.
     *
     * @param string $name       The route name
     * @param string $path       The path to reach this route
     * @param string $controller An existing controller name
     * @param string $action     An existing controller method
     */
    public function __construct($name, $path, $controller, $action,
    array $availableMethods = array('GET', 'PUT', 'POST', 'DELETE'))
    {
        $this->_name = $name;
        $this->setPath($path);
        $this->_pattern = $this->parse($path);
        $this->setController($controller);
        $this->setAction($action);
        $this->setAvailableMethods($availableMethods);
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Parses a given route path and returns the corresponding regex.
     *
     * @param string $path The route path to parse
     * @return string The parsed path w\ regex
     */
    public function parse($path)
    {
        if ($path == '/') return '@^'.$path.'$@';

        $regex = '@';
        $parts = explode('/', $path);

        if($path[0] == '/')
        {
            array_shift($parts);
        }

        foreach ($parts as $part)
        {
            $regex .= '/';
            $args = explode(':', $part);

            if(sizeof($args) == 1)
            {
                $regex .= sprintf(S\REGEX_STATIC, preg_quote(array_shift($args), '@'));
                continue;
            }

            elseif($args[0] == '')
            {
                array_shift($args);
                $type = false;
            }

            else
            {
                $type = array_shift($args);
            }

            $this->_dynamic = true;

            $key = array_shift($args);

            if($key == 'regex')
            {
                $regex .= $key;
                continue;
            }

            $regex .= '(?P<'.$key.'>';
            switch(strtolower($type))
            {
                case 'int':
                case 'integer':
                    $regex .= S\REGEX_INT;
                    break;

                case 'alpha':
                    $regex .= S\REGEX_ALPHA;
                    break;

                case 'alphanumeric':
                case 'alphanum':
                case 'alnum':
                    $regex .= S\REGEX_ALPHANUMERIC;
                    break;

                default:
                    $regex .= S\REGEX_ANY;
                    break;
            }
            $regex .= ')';
        }
        $regex .='$@u';

        return $regex;
    }

    /**
     * Sets this route name.
     *
     * @param string $name The new name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * Retreives this route name.
     *
     * @return string The name
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Sets the path.
     *
     * If the given path is null, this method determines by itself the logical
     * path this route should answer to.
     *
     * @param string $path The path if any
     */
    public function setPath($path)
    {
        $this->_path = isset($path) ? $path : '/'.$this->_controller.'/'.$this->_action;
    }

    /**
     * Retreives the path.
     *
     * @return string The path
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * Sets this route available methods.
     *
     * @param array $methods The HTTP methods (GET / PUT / ...)
     *
     * @throws \InvalidArgumentException for unrecognized HTTP methods
     */
    public function setAvailableMethods(array $methods)
    {
        foreach ($methods as $method) {
            if (!in_array($method, self::$HTTP_METHODS)) {
                throw new \InvalidArgumentException(
                    'Unrecognized http route method '.$method,
                    1003
                );
            }
        }

        $this->_availableMethods = $methods;
    }

    /**
     * Retreives this route available methods.
     *
     * @return array The available methods
     */
    public function getAvailableMethods()
    {
        return $this->_availableMethods;
    }

    /**
     * Sets the controller.
     *
     * @param string $controller The controller name
     *
     * @return this For chaining
     */
    public function setController($controller)
    {
        $controller = ucfirst($controller);

        !class_exists($controller, true)
        && ($controller = self::$CONTROLLER_NAMESPACE.'\\'.$controller);

        if (!class_exists($controller, true)) {
            throw new \InvalidArgumentException(
                "The controller [$controller] has not been defined.",
                1004
            );
        }

        $this->_controller = $controller;

        return $this;
    }

    /**
     * Retreives the controller name.
     *
     * @return string The controller name
     */
    public function getController()
    {
        return $this->_controller;
    }

    /**
     * Sets the action (method).
     *
     * @param callable $action The action (method) name
     *
     * @return this For chaining
     */
    public function setAction($action)
    {
        $reflector = new \ReflectionClass($this->_controller);

        if (!$reflector->hasMethod($action))
        {
            throw new \InvalidArgumentException(
                "The action '$action' for controller '$this->_controller'
                has not been defined.",
                1005
            );
        }

        $this->_action = $action;

        return $this;
    }

    /**
     * Retreives the action name.
     *
     * @return string The action name
     */
    public function getAction()
    {
        return $this->_action;
    }

    /**
     * Checks wheter or not this route path is the given path.
     *
     * @param string $path The seeked path
     *
     * @return bool
     */
    public function is($path)
    {
        return $path == $this->_path || preg_match($this->_pattern, $path);
    }

    /**
     * Checks wheter the given method is allowed or not by this route and
     * calls this route controller action with the given parameters.
     */
    public function run($method, $param = null)
    {
        $dynamic_vars = array();

        if($this->_dynamic)
        {
            $parts = explode('/', $this->_path);
            $nb_parts = count($parts);

            $rq_path = S\App::getRequest()->getUri()->getPath();
            $rq_parts = explode('/', $rq_path);

            for ($i = 0; $i < $nb_parts ; ++$i)
            {
                $part = $parts[$i];
                if($part == '' || $part == '/')
                {
                    continue;
                }

                $regex = explode(':', $part);
                if(count($regex) > 1)
                {
                    $dynamic_vars[$regex[1]] = $rq_parts[$i];
                }
            }
        }

        if (in_array($method, $this->_availableMethods))
        {
            call_user_func_array(
                array(new $this->_controller(null, null, $dynamic_vars), $this->_action),
                array($method, $param)
            );
        }

        else
        {
            throw new s\SimpleException(
                'Unhandled request method ['.$method.'] to controller ['.$this->_controller.'].',
                1006
            );
        }
    }
}
