## ========================================================================== ##
#                                                                              #
# Skyfaring Global Configuration File                                          #
#                                                                              #
# This file is intended to describe internal Skyfaring behaviour.              #
# To create and update your application behaviour, please head to              #
# public/config.yml                                                            #
#                                                                              #
## ========================================================================== ##

# Environment variable. Affects error display and log levels.
environment: 'dev'
version: '0.3.6'

# Defines wheter or not mail activation should be used to register accounts.
mail_activation: false

# Annotation store which regroup shared Routes attributes and behaviours.
store:
    skf-css:
        - '../public/css/normalize.css'
        - '/Simple/MVC/View/assets/css/skf.css'
    skf-html:
        content: 'skf/skf-content.twig'
        general: 'skf/skf-general.twig'
        header: 'skf/skf-header.twig'
        list: 'skf/skf-routes.twig'
        route: 'skf/skf-route.twig'

    skf-error:
        dev: 'errors/dev.twig'
        conf: 'errors/configuration.twig'
        prod: 'errors/prod.twig'

    sensora-html:
        home: 'home.twig'
        connection: 'connection.twig'
        header: 'header.twig'
        inscription: 'inscription.twig'
        myRaspberry: 'myRaspberry.twig'
        contact: 'contact.twig'
        shop: 'shop.twig'
        administration: 'administration.twig'
        profil: 'profil.twig'
        annex: 'annex.twig'

    sensora-css:
        - '/public/css/normalize.css'
        - '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css'
        - 'https://fonts.googleapis.com/icon?family=Material+Icons'
        - 'https://fonts.googleapis.com/css?family=Bree+Serif'
        - '/public/css/jquery.fullPage.css'
        - '/public/flaticon/flaticon.css'
        - '/public/Dragular/dragular.min.css'
        - '/public/css/dragula.css'
        - '/public/angularjs-slider/rzslider.css'
        - '/public/css/sensora.css'

    sensora-js:
        - '/public/js/angular.js'
        - '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js'
        - 'https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-animate.min.js'
        - 'https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-aria.min.js'
        - 'https://ajax.googleapis.com/ajax/libs/angular_material/1.0.7/angular-material.min.js'
        - '/public/js/Chart.js'
        - '/public/js/controller.js'
        - '/public/Dragular/dragular.min.js'
        - '/public/js/jquery-3.1.1.min.js'
        - '/public/js/scrolloverflow.min.js'
        - '/public/js/jquery.fullPage.min.js'
        - '/public/js/dragula.js'
        - '/public/js/master.js'
        - '/public/Jrumble/jquery.jrumble.1.3.js'
        - '/public/JsCookie/js.cookie.js'
        - '/public/angularjs-slider/rzslider.min.js'
        - '/public/js/datetime.js'

    sensora-logo: '/public/img/Sensora_logo_little.png'

    sensora-typo: '/public/img/Typo.png'


routes:
    default:
        route:
            path: '/'
            methods:
                - 'GET'
            controller: 'SensoraHome'
            action: 'index'

        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora'
                description: 'Accueil'
                file: '@sensora-html'
                css: '@sensora-css'
                js: '@sensora-js'

    sensora-accueil:
        route:
            path: '/home'
            methods:
                - 'GET'
            controller: 'SensoraHome'
            action: 'index'

        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora - Home'
                description: 'Accueil'
                file: '@sensora-html'
                css: '@sensora-css'
                js: '@sensora-js'

    sensora-connection:
        route:
            path: '/connection'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraConnection'
            action: 'connect'

        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora - Connection'
                description: 'Sensora - Connection'
                file: '@sensora-html'
                css: '@sensora-css'
                js:
                    - '/public/js/angular.js'
                    - '/public/js/controller.js'
                    - '/public/js/jquery-3.1.1.min.js'
                    - '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js'
                    - '/public/js/master.js'
                    - '/public/JsCookie/js.cookie.js'

    sensora-disconnection:
        route:
            path: '/disconnection'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraConnection'
            action: 'disconnect'

        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora - Disconnection'
                description: 'Disconnection'
                file:
                css:
                js:

    sensora-inscription:
        route:
            path: '/registration'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraInscription'
            action: 'inscription'

        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora - Subscribe'
                description: 'Inscription'
                file: '@sensora-html'
                css: '@sensora-css'
                js:
                    - '/public/js/angular.js'
                    - '/public/js/controller.js'
                    - '/public/js/jquery-3.1.1.min.js'
                    - '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js'
                    - '/public/js/master.js'
                    - '/public/JsCookie/js.cookie.js'

    sensora-addUser:
        route:
            path: '/addUser'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraInscription'
            action: 'addUser'

    sensora-raspberry:
        route:
            path: '/sensoward'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraRaspberry'
            action: 'index'

        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora - Sensoward'
                description: 'Raspberry'
                file: '@sensora-html'
                css: '@sensora-css'
                js: '@sensora-js'

    sensora-sendData:
        route:
            path: '/sendData'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraRaspberry'
            action: 'insertData'

    sensora-deleteDataRaspberry:
        route:
            path: '/deleteDataRaspberry/int:id'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraRaspberry'
            action: 'deleteData'

    sensora-shop:
        route:
            path: '/shop'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraShop'
            action: 'index'

        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora - Shop'
                description: 'Shop'
                file: '@sensora-html'
                css: '@sensora-css'
                js:
                    - '/public/js/angular.js'
                    - '/public/js/controller.js'
                    - '/public/Dragular/dragular.min.js'
                    - '/public/js/jquery-3.1.1.min.js'
                    - '/public/js/dragula.js'
                    - '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js'
                    - '/public/js/master.js'
                    - '/public/JsCookie/js.cookie.js'

    sensora-buy:
        route:
            path: '/buy'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraShop'
            action: 'buy'

    sensora-contact:
        route:
            path: '/contact'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraContact'
            action: 'index'

        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora - Contact'
                description: 'Contact'
                file: '@sensora-html'
                css: '@sensora-css'
                js:
                    - '/public/js/angular.js'
                    - '/public/js/controller.js'
                    - '/public/js/jquery-3.1.1.min.js'
                    - '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js'
                    - '/public/js/master.js'
                    - '/public/JsCookie/js.cookie.js'

    sensora-sendMessage:
        route:
            path: '/sendMessage'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraContact'
            action: 'sendMessage'

    sensora-sendEmail:
        route:
            path: '/sendEmail'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraContact'
            action: 'sendEmail'

    sensora-authenticate:
        route:
            path: '/authentication'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraConnection'
            action: 'authenticate'

    sensora-getAllFrom:
        route:
            path: '/getAllFrom'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraRaspberry'
            action: 'getAllFrom'

    sensora-getAllRaspberry:
        route:
            path: '/getAllRaspberry'
            methods:
                - 'GET'
            controller: 'SensoraRaspberry'
            action: 'getAll'

    sensora-administration:
        route:
            path: '/administration'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraAdministration'
            action: 'index'

        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora - Administration'
                description: 'Administration'
                file: '@sensora-html'
                css: '@sensora-css'
                js:
                    - '/public/js/angular.js'
                    - '/public/js/controller.js'
                    - '/public/js/jquery-3.1.1.min.js'
                    - '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js'
                    - '/public/js/master.js'
                    - '/public/Jrumble/jquery.jrumble.1.3.js'
                    - '/public/JsCookie/js.cookie.js'

    sensora-mail-authenticate:
        route:
            path: '/mail/authenticate'
            methods:
                - 'POST'
            controller: 'SensoraAdministration'
            action: 'authenticate'

    sensora-getAllMessage:
        route:
            path: '/getAllMessage'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraContact'
            action: 'getAll'

    sensora-profil:
        route:
            path: '/profil/int:id'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraProfil'
            action: 'index'

        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora - Profil'
                description: 'Profil'
                file: '@sensora-html'
                css: '@sensora-css'
                js: '@sensora-js'

    sensora-getUser:
        route:
            path: '/getUser'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraUser'
            action: 'getUser'

    sensora-getAllUser:
        route:
            path: '/getAllUser'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraUser'
            action: 'getAllUser'

    sensora-saveUser:
        route:
            path: '/saveUser'
            methods:
                - 'GET'
                - 'POST'
            controller: 'SensoraUser'
            action: 'saveUser'

    sensora-annex:
        route:
            path: '/annex/alnum:section'
            methods:
                - 'GET'
            controller: 'SensoraAnnex'
            action: 'index'

        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora - Profil'
                description: 'Profil'
                file: '@sensora-html'
                css:
                    - 'https://fonts.googleapis.com/icon?family=Material+Icons'
                    - 'https://fonts.googleapis.com/css?family=Bree+Serif'
                    - '/public/css/normalize.css'
                    - '/public/css/sensora.css'
                js:
                    - '/public/js/jquery-3.1.1.min.js'
                    - '/public/js/annex.js'

    error:
        content:
            html:
                charset: 'UTF-8'
                title: 'Sensora - Error'
                description: 'An error occured'
                file: '@skf-error'
                css: '@skf-css'
                js:

    skf-config:
        route:
            path: '/config'
            methods:
                - 'GET'
                - 'POST'
            controller: 'ConfigController'
            action: 'index'

        content:
            html:
                charset: 'UTF-8'
                title: 'Configuration'
                description: 'Skyfaring configuration panel'
                file: '@skf-html'
                css: '@skf-css'
                js:

        constraint:
            environment: 'dev'

    skf-route:
        route:
            path: '/config/routes'
            methods:
                - 'GET'
                - 'POST'
            controller: 'RouteController'
            action: 'listRoutes'

        content:
            html:
                charset: 'UTF-8'
                title: 'Configuration/Routes'
                description: 'Skyfaring routes configuration panel'
                file: '@skf-html'
                css: '@skf-css'
                js:

    skf-playground:
        route:
            path: '/config/playground/int:id'
            methods:
                - 'GET'
            controller: 'Playground'
            action: 'hello'

        content:
            html:
                charset: 'UTF-8'
                title: 'Configuration/Playground'
                description: 'Skyfaring routes configuration panel'
                file: '@skf-html'
                css: '@skf-css'
                js:


# import: '@public'
