<?php

# ============================================================================ #
# DIRECTORIES
# ============================================================================ #

define('SIMPLE_ROOT',                    $_SERVER['DOCUMENT_ROOT'] . '/Simple');
define('SYSTEM_ROOT',                                 SIMPLE_ROOT . '/.system');
define('CACHE_PATH',                                false); //  SYSTEM_ROOT.'/twig_cache'); // false);
define('PUBLIC_ROOT',                    $_SERVER['DOCUMENT_ROOT'] . '/public');
define('DEV_ROOT',                          $_SERVER['DOCUMENT_ROOT'] . '/dev');

define('CONFIG_ROOT',                                  SIMPLE_ROOT . '/config');
define('CSS_ROOT',                                             '../../public/');
define('TEMPLATE_ROOT', serialize( array(
    SIMPLE_ROOT.'/MVC/View/assets',
    PUBLIC_ROOT.'/html'
    // DEV_ROOT.'/MVC/View'
)));


# ============================================================================ #
# NAMESPACE CONSTANTS INCLUSION
# ============================================================================ #

require SIMPLE_ROOT . '/Error/consts.php';
require SIMPLE_ROOT . '/HTTP/consts.php';

# ============================================================================ #
# LIBS INCLUSION
# ============================================================================ #

define('VENDOR_ROOT',                    $_SERVER['DOCUMENT_ROOT'] . '/vendor');

# ============================================================================ #
# REGULAR EXPRESSIONS
# ============================================================================ #

define('Skyfaring\Simple\REGEX_ANY',                                '([^/]+?)');
define('Skyfaring\Simple\REGEX_INT',                               '([0-9]+?)');
define('Skyfaring\Simple\REGEX_ALPHA',                        '([a-zA-Z_-]+?)');
define('Skyfaring\Simple\REGEX_ALPHANUMERIC',              '([0-9a-zA-Z_-]+?)');
define('Skyfaring\Simple\REGEX_STATIC',                                   '%s');
define('Skyfaring\Simple\REGEX_IP',           '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$');
define('Skyfaring\Simple\REGEX_HOSTNAME',     '^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))*$');
