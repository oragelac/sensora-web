<?php

// Vendor extension initialization
require_once ('./../Skyfaring/Simple/Autoloader.php');
use Skyfaring\Simple as S;

S\Autoloader::initialize('/etc/wt2/containers/wt2-sensora-web/app');
S\Autoloader::register();

use Skyfaring\Simple\Stream\Stream;

// PHPUnit initialization
require_once ('PHPUnit/Framework/TestCase.php');
use PHPUnit\Framework\TestCase;

class StreamTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test function read de la class Stream
     */
    public function testReading()
    {
        $file = new Stream('tests/read.txt', Stream::MODE_READ_WRITE);
        $content = $file->getContents();

        $this->assertEquals("love\n", $content);

        $file->close();
    }

    /**
     * Test function write & read de la class Stream
     */
    public function testWriting()
    {
        $file = new Stream('tests/write.txt', Stream::MODE_WRITE_READ);
        $file->write('love');
        $content = $file->getContents();

        $this->assertEquals('love', $content);

        $file->close();
    }
}
