<?php

require '../../vendor/autoload.php';

// Initialization
$driver = null;

class HomeInterfaceTest extends PHPUnit_Extensions_Selenium2TestCase
{
    /*
     *¨Initializes the components Selenium needs.
     */
    public function initialize()
    {
        $this->setHost('localhost');
        $this->setPort(4444);
        $this->setBrowserUrl('https://sens.oragelac.com/home');
        $this->setBrowser('chrome');
    }

    public function testSensoraHome()
    {
        $this->assertContaints('Sensora', $this->webDriver->getTitle());
    }

    /**
     * Stops the test.
     */
    public function stop()
    {
        $this->stop();
    }
}
