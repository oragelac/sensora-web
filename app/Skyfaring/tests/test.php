<?php
use PHPUnit\Framework\TestCase;
use Skyfaring\Simple\MVC\Model as SModel;

class Calcule extends TestCase
{
    // ...

    public function testCanBeNegated()
    {
        // Arrange
        $calcule = new Calcule();

        // Act
        $b = $calcule->add(1,2);

        // Assert
        $this->assertEquals(3, $b);
    }

    // ...
}
