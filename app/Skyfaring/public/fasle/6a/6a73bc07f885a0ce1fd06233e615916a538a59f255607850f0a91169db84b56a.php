<?php

/* header.twig */
class __TwigTemplate_8dde3530dcb40fdb9341e097d5aa32701b4ba6c6b2828662d2a156863463d79b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav class=\"sensora-header\">
    <div class=\"center\">
        <a class=\"redirect\" href=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["link_home"]) ? $context["link_home"] : null), "html", null, true);
        echo "\">Sensora</a>
        ";
        // line 4
        if ( !(null === (isset($context["link_measure"]) ? $context["link_measure"] : null))) {
            // line 5
            echo "            <a class=\"redirect\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["link_measure"]) ? $context["link_measure"] : null), "html", null, true);
            echo "\">Mes mesures</a> <!-- <a> qui apparait uniquement si l'uti est co - Pas le cas pr le moment-->
        ";
        }
        // line 7
        echo "        <a class=\"redirect\" href=\"";
        echo twig_escape_filter($this->env, (isset($context["link_shop"]) ? $context["link_shop"] : null), "html", null, true);
        echo "\">Magasin</a>
        <a class=\"logo\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["link_connection"]) ? $context["link_connection"] : null), "html", null, true);
        echo "\">
            <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
            \t viewBox=\"0 0 841.9 595.3\" enable-background=\"new 0 0 841.9 595.3\" xml:space=\"preserve\">
            <path fill=\"#A7A9AC\" d=\"M278.4,297.6c0,0,38.6-122,189-129.3c0,0-26.4-52.7-177.4,34.8C270.8,252.9,278.4,297.6,278.4,297.6z\"/>
            <path fill=\"#FFFFFF\" d=\"M574.3,282c0.2-76.6-66.8-138.7-149.7-138.7S274.6,205.4,274.4,282c-0.1,34.1,11.6,66.3,30.4,90.4
            \tc35.6,20,71.8,34.1,113.9,40.6C501.5,413,574.1,358.6,574.3,282z\"/>
            <g>
            \t<path fill=\"#414042\" d=\"M156.6,329.1c0,0,9-2.1,24.3-13.8c15.2-11.8,33.6-71.9,82.5-105.5c0,0-19.4,8.2-35.3,19.4
            \t\tc0,0,135.3-182,383.9-77.6c0,0-39.8-1.3-26.9,2.3c0,0,61,14.5,103,73.2c11.9,16.6-27.1,32.5-42.5,27.5c0,0-50.8,180.9-315.3,171.6
            \t\tc0,0,244.6-13.4,284.3-186.5c0,0-104.1-86.3-196-71.7c-104.8,16.1-197.2,85.6-216,155.5c-6,21.8-30.5,25.1-36,22.4
            \t\tC161.6,343.7,155.6,335,156.6,329.1z\"/>
            </g>
            <g>
            \t<path fill=\"#414042\" d=\"M552,365.5l-5.1-3.2c24.6-39.2,28.2-78.1,16.6-117.6c-9.5-32.4-42.5-66.4-42.7-66.6l14.9-2.4
            \t\tc0.8,0.8,23.7,18.1,34.3,48.4C579.8,252,587,314.2,552,365.5z\"/>
            </g>
            <g>
            \t<path fill=\"#414042\" d=\"M308.2,376.8c-13.1-13.2-61.3-75.9-21.4-167.4c0.8-1.7,3-2.3,4.7-1.4l0,0c1.6,0.9,19.5-7.4,18.3-6
            \t\tc-57.4,69.7-23.8,153.5-0.2,173.9C309.9,376.9,308.8,377.4,308.2,376.8z\"/>
            </g>
            <circle fill=\"#414042\" cx=\"328.7\" cy=\"263.3\" r=\"12.5\"/>
            <circle fill=\"#414042\" cx=\"274.1\" cy=\"350.5\" r=\"6.8\"/>
            <circle fill=\"#414042\" cx=\"347.3\" cy=\"392.4\" r=\"10.2\"/>
            <path fill=\"none\" stroke=\"#414042\" stroke-width=\"4\" stroke-miterlimit=\"10\" d=\"M353.3,180.3c0,0-66.7,117.7,10.2,243.7\"/>
            <g>
            \t<path fill=\"#414042\" d=\"M428.5,416.7C320,398.1,269.9,360.2,260,333.4c-3.4-9.2-1.6-20.4,3.1-24.6c-0.1,1.9,0,0,0,0
            \t\tc-1.7,3.1-2.9,15.3,0.1,23.4c9.6,26,62,63.4,169.1,81.8L428.5,416.7z\"/>
            </g>
            <circle class=\"iris-border\" fill=\"#2BB673\" cx=\"428.6\" cy=\"280.1\" r=\"52.5\"/>
            <path fill=\"none\" stroke=\"#414042\" stroke-width=\"4\" stroke-miterlimit=\"10\" d=\"M490.5,165c-84,22.5-280,167.6-198,212.1\"/>
            <text transform=\"matrix(1 0 0 1 344.4144 504)\" display=\"none\"><tspan x=\"0\" y=\"0\" display=\"inline\" fill=\"#414042\" font-family=\"'Roboto-Regular'\" font-size=\"30\">Senso</tspan><tspan x=\"82.8\" y=\"0\" display=\"inline\" fill=\"#414042\" font-family=\"'Roboto-Black'\" font-size=\"30\">Ward</tspan></text>
            <circle class=\"iris\" fill=\"#175C3A\" cx=\"428.6\" cy=\"280.1\" r=\"18.1\"/>
            <circle fill=\"#FFFFFC\" cx=\"468.3\" cy=\"242.3\" r=\"44.7\"/>
            </svg>
        </a>
        <a class=\"redirect\" href=\" ";
        // line 43
        echo twig_escape_filter($this->env, (isset($context["link_contact"]) ? $context["link_contact"] : null), "html", null, true);
        echo "\">Contact</a>
        ";
        // line 44
        if ( !(null === (isset($context["link_measure"]) ? $context["link_measure"] : null))) {
            // line 45
            echo "            <a class=\"redirect\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["link_profil"]) ? $context["link_profil"] : null), "html", null, true);
            echo "\">Profil</a> <!-- <a> qui apparait uniquement si l'uti est co - Pas le cas pr le moment-->
            <a class=\"redirect\" href=\"";
            // line 46
            echo twig_escape_filter($this->env, (isset($context["link_administration"]) ? $context["link_administration"] : null), "html", null, true);
            echo "\">Administration</a> <!-- <a> qui apparait uniquement si l'uti est co - Pas le cas pr le moment-->
        ";
        }
        // line 48
        echo "        <a class=\"redirect\" href=\"";
        echo twig_escape_filter($this->env, (isset($context["link_inscription"]) ? $context["link_inscription"] : null), "html", null, true);
        echo "\">Inscription</a>
        <a class=\"redirect\" href=\"";
        // line 49
        echo twig_escape_filter($this->env, (isset($context["link_connection"]) ? $context["link_connection"] : null), "html", null, true);
        echo "\">Connexion</a>
    </div>
</nav>
";
    }

    public function getTemplateName()
    {
        return "header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 49,  94 => 48,  89 => 46,  84 => 45,  82 => 44,  78 => 43,  40 => 8,  35 => 7,  29 => 5,  27 => 4,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "header.twig", "/app/Skyfaring/public/html/header.twig");
    }
}
