// js/script.js
'use strict';

var sensora = angular.module('sensora', ['dragularModule','rzModule','datetime']).config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

sensora.controller('connectionCtrl', ['$scope', '$http', '$location', 'authentificationService','communicationService',
    function ($scope, $http, $location,authentificationService,communicationService)
    {
        $scope.connexionUser = function()
        {
            var user = JSON.stringify({user : $scope.user});
            var request = {
                method: 'POST',
                url: "/authentication",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: user
            };

            $http(request).success( function (data)
            {
                authentificationService.set(
                    data._id,
                    data._pseudonym,
                    null,
                    null
                );

                communicationService.set("success", ["Connexion réussie."], true);

                window.location.replace("./sensoward");
            }).error(function (data)
            {
                communicationService.set("erreur", ["Identifiant incorrect."], true);
                console.log('Authentication failed.');
            });
        };
    }
]);

sensora.controller('inscriptionCtrl', ['$scope', '$http','communicationService',
    function ($scope, $http,communicationService) {
        $scope.longMdpMin="8";
        $scope.longMdpMax="30";

        $scope.addUser = function(){
            var newUser = $scope.newUser;
            console.log(newUser);
            //Requète ajax

                var data = JSON.stringify({NewUser : newUser});
                    var req = {
                        method: 'POST',
                        url: "/addUser",
                        headers: {
                            'Content-Type': undefined
                        },
                        data: data
                    };
                    $http(req).success(function (data) {
                        if (data.statut) {
                            communicationService.set("success", ["Inscription réussie."], true);
                        } else {
                            communicationService.set("erreur", data.msgErreur, true);
                        }
                    }).error(function (data) {
                        console.log(data);
                    });
            };
    }
]);
sensora.controller('myRaspberryCtrl', ['$scope', '$http','$filter','datetime','communicationService',
    function ($scope, $http,$filter,$datetime,communicationService)
    {
        //INIT
        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.nbPage = $scope.currentPage + 1;
        $scope.content = {type:'',form:''};
        $scope.currentData = null;
        $scope.slider = {
              min: 0,
              max: 0,
              options: {
                stepsArray: []
              }
            };

            var ctx = document.getElementById("myChart-son");
            var data = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                label: "My First dataset",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: [65, 59, 80, 81, 56, 55, 40],
                spanGaps: false,
            },
            {
                label: "My First dataset",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(47,173,102,0.4)",
                borderColor: "rgba(47,173,102,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(47,173,102,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(47,173,102,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: [70, 70, 70, 70, 70, 70, 70],
                spanGaps: false,
            }
        ]
    };
        var  myChart = new Chart(ctx, {
                type: 'line',
                data: data
            });

        //Get raspberry from currentUser
        var data = JSON.stringify({});
        var req = {
            method: 'POST',
            url: "/getAllFrom",
            headers: {
                'Content-Type': undefined
            },
            data: data
        };
        $http(req).success(function (data)
        {
            $scope.raspberries = data.resultat;
            if ($scope.raspberries)
            {
                $scope.currentRaspberry = $scope.raspberries[0];
                $scope.showData('son');
            }
        }).error(function (data)
        {
            communicationService.set("erreur", ["Aucune sensoward trouvée"], true);
            console.log(data);
        });





        //FCT
        /*$scope.setCurrentRaspberry = function($index)
        {
            $scope.raspberryShow = $scope.raspberries[$index]._matricule
            $scope.currentRaspberry = $scope.raspberries[$index];

            //$scope.filtreTemps = '';
            $scope.showData($scope.content.type);
        }*/

        $scope.setCurrentRaspberry = function()
        {
            var indexRasp = $scope.nbCurrentRaspberry;
            if (indexRasp != 'choix')
            {
                $scope.raspberryShow = $scope.raspberries[parseInt(indexRasp)]._matricule
                $scope.currentRaspberry = $scope.raspberries[parseInt(indexRasp)];

                $scope.showData($scope.content.type);
            }
        }

        $scope.showData = function(typeData)
        {
            $scope.content.type = typeData;
            $scope.filtreTemps = 'allDatafilter';
            switch(typeData){
                case 'son' :
                    var tabTime=[];
                    var tabDecibel=[];
                    $scope.limite = 80;
                    $scope.currentData =  $scope.currentRaspberry._dataSound;
                    for (var i = 0; i < $scope.currentData.length; i++) {
                        $scope.currentData[i]._time = new Date ($scope.currentData[i]._time);
                    }

                    for (var i = 0; i < $scope.currentData.length; i++) {
                        tabTime.push($scope.currentData[i]._time);
                        tabDecibel.push($scope.currentData[i]._decibel);
                    }

                    $scope.majTableau($scope.currentData);
                    $scope.majSlider(tabTime);
                    $scope.majChart(tabTime,tabDecibel);
                    break;
                case 'carSpeed' :
                    var tabTime=[];
                    var tabKmH=[]; //tab Kilomètre/Heure
                    $scope.limite = 50;
                    $scope.currentData =  $scope.currentRaspberry._dataCarSpeed;
                    for (var i = 0; i < $scope.currentData.length; i++) {
                        $scope.currentData[i]._time = new Date ($scope.currentData[i]._time);
                    }

                    for (var i = 0; i < $scope.currentData.length; i++) {
                        tabTime.push($scope.currentData[i]._time);
                        tabKmH.push($scope.currentData[i]._speed);
                    }

                    $scope.majTableau($scope.currentData);
                    $scope.majSlider(tabTime);
                    $scope.majChart(tabTime,tabKmH);
                    break;
            }
            $scope.content.form = 'tab';
        }

        $scope.changeFiltre = function (){
            var newFiltre = $scope.filtreTemps ;
            var tabTime=[];
            var tabData=[];
            var dataFilter = [];

            switch (newFiltre){
                case 'allDatafilter':
                    $scope.showData($scope.content.type);
                    break;
                default :
                    switch ($scope.content.type){
                        case "son" :
                            dataFilter = $filter(newFiltre)($scope.currentRaspberry._dataSound,$scope.content.type);
                            break;
                        case "carSpeed" :
                            dataFilter = $filter(newFiltre)($scope.currentRaspberry._dataCarSpeed,$scope.content.type);
                            break;
                    }
            }

            for (var i = 0; i < dataFilter.length; i++) {
                tabTime.push(dataFilter[i]._time);

                switch ($scope.content.type){
                    case "son" :
                        tabData.push(dataFilter[i]._decibel);
                        break;
                    case "carSpeed" :
                        tabData.push(dataFilter[i]._speed);
                        break;
                }
            }

            setTimeout(function(){
                $scope.currentData = dataFilter;
            });

            $scope.majTableau(dataFilter);
            $scope.majSlider(tabTime);
            $scope.majChart(tabTime,tabData);
        }


       $scope.changeRange = function(){
           var tabTime=[];
           var tabData=[];
           var dataFilter = $filter('rangeFilter')($scope.currentData,$scope.slider.min,$scope.slider.max);
           var tabTime = [];
           for (var i = 0; i < dataFilter.length; i++) {
               tabTime.push(dataFilter[i]._time);
           }

           setTimeout(function(){
               $scope.currentDange = dataFilter;
           });

           for (var i = 0; i < dataFilter.length; i++) {
               tabTime.push(dataFilter[i]._time);
               if($scope.content.type =='son'){
                   tabData.push(dataFilter[i]._decibel);
               }else
               {
                   tabData.push(dataFilter[i]._speed);
               }

           }

           $scope.majTableau(dataFilter)
           $scope.majChart(tabTime,tabData);
       }

       $scope.majTableau = function(tabData){
           setTimeout(function(){
               $scope.currentRange = tabData;
               $scope.currentPage = 0;
           });
       }

       $scope.majSlider = function(tabRange){
           var min = tabRange[0];
           var max = tabRange[tabRange.length-1];

           $scope.slider = {
                 min: min,
                 max: max,
                 options: {
                   stepsArray: tabRange,
                   translate: function(date) {
                     if (date != null){
                         return $filter('formatDate')(date);
                         }
                     }
                   }

               };
       }

       $scope.majChart = function(tabLabels,tabData){
           var tabLimite = [];
            var type='line';
            if(tabData.length==1){
                type='bar';
            }
            if(myChart){
                myChart.destroy();
            }

            if ($scope.limite)
            {
                var nbLimite = (tabData.length>0)?tabData.length:2;
                for (var i = 0; i < nbLimite; i++) {
                    tabLimite.push($scope.limite);
                }
            }

            var data = {
                labels: tabLabels,
                datasets: [
                    {
                        type: 'line',
                        label: "Limite",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(230,9,18,0.4)",
                        borderColor: "rgba(230,9,18,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(230,9,18,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(230,9,18,1)",
                        pointHoverBorderColor: "rgba(255,165,0,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: tabLimite,
                        spanGaps: false,
                    },
                    {
                        label: ($scope.content.type =='son')?'Décibel':'Kilomètre/Heure',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(47,173,102,0.4)",
                        borderColor: "rgba(47,173,102,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(47,173,102,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(47,173,102,1)",
                        pointHoverBorderColor: "rgba(255,165,0,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: tabData,
                        spanGaps: false,
                    }
                ]
                };
            var ctx = document.getElementById("myChart-"+$scope.content.type);
            myChart = new Chart(ctx, {
                    type: type,
                    data: data
                });



           }
           $scope.numberOfPages=function(){
                   return Math.ceil($scope.currentRange.length/$scope.pageSize);
               }

       }

]);

sensora.controller('contactCtrl', ['$scope', '$http','$location','communicationService',
    function ($scope, $http,$location,communicationService)
    {
        $scope.contact ={type:"autre"};

        $scope.sendMessage =function ()
        {
            var data = JSON.stringify({Contact : $scope.contact});
            var req = {
                method: 'POST',
                url: "/sendMessage",
                headers: {
                    'Content-Type': undefined
                },
                data: data
            };

            $http(req).success(function (data) {
                communicationService.set("success", ["Message envoyé avec succès."], true);
            }).error(function (data) {
                communicationService.set("success", ["Un problème est survenu lors de l'envoi, veuillez rééssayer."], true);
                console.log(data);
            });
        }
    }
]);

sensora.controller('shopCtrl', ['$scope', '$http','$location','dragularService','authentificationService',
    function ($scope, $http,$location,dragularService,authentificationService)
    {
        //INIT
        $scope.userActuel = authentificationService.get();
        $scope.shop = {etape : 1};
        $scope.detailModule=null;

        $scope.panier = [];
        var initialeSensoward = {
            nom : "Sensoward-"+($scope.panier.length+1),
            type : "sensoward",
            prix : "25",
            modules : [],
        };

        $scope.panier.push(initialeSensoward);
        $scope.currentSensoward = $scope.panier[0];//Afficher directement le premier sensoward ( vide en théorie )
        $scope.listeModules = [
            {
                type : "module-son",
                prix : "5",
                className : "flaticon-radio-microphone",
                nom : "sonomètre",
                description : "Il vous permettra de surveiller le niveau sonore audible dans votre maison, ou depuis votre terrain, via un graphe temporel.",
                isSet: false
            },
            {
                type : "module-densite",
                nom :"Analyseur de traffic",
                description : "Grâce à ce module, vous pourrez analyser finement la densité de trafic de votre rue et visualiser le résultat sous forme de graphe.",
                prix : "5",
                className : "flaticon-people",
                isSet: false
            },
            {
                type : "module-radar",
                nom :"Radar",
                description : "Ce module vous permettra de vérifier que les automobilistes respectent bien les limitations de vitesse de votre quartier",
                prix : "5",
                className : "flaticon-car-speedometer",
                isSet: false
            },
            {
                type : "module-alimentation_solaire",
                prix : "15",
                nom : "alimentation solaire",
                description : "Nous proposons une alternative à l'alimentation classique de votre SensoWard, celui-ci consiste en un module solaire dédié.",
                className : "flaticon-solar-energy",
                isSet: false
            }
        ];

        $scope.fixModules = [];
        var fixModules = [];

        //DRAGULAR
        dragularService([$("#modules")[0], $("#contentSensoward")[0]], {
            scope: $scope,
            containersModel: [$scope.listeModules, fixModules],
            copy: function (el, source)
            {
                return source === $("#modules")[0]
            },
            accepts: function (el, target)
            {
                return target === $("#contentSensoward")[0];
            }
        });

        $scope.$on('dragulardrop', function(e, el,target)
        {
            e.stopPropagation();
            if(target === $("#contentSensoward")[0])
            {
              var id = el.id;
              var index = id.replace("module-", "");
              $scope.currentSensoward.modules.push($scope.listeModules[index]);
            }
        });

        //Ajoute une sensoward vide au panier
        $scope.addSensoward = function()
        {
            $scope.panier.push({
                nom : "Sensoward-"+($scope.panier.length+1),
                type : "sensoward",
                prix : "25",
                modules : [],
            });

            var lastIndex = $scope.panier.length-1;
            //$scope.currentSensoward = $scope.panier[lastIndex];
            changeSensoward(lastIndex);
        };

        $scope.copySensoward = function ()
        {
            var type = $scope.currentSensoward.type;
            var prix = $scope.currentSensoward.prix;
            var modules = [];

            for (var i = 0; i < $scope.currentSensoward.modules.length; i++)
            {
                modules.push($scope.currentSensoward.modules[i]);
            }

            $scope.panier.push({
                nom : "Sensoward-"+($scope.panier.length+1),
                type : "sensoward",
                prix : "25",
                modules : modules,
            });

            var lastIndex = $scope.panier.length-1;
            changeSensoward(lastIndex);
            console.log($scope.panier);
        }

        //Supprime une sensoward du panier
        $scope.removeSensoward = function()
        {
            for (var i = 0; i < $scope.panier.length; i++)
            {
                if($scope.panier[i].nom == $scope.currentSensoward.nom)
                {
                    $scope.panier.splice(i,1);
                    if($scope.panier[i])
                    {
                        $scope.currentSensoward = $scope.panier[i];
                    }

                    else
                    {
                        var lastIndex = $scope.panier.length-1;
                        $scope.currentSensoward = $scope.panier[lastIndex];
                    }

                    break;
                }
            }
        };

        //Supprime un module d'une sensoward du panier
        $scope.removeModule = function($indexModule)
        {
            for (var i = 0; i < $scope.panier.length; i++)
            {
                if($scope.panier[i].nom == $scope.currentSensoward.nom)
                {
                    $scope.panier[i].modules.splice($indexModule,0);
                    break;
                }
            }

            $scope.fixModules.splice($indexModule,1)
        };

        $scope.setCurrentSensoward = function($index)
        {
            changeSensoward($index);
        };

        var changeSensoward = function($index)
        {
            //save currentSensoward
            if($scope.currentSensoward)
            {
                for (var i = 0; i < $scope.panier.length; i++)
                {
                    if($scope.panier[i].nom == $scope.currentSensoward.nom)
                    {
                        var tamponModule = $scope.fixModules;
                        $scope.currentSensoward.modules = tamponModule;
                        $scope.fixModules = [];
                        $scope.panier[i] =  $scope.currentSensoward;

                        break;
                    }
                }
            }

            //change
            $scope.currentSensoward = $scope.panier[$index];
            $scope.fixModules = $scope.currentSensoward.modules;
        }

        changeSensoward(0);

        $scope.redirect = function(path)
        {
            window.location.replace(path);
        }

        $scope.setDetail = function($index)
        {
            $scope.detailModule = $scope.listeModules[$index];
        }

        $scope.buy = function()
        {
            var data = JSON.stringify({Panier : $scope.panier, IdUser : $scope.userActuel._id});
            console.log(data);
            var req = {
                method: 'POST',
                url: "/buy",
                headers: {
                    'Content-Type': undefined
                },
                data: data
            };

            $http(req).success(function (data)
            {
                //TODO afficher que le msg a été envoyé
            }).error(function (data)
            {
                console.log(data);
            });
        }

        /*dragula([$("#modules")[0], $("#contentSensoward")[0]], {
          copy: function (el, source) {
            return source === $("#modules")[0]
          },
          accepts: function (el, target) {
            return target === $("#contentSensoward")[0];
        },
            moves:function (el,target){
                var id = el.id;
                var index = id.replace("module-", "");
                if(!$scope.listeModules[index].isSet){
                    return true;
                }else{
                    return false;
                }
            }
        })
        .on('drop', function (el,target) {
            if(target === $("#contentSensoward")[0]){
                var id = el.id;
                var index = id.replace("module-", "");
                $scope.$apply(function () {
                    //$scope.listeModules[index].isSet = true;
                    $scope.currentSensoward.modules.push($scope.listeModules[index]);
                });
            }

            //var nbModule = $(".colonne-sensoward #"+el.id).length;
            /*if ((target === $("#contentSensoward")[0])){//si pas déjà un meme module && la destination est #contentSensoward alors return true
                var index = el.id.substr(el.id.length-1, 1);
                $scope.$apply(function () {
                    $scope.currentSensoward.push($scope.listeModules[index]);
                });
            }

            alert("dropped");
        });*/

        /*dragula([$("#contentSensoward")[0], $("#modules")[0]],{
            copy: true,
            accepts: function (el, target) {
                var nbModule = $(".colonne-sensoward #"+el.id).length;
                if ((nbModule<1)&&(target === $("#contentSensoward")[0])){//si pas déjà un meme module && la destination est #contentSensoward alors return true
                    return true
                }else {
                    return false
                }
            }
        })
        .on('drop', function (el,target) {
            //var nbModule = $(".colonne-sensoward #"+el.id).length;
            if ((target === $("#contentSensoward")[0])){//si pas déjà un meme module && la destination est #contentSensoward alors return true
                var index = el.id.substr(el.id.length-1, 1);
                $scope.$apply(function () {
                    $scope.currentSensoward.push($scope.listeModules[index]);
                });
            }
        })
        .on('drag', function (el) {
            el.className += ' ex-moved';
        });*/
    }
]);

sensora.controller('administrationCtrl', ['$scope', '$http','$location',
    function ($scope, $http,$location) {
        $scope.content = {type:''};

        $scope.changeContent = function (newContent){
            $scope.content.type = newContent;
            switch (newContent){
                case "messagerie":
                    var data = JSON.stringify({});
                    var req = {
                        method: 'POST',
                        url: "/getAllMessage",
                        headers: {
                            'Content-Type': undefined
                        },
                        data: data
                    };
                    $http(req).success(function (data) {
                        if (data.statut) {
                            $scope.messagerie = data.resultat;
                        } else {
                            //TODO afficher que les msg d'erreur du tab : data.msgErreur
                        }
                    }).error(function (data) {
                        console.log(data);
                    });
                    break;
                case "raspberry":
                    var data = JSON.stringify({});
                    var req = {
                        method: 'GET',
                        url: "/getAllRaspberry",
                        headers: {
                            'Content-Type': undefined
                        },
                        data: data
                    };
                    $http(req).success(function (data) {
                        if (data.statut) {
                            $scope.raspberries = data.resultat;
                        } else {
                            //TODO afficher que les msg d'erreur du tab : data.msgErreur
                        }
                    }).error(function (data) {
                        console.log(data);
                    });
                    break;
                case "utilisateur":
                    var data = JSON.stringify({});
                    var req = {
                        method: 'GET',
                        url: "/getAllUser",
                        headers: {
                            'Content-Type': undefined
                        },
                        data: data
                    };
                    $http(req).success(function (data) {
                            $scope.users = data;
                    }).error(function (data) {
                        console.log(data);
                    });
                    break;
            }
        }
        $scope.seeMessage = function (index){
            $scope.content.type = 'seeMessage';
            $scope.currentMessage = $scope.messagerie[index];
        }
        $scope.sendAnswer = function(){
            var email = {destinataire : $scope.currentMessage._email_source, contenu : $scope.answer};
            var data = JSON.stringify({Action : "answer", Email : email});
                var req = {
                    method: 'POST',
                    url: "/sendEmail",
                    headers: {
                        'Content-Type': undefined
                    },
                    data: data
                };
                $http(req).success(function (data) {
                    $scope.errorSendEmail=data;
                    console.log(data);
                    if (data.statut) {
                        //TODO afficher que le msg a été envoyé

                    } else {
                        //TODO afficher que les msg d'erreur du tab : data.msgErreur
                    }
                }).error(function (data) {
                    console.log(data);
                });
        }
        $scope.redirect = function(path){
            window.location.replace(path);
        }


    }
]);
sensora.controller('profilCtrl', ['$scope', '$http','$location','authentificationService',
    function ($scope, $http,$location,authentificationService) {
        $scope.userActuel = authentificationService.get();
        $scope.longMdpMin="8";
        $scope.longMdpMax="30";

        $('#saveProfil').jrumble({
            speed : 100
        });

        $scope.inputChange = false;
        $scope.showSave = function (){
            $scope.inputChange = true;

            //Gestion jrumble
            var demoStart = function(){
                $('#saveProfil').trigger('startRumble');
                setTimeout(demoStop, 500);
            };

            var demoStop = function(){
                $('#saveProfil').trigger('stopRumble');
                setTimeout(demoStart, 500);
            };

            demoStart();
        }

        var data = JSON.stringify({IdUser : $scope.userActuel._id});
            var req = {
                method: 'POST',
                url: "/getUser",
                headers: {
                    'Content-Type': undefined
                },
                data: data
            };
            $http(req).success(function (data) {
                if (data.statut) {
                    $scope.user = data.resultat;

                } else {
                    //TODO afficher que les msg d'erreur du tab : data.msgErreur
                }
            }).error(function (data) {
                console.log(data);
            });

        $scope.saveProfil = function (){
            console.log("save profil");
            console.log($scope.user);
            var data = JSON.stringify({User : $scope.user});
                var req = {
                    method: 'POST',
                    url: "/saveUser",
                    headers: {
                        'Content-Type': undefined
                    },
                    data: data
                };
                $http(req).success(function (data) {
                    if (data.statut) {
                        authentificationService.set(data.resultat._id, data.resultat._pseudonym, null, null);
                        //TODO afficher que les msg est OK
                    } else {
                        //TODO afficher que les msg d'erreur du tab : data.msgErreur
                    }
                }).error(function (data) {
                    console.log(data);
                });
        }

        $scope.testAdresse = function(){

        }
    }
]);

sensora.controller('hearderCtrl', ['$scope', '$http','$location','authentificationService',
    function ($scope, $http,$location,authentificationService) {

        $scope.deconnection = function(){
            authentificationService.delete();
            window.location.replace("./disconnection");
        }


    }
]);



sensora.controller('nomCtrl', ['$scope', '$http','$location',
    function ($scope, $http,$location) {
        $scope.content = {type:"donneeConnection"};
    }
]);

sensora.controller('CommunicationCtrl', function ($scope, communicationService) {

        $scope.communicationService = communicationService;

});


//---------------------------------------------------------------------DIRECTIVE
sensora.directive("validemdpDirective", function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, mCtrl) {
            function validMdp(value) {
                var longMdpMax = scope.longMdpMax;
                var longMdpMin = scope.longMdpMin;
                var longActuel = value.length;
                if ((longActuel >= longMdpMin) && (longActuel <= longMdpMax)) {
                    mCtrl.$setValidity('charE', true);
                } else {
                    mCtrl.$setValidity('charE', false);
                }
                if (scope.page == "profil") {
                    if (longActuel == 0) {
                        mCtrl.$setValidity('charE', true);
                    }
                }
                return value;
            }
            mCtrl.$parsers.push(validMdp);
        }
    };
});
sensora.directive("inputDirective", function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, mCtrl) {
            function input(value) {
                var regex = /[^A-Za-z0-9-\@\-\.\:\/\é\è\ç\à\ù\ê\ë\ï\ \"\'\_]/g;
                var resultatSpecialChar = value.match(regex);

                if (resultatSpecialChar == null) {
                    mCtrl.$setValidity('haveSpecialChar', true);
                } else {
                    mCtrl.$setValidity('haveSpecialChar', false);
                }

                var regex = /select|update|insert|delete|drop|table/ig;
                var resultatSpecialWord = value.match(regex);

                if (resultatSpecialWord == null) {
                    mCtrl.$setValidity('haveSpecialWord', true);
                } else {
                    mCtrl.$setValidity('haveSpecialWord', false);
                }

                if (value == '') {
                    mCtrl.$setValidity('isEmpty', false);
                } else {
                    mCtrl.$setValidity('isEmpty', true);
                }

                return value;
            }
            mCtrl.$parsers.push(input);
        }
    };
});


sensora.directive("communicationUser", function() {
    return {
        template : "<section id='communicationUser' ng-class='communicationService.type' ng-controller='CommunicationCtrl' ng-show='communicationService.isShow'><div id='alert' class='alert alert-dismissible' role='alert' ><button type='button' class='close' ng-click='communicationService.isShow=!communicationService.isShow'><span class='flaticon-close-cross'></span></button><ul><li>{[{communicationService.contenu[0]}]}</li></ul></div></section>"
    };
});

sensora.directive('ownInput', function() {
   return {
       restrict: 'E',
        scope:{id:'@inputId', model:'=inputModel',form:'=inputForm',type:'@inputType'},
        templateUrl: 'public/html/own-input.twig',
    };
});
//-----------------------------------------------------------------------SERVICE
sensora.service('communicationService', function () {
    var communicationService = {
        contenu: ["enjoy your life"],
        type: "erreur",
        isShow: false,
        set: function (type, contenu, isShow) {
            this.contenu = contenu;
            this.type = type;
            this.isShow = isShow;

            $("#communicationUser").fadeTo(5000, 500).slideUp(500, function () {
                $("#communicationUser").slideUp(500);
            });
        }
    };

    return communicationService;
});

sensora.service('authentificationService', function () {
    var authentificationService = {
        _id: "",
        _pseudo: "",
        _rang: "",
        _idRang: "",
        _isCo: false,
        set: function (id, pseudo, rang, idRang) {
            this._id = id;
            this._pseudo = pseudo;
            this._rang = rang;
            this._idRang = idRang;
            this._isCo = true;
            var user = {id: this._id, pseudo: this._pseudo, rang: this._rang, idRang: this._idRang};
            console.log(user);
            Cookies.set('user', user, {expires: 1 / 12});
        },
        get: function () {
            var user = Cookies.getJSON('user');
            if (user != null) {
                this._id = user.id;
                this._pseudo = user.pseudo;
                this._rang = user.rang;
                this._idRang = user.idRang;
                this._isCo = true;
            } else {
                this._id = "";
                this._pseudo = "";
                this._rang = "";
                this._idRang = "";
                this._isCo = false;
            }
            return this;
        },
        delete: function () {
            this.id = "";
            this.pseudo = "";
            this.rang = "";
            this.idRang = "";
            this.isCo = false;
            Cookies.remove('user');
        }
    };
    return authentificationService;
});
//----------------------------------------------------------------------- FILTRE
sensora.filter('rangeFilter', function () {
    return function (items, min, max) {
        var range = [],
            min=min,
            max=max;

        for (var i=0, l=items.length; i<l; ++i){
            var item = items[i];
            if(item._time<=max && item._time>=min){
                range.push(item);
            }
        }
        return range;
    };
});

sensora.filter('minuteFilter', function () {
    return function (array,contentType) {
        var tabMinutes = [];
        var previousIndex = 0;
        var resultat = [];

        //Crée un tableau où chaque élément est un tableau de chaque minute du param "array"
        for (var i=1 ; i< array.length;i++){
            var minute_previousElem = array[i-1]._time.getMinutes();
            var minute_currentElem =array[i]._time.getMinutes();
            if(minute_previousElem != minute_currentElem)
            {
                var slice = array.slice(previousIndex,i);

                tabMinutes.push(slice);
                previousIndex =i;
            }
        }

        //Rajoute la dernière minute de l'array qui n'a pas été trouvé par la logique du for au dessus
        if(previousIndex !=0){
            var slice = array.slice(previousIndex,array.length);
            tabMinutes.push(slice)
        }else{
            tabMinutes.push(array);
        }

        //Crée un tableau d'objet {time : "temps avec minute précise", decibel : moyenne de la minute}
        for (var i = 0; i < tabMinutes.length; i++) {
            var currentMinute = tabMinutes[i];
            var sommeDbCurrentMinute = 0;
            var nbElemenCurrentMinute = currentMinute.length;
            for (var j = 0; j < currentMinute.length; j++) {
                switch (contentType){
                    case 'son' :
                        sommeDbCurrentMinute += parseInt(currentMinute[j]._decibel);
                        break;
                    case 'carSpeed' :
                        sommeDbCurrentMinute += parseInt(currentMinute[j]._speed);
                        break;
                }
            }
            var moyenneDb = sommeDbCurrentMinute / nbElemenCurrentMinute;
            switch (contentType){
                case 'son' :
                    resultat.push(
                    {
                        _time: new Date(
                                currentMinute[0]._time.getFullYear(),
                                currentMinute[0]._time.getMonth(), //getMonth fourni mois (0-11)
                                currentMinute[0]._time.getDate(),
                                currentMinute[0]._time.getHours(),
                                currentMinute[0]._time.getMinutes(),
                                0,//seconde
                                0),//miliseconde
                        _decibel: moyenneDb
                    });
                    break;
                case 'carSpeed' :
                    resultat.push(
                    {
                        _time: new Date(
                                currentMinute[0]._time.getFullYear(),
                                currentMinute[0]._time.getMonth(), //getMonth fourni mois (0-11)
                                currentMinute[0]._time.getDate(),
                                currentMinute[0]._time.getHours(),
                                currentMinute[0]._time.getMinutes(),
                                0,//seconde
                                0),//miliseconde
                        _speed: moyenneDb
                    });
                    break;
            }

        }
        return resultat;
    };
});

sensora.filter('hourFilter', function () {
    return function (array,contentType) {
        var tabHeures = [];
        var previousIndex = 0;
        var resultat = [];

        //Crée un tableau où chaque élément est un tableau de chaque heure du param "array"
        for (var i=1 ; i< array.length;i++){
            var heure_previousElem = array[i-1]._time.getHours();
            var heure_currentElem =array[i]._time.getHours();
            if(heure_previousElem != heure_currentElem)
            {
                var slice = array.slice(previousIndex,i);

                tabHeures.push(slice);
                previousIndex =i;
            }
        }

        //Rajoute la dernière heure de l'array qui n'a pas été trouvé par la logique du for au dessus
        if(previousIndex !=0){
            var slice = array.slice(previousIndex,array.length);
            tabHeures.push(slice)
        }else{
            tabHeures.push(array);
        }

        //Crée un tableau d'objet {time : "temps avec heure précise", decibel : moyenne de la heure}
        for (var i = 0; i < tabHeures.length; i++) {
            var currentHeure = tabHeures[i];
            var sommeDbCurrentHeure = 0;
            var nbElemenCurrentHeure = currentHeure.length;
            for (var j = 0; j < currentHeure.length; j++) {
                switch (contentType){
                    case 'son' :
                        sommeDbCurrentHeure += parseInt(currentHeure[j]._decibel);
                        break;
                    case 'carSpeed' :
                        sommeDbCurrentHeure += parseInt(currentHeure[j]._speed);
                        break;
                }
            }
            var moyenneDb = sommeDbCurrentHeure / nbElemenCurrentHeure;
            switch (contentType){
                case 'son' :
                    resultat.push(
                    {
                        _time: new Date(
                                currentHeure[0]._time.getFullYear(),
                                currentHeure[0]._time.getMonth(), //getMonth fourni mois (0-11)
                                currentHeure[0]._time.getDate(),
                                currentHeure[0]._time.getHours(),
                                0,//minutes
                                0,//secondes
                                0),//milisecondes
                        _decibel: moyenneDb
                    });
                    break;
                case 'carSpeed' :
                    resultat.push(
                        {
                            _time: new Date(
                                    currentHeure[0]._time.getFullYear(),
                                    currentHeure[0]._time.getMonth(), //getMonth fourni mois (0-11)
                                    currentHeure[0]._time.getDate(),
                                    currentHeure[0]._time.getHours(),
                                    0,//minutes
                                    0,//secondes
                                    0),//milisecondes
                            _speed: moyenneDb
                        });
                    break;
            }
        }
        return resultat;
    };
});

sensora.filter('dayFilter', function () {
    return function (array,contentType) {
        var tabJours = [];
        var previousIndex = 0;
        var resultat = [];

        //Crée un tableau où chaque élément est un tableau de chaque jour du param "array"
        for (var i=1 ; i< array.length;i++){
            var jour_previousElem = array[i-1]._time.getDate();
            var jour_currentElem =array[i]._time.getDate();
            if(jour_previousElem != jour_currentElem)
            {
                var slice = array.slice(previousIndex,i);

                tabJours.push(slice);
                previousIndex =i;
            }
        }

        //Rajoute la dernière jour de l'array qui n'a pas été trouvé par la logique du for au dessus
        if(previousIndex !=0){
            var slice = array.slice(previousIndex,array.length);
            tabJours.push(slice)
        }else{
            tabJours.push(array);
        }

        //Crée un tableau d'objet {time : "temps avec jour précise", decibel : moyenne de la jour}
        for (var i = 0; i < tabJours.length; i++) {
            var currentJour = tabJours[i];
            var sommeDbCurrentJour = 0;
            var nbElemenCurrentJour = currentJour.length;
            for (var j = 0; j < currentJour.length; j++) {
                switch (contentType){
                    case 'son' :
                        sommeDbCurrentJour += parseInt(currentJour[j]._decibel);
                        break;
                    case 'carSpeed' :
                        sommeDbCurrentJour += parseInt(currentJour[j]._speed);
                        break;
                }
            }
            var moyenneDb = sommeDbCurrentJour / nbElemenCurrentJour;
            switch (contentType){
                case 'son' :
                    resultat.push(
                        {
                            _time: new Date(
                                    currentJour[0]._time.getFullYear(),
                                    currentJour[0]._time.getMonth(), //getMonth fourni mois (0-11)
                                    currentJour[0]._time.getDate(),
                                    0,//heures
                                    0,//minutes
                                    0,//secondes
                                    0),//milisecondes
                            _decibel: moyenneDb
                        });
                    break;
                case 'carSpeed' :
                    resultat.push(
                        {
                            _time: new Date(
                                    currentJour[0]._time.getFullYear(),
                                    currentJour[0]._time.getMonth(), //getMonth fourni mois (0-11)
                                    currentJour[0]._time.getDate(),
                                    0,//heures
                                    0,//minutes
                                    0,//secondes
                                    0),//milisecondes
                            _speed: moyenneDb
                        });
                    break;
            }
        }
        return resultat;
    };
});

sensora.filter('monthFilter', function () {
    return function (array,contentType) {
        var tabMois = [];
        var previousIndex = 0;
        var resultat = [];

        //Crée un tableau où chaque élément est un tableau de chaque mois du param "array"
        for (var i=1 ; i< array.length;i++){
            var mois_previousElem = array[i-1]._time.getMonth();
            var mois_currentElem =array[i]._time.getMonth();
            if(mois_previousElem != mois_currentElem)
            {
                var slice = array.slice(previousIndex,i);

                tabMois.push(slice);
                previousIndex =i;
            }
        }

        //Rajoute la dernière mois de l'array qui n'a pas été trouvé par la logique du for au dessus
        if(previousIndex !=0){
            var slice = array.slice(previousIndex,array.length);
            tabMois.push(slice)
        }else{
            tabMois.push(array);
        }

        //Crée un tableau d'objet {time : "temps avec mois précise", decibel : moyenne de la mois}
        for (var i = 0; i < tabMois.length; i++) {
            var currentMonth = tabMois[i];
            var sommeDbCurrentMonth = 0;
            var nbElemenCurrentMonth = currentMonth.length;
            for (var j = 0; j < currentMonth.length; j++) {
                switch (contentType){
                    case 'son' :
                        sommeDbCurrentMonth += parseInt(currentMonth[j]._decibel);
                        break;
                    case 'carSpeed' :
                        sommeDbCurrentMonth += parseInt(currentMonth[j]._speed);
                        break;
                }
            }
            var moyenneDb = sommeDbCurrentMonth / nbElemenCurrentMonth;
            switch (contentType){
                case 'son' :
                    resultat.push(
                        {
                            _time: new Date(
                                currentMonth[0]._time.getFullYear(),
                                currentMonth[0]._time.getMonth(), //getMonth fourni mois (0-11)
                                currentMonth[0]._time.getDate(),
                                    0,//heures
                                    0,//minutes
                                    0,//seconde
                                    0),//miliseconde
                            _decibel: moyenneDb
                        });
                    break;
                case 'carSpeed' :
                    resultat.push(
                        {
                            _time: new Date(
                                currentMonth[0]._time.getFullYear(),
                                currentMonth[0]._time.getMonth(), //getMonth fourni mois (0-11)
                                currentMonth[0]._time.getDate(),
                                    0,//heures
                                    0,//minutes
                                    0,//seconde
                                    0),//miliseconde
                            _speed: moyenneDb
                        });
                    break;
            }

        }
        return resultat;
    };
});

sensora.filter('yearFilter', function () {
    return function (array,contentType) {
        var tabYears = [];
        var previousIndex = 0;
        var resultat = [];

        //Crée un tableau où chaque élément est un tableau de chaque annee du param "array"
        for (var i=1 ; i< array.length;i++){
            var annee_previousElem = array[i-1]._time.getFullYear();
            var annee_currentElem =array[i]._time.getFullYear();
            if(annee_previousElem != annee_currentElem)
            {
                var slice = array.slice(previousIndex,i);

                tabYears.push(slice);
                previousIndex =i;
            }
        }

        //Rajoute la dernière annee de l'array qui n'a pas été trouvé par la logique du for au dessus
        if(previousIndex !=0){
            var slice = array.slice(previousIndex,array.length);
            tabYears.push(slice)
        }else{
            tabYears.push(array);
        }


        //Crée un tableau d'objet {time : "temps avec annee précise", decibel : moyenne de la annee}
        for (var i = 0; i < tabYears.length; i++) {
            var currentYear = tabYears[i];
            var sommeDbCurrentYear = 0;
            var nbElemenCurrentYear = currentYear.length;
            for (var j = 0; j < currentYear.length; j++) {
                switch (contentType){
                    case 'son' :
                        sommeDbCurrentYear += parseInt(currentYear[j]._decibel);
                        break;
                    case 'carSpeed' :
                        sommeDbCurrentYear += parseInt(currentYear[j]._speed);
                        break;
                }
            }
            var moyenneDb = sommeDbCurrentYear / nbElemenCurrentYear;
            switch (contentType){
                case 'son' :
                    resultat.push(
                        {
                            _time: new Date(
                                    currentYear[0]._time.getFullYear(),
                                    currentYear[0]._time.getMonth(), //getMonth fourni mois (0-11)
                                    currentYear[0]._time.getDate(),
                                    0,//heures
                                    0,//minutes
                                    0,//seconde
                                    0),//miliseconde
                            _decibel: moyenneDb
                        });
                    break;
                case 'carSpeed' :
                    resultat.push(
                        {
                            _time: new Date(
                                    currentYear[0]._time.getFullYear(),
                                    currentYear[0]._time.getMonth(), //getMonth fourni mois (0-11)
                                    currentYear[0]._time.getDate(),
                                    0,//heures
                                    0,//minutes
                                    0,//seconde
                                    0),//miliseconde
                            _speed: moyenneDb
                        });
                    break;
            }

        }
        return resultat;
    };
});

sensora.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

sensora.filter('toString', function() {


  return function(input) {

    var output;

    output = input.toString();

    return output;

  }

});

sensora.filter('formatDate', function() {


  return function(date) {
      var annee = date.getFullYear();
      var mois = date.getMonth();
      var jour = date.getDate();

      var heure = date.getHours();
      var minute = date.getMinutes();
      var seconde = date.getSeconds();

      var stringDate = '';
      var tabMois = ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'];

      stringDate = (jour)?stringDate+jour.toString()+' ':stringDate+'';
      stringDate = (mois)?stringDate+tabMois[mois]+' ':stringDate+'';
      stringDate = (annee)?stringDate+annee.toString()+' ':stringDate+'';

      stringDate = (jour||mois||annee)?stringDate+',':stringDate+'';

      stringDate = (heure)?stringDate+heure.toString()+':':stringDate+'';
      stringDate = (minute)?stringDate+minute.toString()+':':stringDate+'H';
      stringDate = (!seconde)?stringDate+':':stringDate+'';
      stringDate = (seconde)?stringDate+seconde.toString():stringDate+'min';
      stringDate = (!heure&&!minute)?stringDate+'sec':stringDate+'';

      return stringDate;

  }

});
