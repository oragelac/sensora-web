var scrollable = true;
var navTriggered = true;

$( window ).on('load', function()
{
    $( '.se-pre-con' ).fadeOut("slow");
});

$(document).ready( function()
{
    if( ! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )
    {
        fpInitialize();

        $( '.arrow' ).on('click', function(e)
        {
            e.preventDefault();
            $.fn.fullpage.moveSectionDown();
        })
    }

    window.cookieconsent.initialise(
    {
      "palette": {
        "popup": {
          "background": "#3c404d",
          "text": "#fff"
        },
        "button": {
          "background": "#2fad66",
          "text": "#ffffff"
        }
      },
      "theme": "classic",
      "position": "bottom",
      "content": {
        "message": "Nous utilisons des cookies pour nous assurer que vous ayez la meilleure expérience possible sur notre site. En poursuivant votre navigation, vous acceptez l'utilisation de ces derniers.",
        "dismiss": "Ok!",
        "link": "En apprendre plus"
      }
    });

    $( '.hamburger' ).on( 'click', function( e )
    {
        e.preventDefault();
        triggerMenu();
    });

    $('.measures-nav .toggle').on( 'click', function( e )
    {
        e.preventDefault();
        triggerNav();
    })

    $( '#mailConnection' ).on( 'submit', function( event )
    {
        event.stopPropagation();
        event.preventDefault();

        $.post( $( this ).attr( 'action' ), $( this ).serialize(), function( data )
        {
            $( '#mailConnection' ).toggle();

            let mails = JSON.parse(data);
            console.log(mails);

            $string = '<div id="mails"><ul>';
            $string += '<li><span class="author-name">Auteur</span>'
                    + '<span class="subject">Sujet</span>'
                    + '<span class="date">Date</span></li>';

            mails.forEach( function( mail )
            {
                let time = formatTime(mail._date);
                $string += '<li><span class="author-name">' + mail._author.name + '</span>'
                + '<span class="subject">' + mail._subject +'</span>'
                + '<span class="date">' + time.dmy + '</span></li>';
            });

            $string += '</ul></div>';

            $( '#mails' ).replaceWith($string);
            $( '#mails').toggle();
        })
        .fail( function(jqxhr, textStatus, errorThrown)
        {
            $( '#mailConnection h2').css('color', 'red');
        });
    });
});

function fpInitialize()
{
    var fpSettings = {
        fixedElements: 'header',
        normalScrollElements: 'sensora-article',
        navigation: true,
        scrollOverflow: true
    }
    $( '#fullpage' ).fullpage(fpSettings);
}

function triggerMenu()
{
    $( '.sensora-header' ).fadeToggle('slow');

    if(scrollable)
    {
        $( 'body' ).attr('style', 'overflow-y: hidden');
        scrollable = false;
    }

    else
    {
        $( 'body' ).attr('style', 'overflow-y: scroll');
        scrollable = true;
    }
}

function triggerNav()
{
    if(navTriggered)
    {
        $('.measures-nav').animate({
            width: '70px'
        });

        $('.measures-nav li').animate({
            width: '35px',
            height: '35px',
            padding: '5px'
        });

        $('.measures-nav .button-text').toggle();
        $('.measures-nav .button-icon').toggle();

        $( '.measures').animate({
            paddingLeft: '70px'
        });

        navTriggered = false;
    }

    else
    {
        $('.measures-nav').animate({
            width: '250px'
        });

        $('.measures-nav li').animate({
            width: '90%',
            height: 'auto',
            padding: '0 20px'
        });

        $('.measures-nav .button-text').toggle();
        $('.measures-nav .button-icon').toggle();

        $( '.measures').animate({
            paddingLeft: '250px'
        });

        navTriggered = true;
    }
}

function mail()
{

}

function formatTime(timestamp)
{
    let date = new Date(timestamp * 1000);

    return {
        object : date,
        dmy : date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear(),
        hm : date.getHours() + ':' + date.getMinutes()
    }
}
