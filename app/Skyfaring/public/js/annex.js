/**
 * Fading animation.
 */
$( window ).on('load', function()
{
    $( '.loader' ).toggle();
    $( '.se-pre-con' ).fadeOut( 'slow' );
});

/**
 * Sets up the document.
 */
$( document ).ready( function()
{
    $nav = $( '.annex-navigation li:not(:last-of-type) a' );
    $nav.unbind( 'mouseenter mouseleave' );

    $nav.on( 'click', function(event)
    {
        event.stopPropagation();
        event.preventDefault();

        if( $( this ).parent().hasClass( 'inactive' ) )
        {
            console.log('love');
        }

        else
        {
            getContent(this);
        }
    });
});

/**
 * Replaces the explorer uri
 *
 * @param string title The new page title
 * @param string url The new page url
 */
function changeURI(title, url)
{
    let uri = { name: title, source: url };
    window.history.pushState(uri, title, url);
}

function getContent(button)
{
    $( '.loader' ).toggle();

    $( '.annex-navigation li' ).removeClass( 'active' );
    $( button ).parent().addClass( 'active' );

    let sectionArray = $( button ).attr( 'href' ).split( '/' );
    let sectionName = 'Sensora - ' + sectionArray[ sectionArray.length - 1 ];
    let newURI = $( button ).attr( 'href' );

    // AJAX Implementation.
    $.get( newURI, function( data )
    {
        document.title = sectionName;
        changeURI( sectionName, newURI );
        updateContent( data );
    })
    .fail( function()
    {
        // TODO - Show something here
    })
    .always( function()
    {
        $( '.loader' ).fadeOut('slow');
    });
}

/**
 * Updates the content with DOM manipulation.
 *
 * @param obj data The new content
 */
function updateContent(data)
{
    let value = JSON.parse(data);

    var string = '<article class="scrollable">';
    value.content.forEach( function( element )
    {
        if( element.hasOwnProperty( 'title' ) )
        {
            string += '<h2>' + element.title + '</h2>';
        }

        if( element.hasOwnProperty( 'subtitle' ) )
        {
            string += '<h3>' + element.subtitle + '</h3>';
        }

        if( element.hasOwnProperty( 'text' ) )
        {
            string += '<p>' + element.text + '</p>';
        }

        if( element.hasOwnProperty( 'link' ) )
        {
            string += '<a>' + element.link + '</a>';
        }

        if( element.hasOwnProperty( 'list' ) )
        {
            string += '<ul>';
            element.list.forEach( function(le)
            {
                string += '<li>' + le + '</li>';
            });
            string += '</ul>';
        }
    });

    $( '.annex-content h1' ).text(value.title);
    $( '.annex-content article' ).replaceWith(string);
}
