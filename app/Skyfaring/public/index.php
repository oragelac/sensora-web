<?php

use Skyfaring\Simple as S;

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once '../Simple/App.php';

S\App::create();
S\App::start();
?>
