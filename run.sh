#!/bin/bash

docker run -itd --name wt2-sensora-web \
 --net containers_lightyear --ip 23.20.2.124 \
 -v "${PWD}/etc":"/etc/apache2" \
 -v "${PWD}/log":"/var/log/apache2" \
 -v "${PWD}/app":"/app" \
 sensora-web:latest
