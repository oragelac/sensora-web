FROM wt2/apache-php:latest
MAINTAINER florian.indot@gmail.com

RUN apt-get update && apt-get upgrade \
 && apt-get -y install git php5-mysql php5-curl php5-dev \
    php-pear libyaml-0-2 libyaml-dev \
 && pear update && pecl install yaml \
 && rm -rf /var/lib/apt/lists/*

RUN mkdir /app
WORKDIR /app

RUN sed -i 's/default extension directory./default extension directory.\nextension=yaml.so/' /etc/php5/apache2/php.ini \
 && sed -i 's/default extension directory./default extension directory.\nextension=yaml.so/' /etc/php5/cli/php.ini

ADD ["init.php", "/app"]
RUN ./app/init.sh
RUN composer update

VOLUME ["/etc/apache2", "/app", "/var/log/apache2"]

EXPOSE 80
EXPOSE 8080
EXPOSE 443

ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
